import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import rootReducer from './Reducers' // Or wherever you keep your reducers
import api from './Middleware/Api'
import {createLogger} from 'redux-logger'
import {persistStore, autoRehydrate} from 'redux-persist'
const logger = createLogger({});

import thunk from 'redux-thunk'
import {createStore, applyMiddleware, compose} from 'redux'
import createHistory from 'history/createBrowserHistory'
import {routerMiddleware as RouterMiddleware} from 'react-router-redux'

// Create a history of your choosing (we're using a browser history in this case)
const history = createHistory()

// Build the middleware for intercepting and dispatching navigation actions
const routerMiddleware = RouterMiddleware(history)

// Add the reducer to your store on the `router` key
// Also apply our middleware for navigating
const store = createStore(
    rootReducer,
    undefined,
    compose(
        applyMiddleware(routerMiddleware, thunk, api, logger),
        autoRehydrate()
    )
)

persistStore(
    store,
    {whitelist: ['profile', 'collections']},
    () => {
        console.log('rehydration complete')
    }
)

// Now you can dispatch navigation actions from anywhere!
// store.dispatch(push('/foo'))

ReactDOM.render(
    <App store={store} history={history}/>,
    document.getElementById('root')
);
