import React, {Component} from 'react'
import MainMenu from '../Components/MainMenu'
import ReadableText from '../Components/ReadableText'

class PrivacyPolicy extends Component {
    render() {
        return (
            <section id="main" className="show-menu">
                <MainMenu/>
                <ReadableText title="PlusAfrik Privacy Policy">
                    <p>Plus Afrik, is a social network and online commerce platform
                        for businesses. We enable business to connect to their customers and showcase
                        their products and services</p>

                    <p>Our MISSION is to connect the world through trade, through enabling
                        businesses and customers to build lasting connections and meaningful open
                        relationships.</p>

                    <p>Our registered users (“Members”) share their business
                        identities, engage with their customers and supplier networks, post and view
                        relevant content, and find business and growth opportunities. We believe that
                        our services allow our Members to get the most out of their networks and
                        achieve their full potential. We therefore strive to put our ‘Members’ first
                        and protect your personal information and business identities using industry
                        standard safeguards.</p>

                    <p>We may share your information with your consent or as
                        required by law, and we will always let you know when we make significant
                        changes to this Privacy Policy. Maintaining your trust is our top priority, so
                        we adhere to the following principles to protect your privacy:</p>

                    <p>We protect your personal information and will only provide
                        it to third parties:</p>

                    <p>(a) with your consent;</p>

                    <p>(b) where it is necessary to carry out your instructions; </p>

                    <p>(c) as reasonably necessary in order to provide our features
                        and functionality to you; </p>

                    <p>(d) when we reasonably believe it is required by law,
                        subpoena or other legal process; or </p>

                    <p>(e) as necessary to enforce our User Agreement or protect
                        the rights, property, or safety of Plus Afrik, our Members and Visitors, and
                        the public.</p>

                    <p>This Privacy Policy applies to www.PlusAfrik.com and the
                        Plus Afrik mobile application and all other products and services (collectively
                        the “Services”). We may modify this Privacy Policy from time to time, and if we
                        make material changes to it, we will provide notice through our Service, or by
                        other means so that you may review the changes before you continue to use our
                        Services. If you object to any changes, you may close your account. Continuing
                        to use our Services after we publish or communicate a notice about any changes
                        to this Privacy Policy means that you are consenting to the changes.</p>

                    <p><b>&nbsp;</b></p>

                    <p><b>The Information We Collect:</b></p>

                    <p>Our Privacy Policy applies to any Member or Visitor. We
                        collect information when you use our Services to offer you a personalized and
                        relevant experience, including growing your business’ supplier and customer
                        base and enabling business opportunities.</p>

                    <p>If you have any concern about providing information to us or
                        having such information displayed on our Services or otherwise used in any
                        manner permitted in this Privacy Policy and the User Agreement, you should not
                        become a Member, visit our websites, or otherwise use our Services. If you have
                        already registered, you can close your accounts.</p>

                    <p>&nbsp;</p>

                    <p><b>How we collect information:</b></p>

                    <p>&nbsp;</p>

                    <p><u>Registration</u></p>

                    <p>When you create an account with us, we collect information
                        (including your name, email address, and password).</p>

                    <p>To create an account on Plus Afrik, you must provide us with
                        at least your name, email address and/or mobile number, and a password and
                        agree to our User Agreement and this Privacy Policy, which governs how we treat
                        your information. You may provide additional information during the
                        registration flow (for example, your business contacts, company profiles, and
                        other details) to help you build your profile and to provide you more
                        customized services. You understand that, by creating an account, we and others
                        will be able to identify you by your Plus Afrik profile. We may also ask for
                        your credit card details if you purchase certain additional services.</p>

                    <p><u>Profile Information</u></p>

                    <p>We collect information when you fill out a profile. A
                        complete Plus Afrik profile that includes business details – like your business
                        details, skills and your products and services which helps you get found by
                        other people who want to do business with you.</p>

                    <p>After you create an account you may choose to provide
                        additional information on your Plus Afrik profile, such as honors, awards,
                        professional affiliations, companies or individuals that you follow, and other
                        information including content. Your connections may provide recommendations,
                        ratings and endorsements of you. Providing additional information enables you
                        to derive more benefit from our Services by helping you express your business
                        identity; and help customers and business opportunities find you. It also
                        enables us to serve you ads and other relevant content on and off of our
                        Services.</p>

                    <p><u>Address Book and Other Services</u></p>

                    <p>We collect information when you sync non-Plus Afrik content
                        – like your email address book, mobile device contacts, or calendar – with your
                        account. You can remove your address book and any other synced information
                        whenever you like.</p>

                    <p>You may use our address book or “contacts” importer (or
                        other similar features) to upload your address book into our Services. We store
                        this information (including phone numbers) and use it to help you manage and
                        leverage your contacts in connection with our Services. We also use this
                        information to enhance your experience with our Services by helping you to grow
                        your network by: identifying your contacts that are already Members of our
                        Services; providing a template to send invitations on your behalf to your
                        contacts that are not Members; and suggesting people you may know (even if not
                        in your contacts) but are not yet connected with you on our Services. We may
                        also use this information to show you and other Members that you share the same
                        uploaded contacts who may or may not be Members.</p>

                    <p>&nbsp;</p>

                    <p>Please note that when you send an invitation to connect to
                        another individual on our Services or to join our Service to connect with you,
                        that person may have access to your email address or, for SMS invitations,
                        mobile number because it may be displayed in the invitation. After sending
                        these invitations, we may also remind your invitees of your invitation on your
                        behalf. Your Plus Afrik connections will also have access to your email
                        address.</p>

                    <p>We make other tools available to sync information with our
                        Services, and may also develop additional features that allow Members to use
                        their account in conjunction with other third-party services.</p>

                    <p>Any information that you upload or sync with our Services is
                        covered by the User Agreement and this Privacy Policy. You can remove your
                        information at your convenience using the features we make available. You can
                        remove your address book and any other synced information at any time.</p>

                    <p><u>Customer Service</u></p>

                    <p>We collect information when you contact us for customer
                        support.</p>

                    <p>When you contact our customer support services (e.g. on our
                        Help Center) we may collect the information we need to categorize your
                        question, respond to it, and, if applicable, investigate any breach of our User
                        Agreement or this Privacy Policy. We also use this information to track
                        potential problems and trends and customize our support responses to better
                        serve you. We do not use this information for advertising.</p>

                    <p><u>Using the Plus Afrik Sites and Applications</u></p>

                    <p>We collect information when you visit our Services, use our
                        mobile applications, and interact with advertising on and off our Services.</p>

                    <p>We collect information when you use (whether as a Member or
                        a Visitor) our websites, applications, our platform technology or other
                        Services. For example, we collect information when you view or click on ads on
                        and off our Services, perform a search, import your address book, join and
                        participate in groups, participate in polls, install one of our mobile
                        applications or post updates on our Services. If you are logged in on PlusAfrik.com,
                        one of our cookies on your device identifies you, your usage information and
                        the log data described in this policy, such as your IP address, will be
                        associated by us with your account. Even if you’re not logged into a Service,
                        we log information about devices used to access our Services, including IP
                        address.</p>

                    <p><u>Third-Party Services and Third-Party Sites</u></p>

                    <p>We collect information when you use your account to sign in
                        to other sites or services, and when you view web pages that include our
                        plugins and cookies.</p>

                    <p>You allow us to receive information when you use your
                        account to log in to a third-party website or application. Also, when you visit
                        a third-party site that embeds our social plugins (such as “Save on PlusAfrik”
                        for customers or ‘Share on Plus Afrik for sellers) we receive information that
                        those pages have loaded in your web browser. If you are logged in as a Member
                        when you visit sites with our plugins, we use this information to recommend
                        tailored content to you. We will use this information to personalize the
                        functionality we provide on third-party sites, including providing you insights
                        from your business network and allowing you to share information with your
                        network. Our retention of this data is addressed in Sections below. We may
                        provide reports containing aggregated impression information to companies
                        hosting our plugins and similar technologies to help them measure traffic to
                        their websites, but no personal data.</p>

                    <p>You also allow us to receive information about your visits
                        and interaction with the sites and services of our partners that include our
                        cookies and similar technologies, unless you opt out. If you are not a Member,
                        we rely on the online terms between you and our partners.</p>

                    <p><u>Cookies</u></p>

                    <p>As described in our Cookie Policy, we use cookies and
                        similar technologies, including mobile application identifiers, to help us
                        recognize you across different Services, learn about your interests both on and
                        off our Services, improve your experience, increase security, measure use and
                        effectiveness of our Services, and serve advertising. You can control cookies
                        through your browser settings and other tools. By visiting our Services, you
                        consent to the placement of cookies and beacons in your browser and HTML-based
                        emails in accordance with this Privacy Policy, which incorporates by reference
                        our Cookie Policy.</p>

                    <p><b> Please Note:</b></p>

                    <p>a)We
                        do not share your personal information with any third-party advertisers or ad
                        networks for advertising without your separate permission. Note your profile is
                        visible to other Members and through public search depending on your settings.
                        Also, advertising partners may associate personal information collected by the
                        advertiser directly from you with our cookies and similar technologies. In such
                        instances, we contractually require such advertisers to obtain your explicit
                        opt-in consent before doing so.</p>

                    <p>b)We
                        may show you sponsored or featured content in your Newsfeeds, which will be
                        designated as Featured content and will behave like other updates. If you take
                        social action (for example, if you “like” or “comment” on the sponsored
                        content), your action may be seen by your network and other Members who are
                        shown the sponsored content after you have acted on it.</p>

                    <p>c)We
                        adhere to self-regulatory principles for interest based advertising. If you
                        wish to not receive targeted ads from most third party companies, you may
                        opt-out as described here. Please note this does not opt you out of being
                        served advertising. You will continue to receive generic ads or targeted ads by
                        companies not listed with these opt-out tools. You can also opt out
                        specifically from our use of cookies and similar technologies to track your
                        behavior on third party sites. For non-Members, this opt out setting is here.</p>

                    <p>&nbsp;</p>

                    <p><u>Log Files, IP Addresses, and Information About Your
                        Computer and Mobile Device</u></p>

                    <p>We collect information from the devices and networks that
                        you use to access our Services.</p>

                    <p>When you visit or leave our Services (whether as a Member or
                        Visitor) by clicking a hyperlink or when you view a third-party site that
                        includes our plugin or cookies (or similar technology), we automatically
                        receive the URL of the site from which you came or the one to which you are
                        directed. Also, advertisers receive the URL of the page that you are on when
                        you click an ad on or through our Services. We also receive the internet
                        protocol (“IP”) address of your computer or the proxy server that you use to
                        access the web, your computer operating system details, your type of web
                        browser, your mobile device (including your mobile device identifier provided
                        by your mobile device operating system), your mobile operating system (if you
                        are accessing Plus Afrik using a mobile device), and the name of your ISP or
                        your mobile carrier. We may also receive location data passed to us from
                        third-party services or GPS-enabled devices that you have set up, which we use
                        to show you local information on our mobile applications and for fraud
                        prevention and security purposes. Most mobile devices allow you to prevent real
                        time location data being sent to us, and of course we will honor your settings.</p>

                    <p>In the case of our Android apps, you will be provided notice
                        of the types of data (e.g. location) that will be sent to us. If you choose to
                        use our app after this notice, we process this data to enable registration or
                        preview product features for you. If you choose not to become a Member, we will
                        delete this information.</p>

                    <p><u>Other</u></p>

                    <p>We are constantly innovating to improve our Services, which
                        means we may create new ways to collect information on the Services.</p>

                    <p>Our Services are a dynamic, innovative environment, which
                        means we are always seeking to improve the Services we offer you. We often
                        introduce new features, some of which may result in the collection of new
                        information. Furthermore, new partnerships or corporate acquisitions may result
                        in new features, and we may potentially collect new types of information. If we
                        start collecting substantially new types of personal information and materially
                        change how we handle your data, we will modify this Privacy Policy and notify
                        you in accordance with this document.</p>

                    <p>&nbsp;</p>

                    <p><b>HOW WE USE YOUR PERSONAL INFORMATION</b></p>

                    <p><u>Information Processing Consent</u></p>

                    <p>You agree that information you provide on your profile can
                        be seen by others and used by us as described in this Privacy Policy and our
                        User Agreement.</p>

                    <p>The personal information that you provide to us may reveal
                        or allow others to identify aspects of your life that are not expressly stated
                        on your profile (for example, your picture or your name may reveal your
                        gender). By providing personal information to us when you create or update your
                        account and profile, you are expressly and voluntarily accepting the terms and
                        conditions of our User Agreement and freely accepting and agreeing to our
                        processing of your personal information in ways set out by this Privacy Policy.
                        Supplying to us any information deemed “sensitive” by applicable law is
                        entirely voluntary on your part. You can withdraw or modify your consent to our
                        collection and processing of the information you provide at any time, in
                        accordance with the terms of this Privacy Policy and the User Agreement, by
                        changing your account settings or your profile on Plus Afrik, or by closing your
                        accounts.</p>

                    <p><b>&nbsp;</b></p>

                    <p><u>Plus Afrik Communications to you</u></p>

                    <p>We communicate with you using Plus Afrik messaging, email,
                        and other ways available to us. We may send you messages relating to the
                        availability of the Services, security, or other service-related issues. We
                        also may send promotional messages to your inbox. You can change your email
                        settings at any time.</p>

                    <p>We communicate with you through email, notices posted on the
                        Plus Afrik websites, messages to your Plus Afrik inbox, and other means
                        available through the Services, including mobile text messages and push
                        notifications.</p>

                    <p> Examples of these communications include: </p>

                    <p>a) Welcome and engagement communications - informing you
                        about how to best use our Services, new features, updates about other Members
                        you are connected to and their actions, etc.; </p>

                    <p>b) Service communications - these will cover service
                        availability, security, and other issues about the functioning of our Services;
                    </p>

                    <p>c) Promotional communications - these include both email and
                        Plus Afrik messages, and may contain promotional information directly or on
                        behalf of our partners, including products, promotions and information from
                        companies that are marketing. These messages will be sent to you based on your
                        profile information and messaging preferences. We track the open rate of your
                        messages to provide you with an acceptance score. You may change your email and
                        contact preferences at any time by signing into your account and changing your
                        Plus Afrik email settings. You can also opt out of promotional messages by
                        sending a request to us.</p>

                    <p>NB. Please be aware that you cannot opt out of receiving
                        service messages from us.</p>

                    <p><u>User Communications</u></p>

                    <p>With certain communications you send on our Services, the
                        recipient can see your name, email address, and some network information.</p>

                    <p>Many communications that you initiate through our Services
                        (for example, an invitation sent to a non-Member) will list your name and
                        primary email address in the header of the message. Messages you initiate may also
                        provide the recipient with aggregate information about your network (for
                        example, how many people are in your network). Other communications that you
                        initiate through the Services, like an invitation, will list your name as the
                        initiator but will not include your personal email address contact information.
                        Once you have connected with an individual, regardless of who sent the
                        invitation, your contact information will be shared with that individual.</p>

                    <p>We use automatic scanning technology to help protect you and
                        other Members. Such technology checks links and other content, updates and
                        contributions to help us identify and block malicious links and malware, reduce
                        spam and optimize the delivery of our Services.</p>

                    <p><b>&nbsp;</b></p>

                    <p><u>Service Development and customisation</u></p>

                    <p>We use the information and content you provide to us to
                        conduct research and development and to customize your experience and try to
                        make it relevant and useful to you.</p>

                    <p>We use information and content that you and other Members
                        provide to us to conduct research and development for the improvement of our
                        Services in order to provide you and other Members and Visitors with a better,
                        more intuitive experience and drive membership growth and engagement on our Services.</p>

                    <p>&nbsp;</p>

                    <p>We also customize your experience and the experiences of
                        others on our Services. For example, when you sign in to your account, we may
                        display the names and photos of new Members who have recently joined your
                        network or recent updates from your customers and suppliers. We try to show you
                        content, such as products, services and promotions, that is relevant to you, your
                        industry, or your trade. We also use Members information and content for
                        invitations and communications promoting our Services that are tailored to the
                        recipient.</p>

                    <p><u>Sharing Information with Affiliates </u></p>

                    <p>We share your information across our different Services,
                        among companies in the Plus Afrik family.</p>

                    <p>We may share your personal information with our affiliates
                        (meaning entities controlled by, controlling or under common control with Plus
                        Afrik) outside of the Plus Afrik entity that is your data controller as
                        reasonably necessary to provide the Services. You are consenting to this
                        sharing.</p>

                    <p>We combine information internally across different Services
                        within Plus Afrik as we may be able to identify you across different Services
                        using cookies or similar technologies.</p>

                    <p><u>Sharing Information with Third Parties</u></p>

                    <p>Any information you put on your profile and any content you
                        post on Plus Afrik may be seen by others.</p>

                    <p>We don’t provide any of your non-public information (like
                        your email address) to third parties without your consent, unless required by
                        law.</p>

                    <p>Other people may find your Plus Afrik profile information
                        through search engines (you can choose which parts of your public profile are
                        accessible to public search engines in your settings), or use services like
                        Twitter in conjunction with your Plus Afrik account.</p>

                    <p>We offer a “public profile” feature that allows you as a
                        Member to publish portions of your business’ profile to the public Internet.
                        This public profile will be indexed and displayed through public search engines
                        when someone searches for your name. </p>

                    <p>The visibility of your profile to other Members depends on
                        your degree of connection with the viewing Member, the subscriptions they may
                        have, their usage of the Services, access channels and search types (e.g. by
                        name or by keyword). Please note that customers, suppliers and other such
                        business subscribers can see your full profile.</p>

                    <p>We will not disclose personal information that is not
                        published to your profile or generated through engagement with our other
                        services, except to carry out your instructions (for example, to process
                        payment information) or unless we have your separate consent or we have a good
                        reason to believe that disclosure is permitted by law or is reasonably
                        necessary to:</p>

                    <p>a) comply with a legal requirement or process, including,
                        but not limited to, civil and criminal subpoenas, court orders or other
                        compulsory disclosures;</p>

                    <p>b) enforce this Privacy Policy or our User Agreement; </p>

                    <p>c) respond to claims of a violation of the rights of third
                        parties; </p>

                    <p>d) respond to Member service inquiries; or </p>

                    <p>e) protect the rights, property, or safety of Plus Afrik,
                        our Services, our Members, Visitors, or the public. </p>

                    <p>Also, if you have opted to bind any of your Service accounts
                        to your Twitter, Facebook or other similar account, you can easily share
                        content from our Services to these third party services, in accordance with
                        your account settings (which you may change at any time) and respective
                        policies of these third parties. Further, we allow third parties to look-up
                        profile information (subject to your privacy settings) using your email address
                        or first and last name information through its profile API.</p>

                    <p>Third parties (for example, your email provider) may give
                        you the option to upload certain information in your contacts stored with us
                        onto their own service. If you choose to share your contacts in this way, you
                        will be granting your third party provider the right to store, access, disclose
                        and use these contacts in the ways described in such third party's terms and
                        privacy policy.</p>

                    <p><u>Polls and Surveys</u></p>

                    <p>We conduct our own surveys and polls and also help third
                        parties do this type of research. Your participation in surveys or polls is up
                        to you. You may also opt out of getting invitations to participate in surveys.</p>

                    <p>Polls and Surveys may be conducted by us, Members, or third
                        parties. Some third parties may target advertisements to you on the results
                        page based on your answers in the poll. We may use third parties to deliver
                        incentives to you to participate in surveys or polls. If the delivery of
                        incentives requires your contact information, you may be asked to provide
                        personal information to the third party fulfilling the incentive offer, which
                        will be used only for the purpose of delivering incentives and verifying your
                        contact information. It is up to you whether you provide this information, or
                        whether you desire to take advantage of an incentive. Your consent to use any
                        personal information for the purposes set forth in the poll or survey will be
                        explicitly requested by the party conducting it. </p>

                    <p><u>Search</u></p>

                    <p>Our Services help you search for other business people,
                        companies, products for sale, services for sale, places, events and groups.</p>

                    <p>You can search for Members, customers, information about
                        companies, and community content on our Services. You can also find business
                        opportunities, new customers, new products that you may want to buy and
                        information about companies. We use personal information from our Services, to
                        inform and refine our search service.</p>

                    <p><u>Compliance with Legal Process and Other Disclosures</u></p>

                    <p>We may disclose your personal information if compelled by
                        law, subpoena, or other legal process, or if necessary to enforce our User
                        Agreement.</p>

                    <p>It is possible that we may need to disclose personal
                        information, profile information, or information about your activities as a
                        Member or Visitor when required by law, subpoena, or other legal process,
                        within the relevant jurisdictions if we have a good reason to believe that
                        disclosure is reasonably necessary to (1) investigate, prevent, or take action
                        regarding suspected or actual illegal activities or to assist government
                        enforcement agencies; (2) enforce the User Agreement, investigate and defend
                        ourselves against any third-party claims or allegations, or protect the
                        security or integrity of our Service; or (3) exercise or protect the rights,
                        property, or safety of Plus Afrik, our Members, personnel, or others. We
                        attempt to notify Members about legal demands for their personal information
                        when appropriate in our judgment, unless prohibited by law or court order or
                        when the request is an emergency. In light of our principles, we may dispute
                        such demands when we believe, in our discretion, that the requests are
                        overbroad, vague or lack proper authority, but do not commit to challenge every
                        demand. </p>

                    <p><u>Disclosures to Others as the Result of a Change in
                        Control or Sale of Plus Afrik</u></p>

                    <p>If there is a change in control or sale of all or part of
                        Plus Afrik, we may share your information with a third party, who will have the
                        right to use that information in line with this Privacy Policy.</p>

                    <p>We may also disclose your personal information to a third
                        party as part of a sale of the assets of Plus Afrik, a subsidiary, or division,
                        or as the result of a change in control of the company or one of its
                        affiliates, or in preparation for any of these events. Any third party to which
                        we transfer or sell our assets will have the right to continue to use the
                        personal and other information that you provide to us in the manner set out in
                        this Privacy Policy.</p>

                    <p><u>Service Providers</u></p>

                    <p>We may employ third parties to help us with the Services</p>

                    <p>We may employ third party companies and individuals to
                        facilitate our Services (e.g. maintenance, analysis, audit, marketing and
                        development). These third parties have limited access to your information only
                        to perform these tasks on our behalf and are obligated to Plus Afrik not to
                        disclose or use it for other purposes.</p>

                    <p><u>Data Processing Outside Your Country</u></p>

                    <p>We may process your information outside the country where
                        you live.</p>

                    <p>We may transfer your information and process it outside your
                        country of residence, wherever Plus Afrik, its affiliates and service providers
                        operate.</p>

                    <p>&nbsp;</p>

                    <p><b>Your Choices &amp; Obligations</b></p>

                    <p><u>Rights to Access, Correct, or Delete Your Information,
                        and Closing Your Account</u></p>

                    <p>You can change your Plus Afrik information at any time by
                        editing your profile, deleting content that you have posted, or by closing your
                        account. You can also ask us for additional information we may have about your
                        account.</p>

                    <p>You have a right to (1) access, modify, correct, or delete
                        your personal information controlled by Plus Afrik regarding your profile, (2)
                        change or remove your content, and (3) close your account. You can request your
                        personal information that is not viewable on your profile or readily accessible
                        to you (for example, your IP access logs). If you close your account(s), your
                        information will generally be removed from the Service within 24 hours. We
                        generally delete closed account information and will de-personalize any logs or
                        other backup information through the deletion process within 30 days of account
                        closure, except as noted below.</p>

                    <p>Please note: Information you have shared with others (for
                        example, through messages, network updates, content sharing) or that others
                        have copied may also remain visible after you have closed your account or
                        deleted the information from your own profile. In addition, you may not be able
                        to access, correct, or eliminate any information about you that other Members
                        copied or exported out of our Services, because this information may not be in
                        our control. Your public profile may be displayed in search engine results
                        until the search engine refreshes its cache.</p>

                    <p><u>Data Retention</u></p>

                    <p>We keep your information for as long as your account is
                        active or as needed. For example, we may keep certain information even after
                        you close your account if it is necessary to comply with our legal obligations,
                        meet regulatory requirements, resolve disputes, prevent fraud and abuse, or
                        enforce this agreement.</p>

                    <p>We retain the personal information you provide while your
                        account is in existence or as needed to provide you services. We may retain
                        your personal information even after you have closed your account if retention
                        is reasonably necessary to comply with our legal obligations, meet regulatory
                        requirements, resolve disputes between Members, prevent fraud and abuse, or
                        enforce this Privacy Policy and our User Agreement. We may retain personal information,
                        for a limited period of time, if requested by law enforcement. Our Customer
                        Service may retain information for as long as is necessary to provide
                        support-related reporting and trend analysis only, but we generally delete or
                        de-personalize closed account data as stated above.</p>

                    <p><b>Changes to this Privacy Policy</b></p>

                    <p>We will notify you when we change this Privacy Policy.</p>

                    <p>We may change this Privacy Policy from time to time. If we
                        make significant changes in the way we treat your personal information, or to
                        the Privacy Policy, we will provide notice to you on the Services or by some
                        other means, such as email. Please review the changes carefully. If you agree
                        to the changes, simply continue to use our Services. If you object to any of
                        the changes to our terms and you no longer wish to use our Services, you may
                        close your account(s). Unless stated otherwise, our current Privacy Policy
                        applies to all information that we have about you and your account. Using our
                        Services after a notice of changes has been communicated to you or published on
                        our Services shall constitute consent to the changed terms or practices.</p>

                    <p><b>Security</b></p>

                    <p>We take privacy and security seriously and have enabled
                        HTTPS access to our site (turn on HTTPS), in addition to existing SSL access
                        over mobile devices. Also, please know that the Internet is not a secure
                        environment, so be careful and select strong passwords.</p>

                    <p>We have implemented security safeguards designed to protect
                        the personal information that you provide. Access to your data on our Services
                        is password-protected, and data such as credit card information is protected by
                        SSL encryption when it is exchanged between your web browser and the Services.
                        We also offer secure https access to the website. However, since the Internet
                        is not a 100% secure environment, we cannot ensure or warrant the security of
                        any information that you transmit to us. There is no guarantee that information
                        may not be accessed, disclosed, altered, or destroyed by breach of any of our
                        physical, technical, or managerial safeguards. It is your responsibility to
                        protect the security of your login information. Please note that emails,
                        instant messaging, and similar means of communication with other Members are
                        not encrypted, and we strongly advise you not to communicate any confidential
                        information through these means. Please help keep your account safe by using a
                        strong password.</p>

                    <p><b>IF YOU HAVE ANY QUESTIONS TO THIS POLICY PLEASE CONTACT
                        US</b></p>
                </ReadableText>
            </section>
        )
    }
}

PrivacyPolicy.propTypes = {}

export default PrivacyPolicy