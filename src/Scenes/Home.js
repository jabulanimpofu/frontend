import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import {API} from '../Constants'
const { GET_POSTS, GET_SERVICES, GET_PRODUCTS, GET_TIMELINE_LOCATION, GET_TIMELINE } = API
import {getTimeline, getProductCategories, getSubCategories, getProducts, getServices, getPosts, createPost, save, comment, share, like, follow, invite, loadPhotos, loadPhoto, getActive, getFeatured, getRecentActivity } from '../Actions/Api'
import {clearNewTimeline} from '../Actions/App'
import MainMenu from '../Components/MainMenu'
import Wall from '../Components/Wall'
import Recent from '../Components/Recent'

class Home extends Component {
    componentDidMount() {
        const {
            active_users,
            timeline,
            token,
            user_id,
            doGetPosts,
            doGetProducts,
            doGetServices,
            doGetTimeline,
            doGetActive,
            doGetFeatured,
            doLoadPhotos,
            doLoadPhoto,
        } = this.props

        //has id
        if (user_id) {
            if(!this.props.filter.type || this.props.filter.type === GET_TIMELINE)
                doGetTimeline()
            if(this.props.filter.type === GET_PRODUCTS)
                doGetProducts()
            if(this.props.filter.type === GET_SERVICES)
                doGetServices()
            if(this.props.filter.type === GET_POSTS)
                doGetPosts()

            doGetActive()
            doGetFeatured()
        }

    }

    componentDidUpdate(prevProps) {
        const {
            active_users,
            timeline,
            token,
            user_id,
            doGetPosts,
            doGetProducts,
            doGetServices,
            doGetTimeline,
            doGetActive,
            doGetFeatured,
            doLoadPhotos,
            doLoadPhoto,
        } = this.props



        //new id
        if (user_id && ((prevProps.user_id !== user_id) || (prevProps.filter.type !== this.props.filter.type))) {
            if(!this.props.filter.type  || this.props.filter.type === GET_TIMELINE)
                doGetTimeline()
            if(this.props.filter.type === GET_PRODUCTS)
                doGetProducts()
            if(this.props.filter.type === GET_SERVICES)
                doGetServices()
            if(this.props.filter.type === GET_POSTS)
                doGetPosts()

            doGetActive()
            doGetFeatured()
        }
    }

    render() {
        const {timeline, profile, product_categories, sub_categories, active_users,  featured, recent_activity, doCreatePost, doClearNewTimeline, doComment, doSave, doShare, doLike, doFollow, doConnect, doGetProductCategories, doGetSubCategories, doGetRecentActivity} = this.props
        return (
            <section id="main" className="show-recent show-menu">
                <MainMenu/>
                <Recent/>
                <Wall posts={timeline}
                      product_categories={product_categories}
                      sub_cateogries={sub_categories}
                      active_users={active_users}
                      featured={featured}
                      createPost={doCreatePost}
                      clearNewTimeline={doClearNewTimeline}
                      doGetProductCategories={doGetProductCategories}
                      doGetSubCategories={doGetSubCategories}
                      doComment={doComment}
                      doSave={doSave}
                      doShare={doShare}
                      doLike={doLike}
                      doFollow={doFollow}
                      doConnect={doConnect}
                />
            </section>
        )
    }
}

Home.propTypes = {
    active_users: PropTypes.object.isRequired,
    product_categories: PropTypes.object.isRequired,
    sub_categories: PropTypes.object.isRequired,
    timeline: PropTypes.object.isRequired,
    doGetProductCategories: PropTypes.func.isRequired,
    doGetSubCategories: PropTypes.func.isRequired,
    doGetTimeline: PropTypes.func.isRequired,
    doCreatePost: PropTypes.func.isRequired,
    doClearNewTimeline: PropTypes.func.isRequired,
}

const mapStateToProps = (state, ownProps) => {
    const user_id = state.profile.data.id
    const {collections, filter, profile} = state
    const {timeline, product_categories, sub_categories, active_users, featured, recent_activity} = collections

    return {
        active_users,
        timeline,
        user_id,
        product_categories,
        sub_categories,
        featured,
        recent_activity,
        filter,
        profile
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        doGetProductCategories(){
            return dispatch(getProductCategories())
        },
        doGetSubCategories(){
            return dispatch(getSubCategories())
        },
        doGetPosts(){
            return dispatch(getPosts())
        },
        doGetProducts(){
            return dispatch(getProducts())
        },
        doGetServices(){
            return dispatch(getServices())
        },
        doGetTimeline(){
            return dispatch(getTimeline())
        },
        doClearNewTimeline(){
            return dispatch(clearNewTimeline())
        },

        doCreatePost(post){
            return dispatch(createPost(post))
        },
        doLike(post_id){
            return dispatch(like(post_id))
        },
        doShare(post_id){
            return dispatch(share(post_id))
        },
        doSave(post_id){
            return dispatch(save(post_id))
        },
        doComment(post_id, comment_text){
            return dispatch(comment(post_id, comment_text))
        },
        doFollow(user_id){
            alert(user_id)
            return dispatch(follow(user_id))
        },
        doConnect(user_id){
            return dispatch(invite(user_id))
        },


        doGetActive(){
            return dispatch(getActive())
        },
        doGetFeatured(type){
            return dispatch(getFeatured(type))
        },
        doGetRecentActivity(){
            return dispatch(getRecentActivity())
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)