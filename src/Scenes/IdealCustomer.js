import React, {Component} from 'react'
import {connect} from 'react-redux'
import Jumbotron from '../Components/Jumbotron'


const offers = [
    {
        name: 'starter',
        number: 25,
        custom: false,
        color: "bgm-amber",
    },
    {
        name: 'moderate',
        number: 100,
        custom: false,
        color: "bgm-blue",
    },
    {
        name: 'pro',
        number: 500,
        custom: false,
        color: "bgm-green",
    },
    {
        name: 'custom',
        number: 100,
        custom: true,
        color: "bgm-red",
    },
]

const unit_price = 20

const Offer = ({offer: {name, number, custom, color}, number: active_number, active, onSelect}) => {
    const _custom = custom ? (
        <input placeholder={"number"}
               type="number"
               className="form-control"
               name={"number"}
               value={active_number}
               style={{
                   display: 'inline',
                   width: 'initial'
               }}
               onChange={
                   (event) => {
                       event.preventDefault()
                       onSelect(event.target.value)
                   }}/>
    ) : number

    return (
        <div className="col-xs-3">
            <div className={`${color} ${active ? "card" : ""} brd-2 p-15`} onClick={(e) => {
                e.preventDefault()
                onSelect(number)
            }}>
                <div className="c-white m-b-5">{name.toUpperCase()}</div>
                <h2 className="m-0 c-white f-300">{_custom} invites</h2>
            </div>
        </div>
    )
}

class GoldSeller extends Component {
    constructor(props) {
        super(props)
        this.state = {
            offer: 'advanced',
            number: 1000,
        }
    }

    onSelect({name: offer}, number) {
        this.setState({offer, number})
    }

    componentDidMount() {

    }

    componentDidUpdate(prevProps) {
    }

    render() {
        const {offer: selected_offer, number} = this.state
        const _offers = offers.map((offer, index) => {
            return (<Offer offer={offer} active={offer.name === selected_offer}
                           onSelect={this.onSelect.bind(this, offer.name)} key={index}/>)
        })
        return (
            <section id="main" className="" style={{padding: 0}}>
                <section id="content">
                    <div className="container c'boxed">
                        <Jumbotron>
                            <h1>Every business has it's ideal customer... Find Yours!</h1>

                            <p>'Ideal Customer' is a service offered to Plus Afrik Gold Sellers. You get a unique
                                'INVITE' button that allows you to find and send invitations to follow you to your
                                'ideal' customers.</p>

                            <p> This is the best customer targeting tool on the market.</p>
                        </Jumbotron>
                    </div>
                    <div className="container invoice" style={{marginLeft: 32, marginRight: 32}}>

                        <div className="card">
                            <div className="card-header ch-alt text-center">
                                <img className="i-logo" src="/img/badge.png" alt=""/>
                            </div>

                            <div className="card-body card-padding">
                                <div className="row m-b-25 text-center">
                                    <h4>
                                        <h2>Get as many customers as you want
                                            <small>Select the number of customers you want and get the cost</small>
                                        </h2>
                                    </h4>
                                </div>

                                <div className="clearfix"></div>

                                <div className="row m-t-25 p-0 m-b-25">
                                    {_offers}
                                </div>

                                <div className="clearfix"></div>

                                <table className="table i-table m-t-25 m-b-25">
                                    <thead className="text-uppercase">
                                    <tr>
                                        <th className="c-gray">ITEM DESCRIPTION</th>
                                        <th className="c-gray">UNIT PRICE</th>
                                        <th className="c-gray">QUANTITY</th>
                                        <th className="highlight">TOTAL</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    </tbody>
                                    <thead>
                                    <tr>
                                        <td width="50%">
                                            <h5 className="text-uppercase f-400">Ideal Customer Invites</h5>
                                            <p className="text-muted">The number of invites you get</p>
                                        </td>

                                        <td>{`$${unit_price}.00`}</td>
                                        <td>{number}</td>
                                        <td className="highlight">{`$${number * unit_price}.00`}</td>
                                    </tr>
                                    </thead>

                                </table>

                                <div className="clearfix"></div>

                            </div>

                        </div>

                    </div>
                </section>
            </section>
        )
    }
}

GoldSeller.propTypes = {}

const mapStateToProps = (state, ownProps) => {
    return {}
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(GoldSeller)