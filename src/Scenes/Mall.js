import React, {Component} from 'react'
import MainMenu from '../Components/MainMenu'
import ProductsSearch from '../Components/ProductsSearch'
import Recent from '../Components/Recent'
import {connect} from 'react-redux'
import {API} from '../Constants'
const { GET_POSTS, GET_SERVICES, GET_PRODUCTS, GET_TIMELINE_LOCATION, GET_TIMELINE } = API
import {getTimeline, getProductCategories, getSubCategories, getProducts, getServices, getPosts, createPost, save, comment, share, like, follow, invite, loadPhotos, loadPhoto, getActive, getFeatured, getRecentActivity } from '../Actions/Api'
import {clearNewTimeline} from '../Actions/App'
import Wall from '../Components/Wall'

const categories = [
    'Settings',
    'Account',
    'Business',
    'Community',
    'Privacy',
    'General',
    'Adverts',
    'Location',

]


const products = {
    data: [1, 2, 3, 4, 5, 6,].map((review, index) => ({
        object: {},
        id: index
    }))
}

class Mall extends Component {
    render() {
        const {timeline, profile, product_categories, sub_categories, active_users,  featured, recent_activity, doCreatePost, doClearNewTimeline, doComment, doSave, doShare, doLike, doFollow, doConnect, doGetProductCategories, doGetSubCategories, doGetRecentActivity} = this.props
        return (
            <section id="main" className="show-recent show-menu">
                <MainMenu />
                <Recent />
                <ProductsSearch products={products} />
            </section>
        )
    }
}

Mall.propTypes = {}

const mapStateToProps = (state, ownProps) => {
    const user_id = state.profile.data.id
    const {collections, filter, profile} = state
    const {timeline, product_categories, sub_categories, active_users, featured, recent_activity} = collections

    return {
        active_users,
        timeline,
        user_id,
        product_categories,
        sub_categories,
        featured,
        recent_activity,
        filter,
        profile
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        doGetProductCategories(){
            return dispatch(getProductCategories())
        },
        doGetSubCategories(){
            return dispatch(getSubCategories())
        },
        doGetPosts(){
            return dispatch(getPosts())
        },
        doGetProducts(){
            return dispatch(getProducts())
        },
        doGetServices(){
            return dispatch(getServices())
        },
        doGetTimeline(){
            return dispatch(getTimeline())
        },
        doClearNewTimeline(){
            return dispatch(clearNewTimeline())
        },

        doCreatePost(post){
            return dispatch(createPost(post))
        },
        doLike(post_id){
            return dispatch(like(post_id))
        },
        doShare(post_id){
            return dispatch(share(post_id))
        },
        doSave(post_id){
            return dispatch(save(post_id))
        },
        doComment(post_id, comment_text){
            return dispatch(comment(post_id, comment_text))
        },
        doFollow(user_id){
            alert(user_id)
            return dispatch(follow(user_id))
        },
        doConnect(user_id){
            return dispatch(invite(user_id))
        },


        doGetActive(){
            return dispatch(getActive())
        },
        doGetFeatured(type){
            return dispatch(getFeatured(type))
        },
        doGetRecentActivity(){
            return dispatch(getRecentActivity())
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Mall)