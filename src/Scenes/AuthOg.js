import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import {login, register, reset, email} from '../Actions/Api'

const Login = ({email, password, onFieldChange, show, login}) => {
    return (
        <div className="center-block col-xs-12  col-sm-8 col-md-8 col-lg-6">
            <div className="lb-header">
                <h3>Connect to your business</h3>
                <h4>Sign in to get in touch</h4>
            </div>

            <div className="lb-body">
                <div className="form-group fg-float">
                    <div className="fg-line">
                        <input value={email} onChange={onFieldChange.bind(undefined, 'email')} type="text"
                               className="input-sm form-control fg-input"/>
                        <label className="fg-label">Email</label>
                    </div>
                </div>

                <div className="form-group fg-float">
                    <div className="fg-line">
                        <input value={password} onChange={onFieldChange.bind(undefined, 'password')} type="password"
                               className="input-sm form-control fg-input"/>
                        <label className="fg-label">Password</label>
                    </div>
                </div>

                <button className="btn btn-block btn-lg palette-Light-Blue-A200 bg" onClick={login}>Sign
                    in
                </button>

                <div className="m-t-20 m-b-20">
                    <a onClick={show.bind(undefined, "reset")} className="palette-Grey-800 text">Forgot password?</a>
                    <a onClick={show.bind(undefined, "register")} className="palette-Grey-400 text d-block m-b-5"
                       href="">Don't have an account?</a>
                </div>

                <button onClick={show.bind(undefined, "register")}
                        className="btn btn-block btn-lg palette-Light-Blue-A200 bg">Sign Up
                </button>

            </div>
        </div>
    )
}

const Register = ({name, email, password, account_type, accept_terms, onFieldChange, show, register}) => {
    return (
        <div className="card">
            <div className="lb-header">
                <h3>Connect with your business</h3>
            </div>

            <div className="lb-body row">
                <div className="col-md-6">

                    <div className="list-group lg-alt c-overflow">
                        <a href="" className="list-group-item media" style={{borderWidth: 1, borderColor: 'grey'}}>
                            <div className="pull-left">
                                <img style={{borderRadius: 0}} className="avatar-img" src="/img/icons/radar-icon.png" alt=""/>
                            </div>

                            <div className="media-body">
                                <div className="lgi-heading">Find Customers</div>
                                <small className="lgi-text">and create relationships</small>
                            </div>
                        </a>

                        <a href="" className="list-group-item media">
                            <div className="pull-left">
                                <img className="avatar-img" src="/img/icons/connect-icon.png" alt=""/>
                            </div>
                            <div className="media-body">
                                <div className="lgi-heading">Connect with existing customers</div>
                                <small className="lgi-text">and build trust</small>
                            </div>
                        </a>

                        <a href="" className="list-group-item media">
                            <div className="pull-left">
                                <img className="avatar-img" src="/img/icons/post-icon-01.png" alt=""/>
                            </div>
                            <div className="media-body">
                                <div className="lgi-heading">Post unlimited products and services</div>
                                <small className="lgi-text">like a pro</small>
                            </div>
                        </a>

                        <a href="" className="list-group-item media">
                            <div className="pull-left">
                                <img className="avatar-img" src="/img/icons/$0-02.png" alt=""/>
                            </div>
                            <div className="media-body">
                                <div className="lgi-heading">Its all free and</div>
                                <small className="lgi-text">always will be</small>
                            </div>
                        </a>

                    </div>
                </div>
                <div className="col-md-6">

                    <div className="form-group fg-float">
                        <div className="fg-line">
                            <input value={name} onChange={onFieldChange.bind(undefined, 'name')} type="text"
                                   className="input-sm form-control fg-input"/>
                            <label className="fg-label">Name</label>
                        </div>
                    </div>

                    <div className="form-group fg-float">
                        <div className="fg-line">
                            <input value={email} onChange={onFieldChange.bind(undefined, 'email')} type="text"
                                   className="input-sm form-control fg-input"/>
                            <label className="fg-label">Email</label>
                        </div>
                    </div>

                    <div className="form-group fg-float">
                        <div className="fg-line">
                            <input value={password} onChange={onFieldChange.bind(undefined, 'password')}
                                   type="password" className="input-sm form-control fg-input"/>
                            <label className="fg-label">Password</label>
                        </div>
                    </div>

                    <div className="form-group fg-float">
                        <div className="fg-line row">
                            <div className="radio m-b-15 col-xs-6">
                                <label>
                                    <input value="individual"
                                           onChange={onFieldChange.bind(undefined, 'account_type')} type="radio"
                                           name="account_type"/>
                                    <i className="input-helper"></i>
                                    Individual
                                </label>
                            </div>

                            <div className="radio m-b-15 col-xs-6" style={{marginTop: 10}}>
                                <label>
                                    <input value="company"
                                           onChange={onFieldChange.bind(undefined, 'account_type')} type="radio"
                                           name="account_type"/>
                                    <i className="input-helper"></i>
                                    Company
                                </label>
                            </div>

                        </div>
                    </div>

                    <div className="checkbox m-b-30">
                        <label>
                            <input value={accept_terms} onChange={onFieldChange.bind(undefined, 'accept_terms')}
                                   type="checkbox"/>
                            <i className="input-helper"></i>
                            Accept the license agreement
                        </label>
                    </div>

                    <button className="btn palette-Light-Blue-A200 bg" onClick={register}>Create
                        Account
                    </button>

                    <div className="m-t-30">
                        <a onClick={show.bind(undefined, "login")}
                           className="palette-Light-Blue-A200 text d-block m-b-5" href="">Already have an
                            account?</a>
                        <a onClick={show.bind(undefined, "reset")} href=""
                           className="palette-Light-Blue-A200 text">Forgot password?</a>
                    </div>
                </div>
            </div>
        </div>
    )

}

const Reset = ({email, onFieldChange, show, reset}) => {
    return (
        <div className="card">
            <div className="lb-header palette-Purple bg">
                <i className="zmdi zmdi-account-circle"></i>
                Forgot Password?
            </div>

            <div className="lb-body">
                <p className="m-b-30">Lorem ipsum dolor fringilla enim feugiat commodo sed ac lacus.</p>

                <div className="form-group fg-float">
                    <div className="fg-line">
                        <input value={email} onChange={onFieldChange.bind(undefined, 'email')} type="text"
                               className="input-sm form-control fg-input"/>
                        <label className="fg-label">Email Address</label>
                    </div>
                </div>

                <button className="btn palette-Purple bg" onClick={reset}>Reset Password
                </button>

                <div className="m-t-30">
                    <a onClick={show.bind(undefined, "login")} className="palette-Purple text d-block m-b-5"
                       href="">Already have an account?</a>
                    <a onClick={show.bind(undefined, "register")} href="" className="palette-Purple text">Create an
                        account</a>
                </div>
            </div>
        </div>
    )
}

class Auth extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            name: '',
            account_type: 'individual',
            accept_terms: '',
            show: 'login'
        }
    }

    componentDidMount() {
        const clearForm = () => {
            this.setState({
                email: '',
                password: '',
                name: '',
                account_type: '',
                accept_terms: '',
            })
        }
    }

    onFiledChange(field, event) {
        this.setState({[field]: event.target.value});
    }

    login() {
        const {email, password} = this.state
        const {doLogin} = this.props
        doLogin(email, password)

    }

    register() {
        const {email, password, name, account_type, accept_terms} = this.state
        const {doRegister} = this.props
        doRegister(email, password, name, account_type, accept_terms)

    }

    email() {
        const {email} = this.state
        const {doEmail} = this.props
        doEmail(email)
    }

    reset() {
        const {password} = this.state
        const {doReset} = this.props
        doReset(doReset)
    }

    onShow(show, e) {
        e.preventDefault()
        this.setState({show})
    }

    render() {
        console.log(this.props)
        const {email, password, name, account_type, accept_terms, show} = this.state
        let content = null

        switch (show) {
            case "register": {
                content = <Register email={email}
                                    password={password}
                                    onFieldChange={this.onFiledChange.bind(this)}
                                    show={this.onShow.bind(this)} login={this.login.bind(this)}/>

            }
                break
            case "reset": {
                content = <Reset email={email}
                                 password={password}
                                 onFieldChange={this.onFiledChange.bind(this)}
                                 show={this.onShow.bind(this)} login={this.login.bind(this)}/>

            }
                break
            case "login":
            default: {
                content = <Login email={email}
                                 password={password}
                                 onFieldChange={this.onFiledChange.bind(this)}
                                 show={this.onShow.bind(this)} login={this.login.bind(this)}/>

            }
        }
        return (
            <section id="main" className="show-recent show-menu">
                <section id="content" style={{}}>
                    <div className="container">
                        {content}
                    </div>
                </section>
                <footer id="footer">
                    Copyright &copy; 2015 Material Admin

                    <ul className="f-menu">
                        <li><a href="">Home</a></li>
                        <li><a href="">Dashboard</a></li>
                        <li><a href="">Reports</a></li>
                        <li><a href="">Support</a></li>
                        <li><a href="">Contact</a></li>
                    </ul>
                </footer>
            </section>
        )
    }
}

Auth.propTypes = {
    doLogin: PropTypes.func.isRequired,
    doEmail: PropTypes.func.isRequired,
    doReset: PropTypes.func.isRequired,
    doRegister: PropTypes.func.isRequired,
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        doLogin(email, password){
            return dispatch(login(email, password))
        },
        doRegister(email, password, name, account_type){
            return dispatch(register(email, password, name, account_type))
        },
        doEmail(email_address){
            return dispatch(email(email_address))
        },
        doReset(password){
            return dispatch(reset(password))
        },

    }
}

export default connect(undefined, mapDispatchToProps)(Auth)