import React, {Component, PropTypes} from 'react'

class Companies extends Component {
    render() {
        return (
            <div className="four-zero">
                <div className="fz-inner">
                    <h2>{this.props.title}</h2>
                    <p>{this.props.message}</p>
                </div>
            </div>
        )
    }
}

Companies.propTypes = {
    title: PropTypes.string.isRequired,
    message: PropTypes.string.isRequired,
}

Companies.defaultProps = {
    title: "Forgot Password",
    message: "We've sent you an email with a link to reset your password.",
}

export default Companies