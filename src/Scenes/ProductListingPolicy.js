import React, {Component} from 'react'
import MainMenu from '../Components/MainMenu'
import ReadableText from '../Components/ReadableText'

class ProductListingPolicy extends Component {
    render() {
        return (
            <section id="main" className="show-menu">
                <MainMenu />
                <ReadableText title="PlusAfrik Product Listing Policy">

                    <p><b>Plus Afrik use of cookies?</b></p>

                    <p>We use cookies and other technologies to ensure everyone who
                        uses Plus Afrik has the best possible experience. Cookies also help us keep
                        your account safe.</p>

                    <p>By continuing to use our services, you are agreeing to the
                        use of cookies and similar technologies for the purposes we describe in this
                        policy.</p>

                    <p><b>What is a cookie?</b></p>

                    <p>A cookie is a small file placed onto your device that
                        enables Plus Afrik features and functionality. For example, cookies enable us
                        to identify your device, secure your access to Plus Afrik sites, and even know
                        if someone attempts to access your account from a different device. Cookies
                        also enable you to easily share content on Plus Afrik and help us serve
                        relevant ads to you.</p>

                    <p><b>When do we use cookies?</b></p>

                    <p>We use cookies on our websites and mobile applications. Any
                        browser loading these sites will receive cookies from us.</p>

                    <p><b>What types of cookies do we use?</b></p>

                    <p><u>Persistent cookies</u> help us recognize you as an
                        existing user, so it's easier to return to your account without signing in
                        again. After you sign in, a persistent cookie stays in your browser and will be
                        read by Plus Afrik when you return to the site. </p>

                    <p><u>Session cookies</u> only last for as long as the session
                        (usually the current visit to a web site or a browser session).</p>

                    <p><b>What are cookies used for?</b></p>

                    <p>Cookies can be used to do lots of different things, like
                        recognize you when you visit Plus Afrik, remember your preferences, and give
                        you a personalized experience that's in line with your settings. Cookies also
                        make your interactions with Plus Afrik more secure and faster. Additionally,
                        cookies allow us to bring you advertising both on and off the Plus Afrik site,
                        and bring customized features to you.</p>

                    <p><b>What we use cookies for:</b></p>

                    <p><u>Authentication:</u> If you're signed in to Plus Afrik,
                        cookies help us show you the right information and personalize your experience.</p>

                    <p><u>Security:</u> We use cookies to enable
                        and support our security features, and to help us detect malicious activity and
                        violations of our User Agreement.</p>

                    <p><u>Preferences, features and services: </u>Cookies can tell
                        us which language you prefer and what your communications preferences are. They
                        can help you fill out forms on Plus Afrik more easily. They also provide you
                        with features, insights, and customized content in conjunction with our
                        plugins. You can learn more about plugins in our Privacy Policy.</p>

                    <p><u>Advertising:</u> We may use cookies to show you relevant
                        advertising both on and off the Plus Afrik site. We may also use a cookie to
                        learn whether members who saw an ad on Plus Afrik later visited the advertiser's
                        site. Similarly, our partners may use a cookie to determine whether we've shown
                        an ad and how it performed, or provide us with information about how you
                        interact with ads. We may also work with a partner to show you an ad on or off
                        Plus Afrik, such as after you've visited a partner's site or application.</p>

                    <p><u>Performance, Analytics and Research</u>: Cookies help us
                        learn how well our site and plugins perform in different locations. We also use
                        cookies to understand, improve, and research products, features, and services,
                        including when you access Plus Afrik from other websites, applications, or
                        devices such as your work computer or your mobile device.</p>

                    <p>To learn more about our cookies, take a look at our cookie
                        table in more detail. Please note that the names of cookies, pixels and other
                        technologies may change over time.</p>

                    <p><b>How are cookies used for advertising purposes?</b></p>

                    <p>Cookies and ad technology such as web beacons, pixels, and
                        anonymous ad network tags help us serve relevant ads to you more effectively.
                        They also help us collect aggregated auditing, research, and reporting for
                        advertisers. Pixels help us understand and improve our service, show you ads on
                        Plus Afrik, and know when content has been shown to you. Note: Because your web
                        browser may request advertisements and web beacons directly from ad network
                        servers, these networks can view, edit, or set their own cookies, just as if
                        you had requested a web page from their site.</p>

                    <p>Although we do not use cookies to create a profile of your
                        browsing behavior on third-party sites, we do use aggregate data from third
                        parties and data from your profile and Plus Afrik activity to show you
                        relevant, interest-based advertising. We do not provide any personal
                        information that we collect to advertisers. You can opt out of off-site and
                        third-party-informed advertising by adjusting your settings. Please note that
                        opting out will not remove advertising from the pages you visit; it will mean
                        that the ads you see will not be matched to your interests.</p>

                    <p><b>Controlling cookies</b></p>

                    <p>Most browsers allow you to control cookies through their
                        settings preferences. However, if you limit the ability of websites to set
                        cookies, you may worsen your overall user experience, since it will no longer
                        be personalized to you. It may also stop you from saving customized settings
                        like login information.</p>

                    <p><b>What to do if you don't want cookies</b></p>

                    <p>If you don't want us to use cookies when you visit Plus
                        Afrik, you can opt out of some cookies on your settings page. In order for us
                        to recognize that you have opted out of cookies, we have to place our Plus
                        Afrik opt-out cookie on your device so we can know not to set other Plus Afrik
                        cookies the next time you visit. If you do not want to receive cookies, you can
                        also change your browser settings on your computer or other device you're using
                        to access our services. If you use Plus Afrik without changing your browser
                        settings, we'll assume that you're happy to receive all cookies on the website.
                        Please note that the Plus Afrik site will not work properly without cookies.</p>

                </ReadableText>
            </section>
        )
    }
}

ProductListingPolicy.propTypes = {}

export default ProductListingPolicy