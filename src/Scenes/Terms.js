import React, {Component} from 'react'
import MainMenu from '../Components/MainMenu'
import ReadableText from '../Components/ReadableText'

class PrivacyPolicy extends Component {
    render() {
        return (
            <section id="main" className="show-menu">
                <MainMenu/>
                <ReadableText title="Application and Acceptance of the Terms">
                    <p>Welcome, and thanks for using Plus Afrik. When you use our
                        services, you're agreeing to our terms, so please take a few minutes to read
                        this User Agreement.</p>

                    <p>Please note that you are entering into a legally binding
                        agreement.</p>

                    <p>Your use of the Plus Afrik services, software and products
                        (collectively the as the “Services” hereinafter) is subject to the terms and
                        conditions contained in this document as well as the Privacy Policy , the Product
                        Listing Policy  and any other rules and policies of the Sites that Plus Afrik
                        may publish from time to time.  This document and such other rules and policies
                        of the Sites are collectively referred to below as the “Terms”.  By accessing
                        the Site or using the Services, you agree to accept and be bound by the Terms.
                        Please do not use the Services or the Sites if you do not accept all of the
                        Terms.  </p>

                    <p>You may not use the Services if:</p>

                    <p>a)You
                        are not of legal age to form a binding contract with Plus Afrik</p>

                    <p>b)You
                        are not permitted to receive any Services under the laws of the country /
                        region in which you are resident or from which you use the Services.</p>

                    <p>You acknowledge and agree that Plus Afrik may amend any
                        Terms at any time by posting the relevant amended and restated Terms on the
                        Sites.  By continuing to use the Services or the Sites, you agree that the
                        amended Terms will apply to you.</p>

                    <p>If Plus Afrik has posted or provided a translation of the
                        English language version of the Terms, you agree that the translation is
                        provided for convenience only and that the English language version will govern
                        your uses of the Services or the Sites. </p>

                    <p>&nbsp;</p>

                    <p><b>Introduction</b></p>

                    <p>Plus Afrik, is a social network and online commerce platform
                        for businesses. We enable business to connect to their customers and showcase
                        their products and services</p>

                    <p>Purpose </p>

                    <p>Our MISSION is to connect the world through trade, through
                        enabling businesses and customers to build lasting connections and meaningful
                        open relationships.</p>

                    <p>Our services are designed to enable selling by helping
                        create connections between sellers and buyers in a network of trusted
                        relationships.</p>

                    <p>&nbsp;</p>

                    <p><b>Obligations &amp; Service Eligibility</b></p>

                    <p>Here are some promises you make to us in this Agreement:</p>

                    <p>You are of the legal age that permits you to enter into
                        binding contract in your country/region</p>

                    <p>You'll keep your password a secret.</p>

                    <p>You will not share an account with anyone else and will
                        follow our rules and the law.</p>

                    <p>As between you and others, your account belongs to you.</p>

                    <p>You agree to: </p>

                    <p>a) try to choose a strong and secure
                        password; </p>

                    <p>b) keep your password secure and
                        confidential; </p>

                    <p>c) take full responsibility for
                        your account and anyone to whom you assign any management role of that account</p>

                    <p>d) follow the law and the Dos and
                        Don'ts below. You are responsible for anything that happens through your
                        account unless you close it or report misuse. 1   As a condition of your access
                        to and use of the Sites or Services, you agree that you will comply with all
                        applicable laws and regulations when using the Sites or Services.</p>

                    <p>You agree that:</p>

                    <p>a) you will not copy, reproduce,
                        download, re-publish, sell, distribute or resell any Services or any
                        information, text, images, graphics, video clips, sound, directories, files,
                        databases or listings, etc available on or through the Sites (the “Site
                        Content”), and </p>

                    <p>b) you will not copy, reproduce,
                        download, compile or otherwise use any Site Content for the purposes of
                        operating a business that competes with Plus Afrik or otherwise commercially
                        exploiting the Site Content.</p>

                    <p>You accept the terms of the Privacy Policy and agree to the
                        use of the personal information about you in accordance with the Privacy
                        Policy.</p>

                    <p>Plus Afrik may allow users to access to content, products or
                        services offered by third parties through hyperlinks (in the form of word link,
                        banners, channels or otherwise), API or otherwise to such third parties' web
                        sites. You are cautioned to read such web sites' terms and conditions and/or
                        privacy policies before using the sites. </p>

                    <p>You acknowledge that Plus Afrik has no control over such third
                        parties' web sites, does not monitor such web sites, and shall not be
                        responsible or liable to anyone for such web sites, or any content, products or
                        services made available on such web sites.</p>

                    <p>You agree not to undertake any action to undermine the integrity
                        of the computer systems or networks of Plus Afrik and/or any other User nor to
                        gain unauthorized access to such computer systems or networks.</p>

                    <p>You agree not to undertake any action which may undermine
                        the integrity of Plus Afrik’s feedback system, such as leaving positive
                        feedback for yourself using secondary Member IDs or through third parties or by
                        leaving unsubstantiated negative feedback for another User.</p>

                    <p>By posting or displaying any information, content or
                        material on the Site, you grant an irrevocable, perpetual, worldwide,
                        royalty-free, and sub-licensable (through multiple tiers) license to Plus Afrik
                        to display, transmit, distribute, reproduce, publish, duplicate, adapt, modify,
                        translate, create derivative works, and otherwise use any or all of the User
                        Content in any form, media, or technology now known or not currently known in
                        any manner and for any purpose which may be beneficial to the operation of the
                        Site, the provision of any Services and/or the business of the User. You confirm
                        and warrant to Plus Afrik that you have all the rights, power and authority
                        necessary to grant the above license. </p>

                    <p>Each Member further represents, warrants and agrees that the
                        content that you submit, post or display shall:</p>

                    <p>a) be true, accurate, complete and
                        lawful;</p>

                    <p>b)  not be false, misleading or
                        deceptive;</p>

                    <p>c) not contain information that is
                        defamatory, libelous, threatening or harassing, obscene, objectionable,
                        offensive, sexually explicit or harmful to minors;</p>

                    <p>d)  not contain information that is
                        discriminatory or promotes discrimination based on race, sex, religion,
                        nationality, disability, sexual orientation or age;</p>

                    <p>e) not violate the Product Listing
                        Policy, other Terms or any applicable Additional Agreements</p>

                    <p>f) not violate any applicable laws
                        and regulations or promote any activities which may violate any applicable laws
                        and regulations;</p>

                    <p>g) not contain any link directly or
                        indirectly to any other web Sites which includes any content that may violate
                        the Terms.</p>

                    <p>Each Member further represents, warrants and agrees that you
                        shall:</p>

                    <p>a) Carry on your activities on the
                        Sites in compliance with any applicable laws and regulations;</p>

                    <p>b) Conduct your business
                        transactions with other users of the Sites in good faith;</p>

                    <p>c) Carry on your activities in
                        accordance with the Terms and any applicable Additional Agreements;</p>

                    <p>d) Not use the Services or Sites to
                        defraud any person or entity (including without limitation sale of stolen
                        items, use of stolen credit/debit cards);</p>

                    <p>e) Not impersonate any person or
                        entity, misrepresent yourself or your affiliation with any person or entity;</p>

                    <p>f) Not engage in spamming or
                        phishing;</p>

                    <p>g) Not engage in any other unlawful
                        activities (including without limitation those which would constitute a
                        criminal offence, give rise to civil liability, or encourage or abet any
                        unlawful activities;</p>

                    <p>h) Not involve attempts to copy,
                        reproduce, exploit or expropriate Plus Afrik’s various proprietary directories,
                        databases and listings;</p>

                    <p>i) Not involve any computer viruses
                        or other destructive devices and codes that have the effect of damaging,
                        interfering with, intercepting or expropriating any software or hardware
                        system, data or personal information;</p>

                    <p>j) Not involve any scheme to
                        undermine the integrity of the data, systems or networks used by Plus Afrik and/or
                        any user of the Sites or gain unauthorized access to such data, systems or
                        networks;</p>

                    <p>k) Not engage in any activities
                        that would otherwise create any liability for Plus Afrik or our affiliates.  </p>

                    <p>If Member provides a business referee, Member represents,
                        warrants and agrees that you have obtained all necessary consents, approvals
                        and waivers from your business partners and associates to:</p>

                    <p>(a) act as your business’ referee; </p>

                    <p>(b) post and publish their contact
                        details and information, reference letters and comments on their behalf; and </p>

                    <p>(c) that third parties may contact
                        such business referees to support claims or statements made about you. You
                        further warrant and agree that all reference letters and comments are true and
                        accurate and third parties may contact the business referees without the need
                        to obtain your consent.</p>

                    <p>Member agrees to provide all necessary information,
                        materials and approval, and render all reasonable assistance and cooperation
                        necessary for Plus Afrik’s provision of the Services. Member’s failure to do so
                        results in delay in, or suspension or termination of, the provision of any
                        Service. Plus Afrik shall not be obliged to extend the relevant service period
                        nor shall be liable for any loss or damages arising from such delay, suspension
                        or termination.</p>

                    <p>Member acknowledges and agrees that Plus Afrik shall not be
                        required to actively monitor nor exercise any editorial control whatsoever over
                        the content of any message or material or information created, obtained or
                        accessible through the Services or Sites. Plus Afrik does not endorse, verify
                        or otherwise certify the contents of any comments or other material or
                        information made by any Member. Each Member is solely responsible for the
                        contents of their communications and may be held legally liable or accountable
                        for the content of their comments or other material or information</p>

                    <p>Member acknowledges and agrees that Plus Afrik is a business
                        platform and anything posted on the platform must be for the furtherance of the
                        member’s or others’ business activities.</p>

                    <p>Member acknowledges and agrees that each Member is solely
                        responsible for observing applicable laws and regulations in its respective
                        jurisdictions to ensure that all use of the Site and Services are in compliance
                        with the same.</p>

                    <p><b>Payment</b></p>

                    <p>You'll honor your payment obligations and you are okay with
                        us storing your payment information. Also, there may be fees and taxes that are
                        added to our prices.</p>

                    <p>We don't guarantee refunds.</p>

                    <p>If you purchase any of our paid Services, you agree to pay
                        us the applicable fees and taxes. Failure to pay these fees may result in the
                        termination of your subscription. </p>

                    <p>Also:</p>

                    <p>Your purchase may be subject to foreign exchange fees or
                        differences in prices based on location (e.g. exchange rates).</p>

                    <p>You authorize us to store and continue billing your payment
                        method (e.g. credit card) even after it has expired, to avoid interruptions in
                        your service (e.g. subscriptions) and to facilitate easy payment for new
                        services.</p>

                    <p>You must pay us for applicable fees and taxes unless you
                        cancel the paid service, in which case you agree to still pay these fees
                        through the end of the applicable subscription period.</p>

                    <p>Taxes are calculated based on the billing information that
                        you provide us at the time of purchase.</p>

                    <p>&nbsp;</p>

                    <p><b>Breaches by Members</b></p>

                    <p>Plus Afrik reserves the right in our sole discretion to
                        remove, modify or reject any User Content that you submit to, post or display
                        on the Sites which we reasonably believe is unlawful, violates the Terms, could
                        subject Plus Afrik or our affiliates to liability, or is otherwise found
                        inappropriate in our opinion.</p>

                    <p>If any Member breaches any Terms or if Plus Afrik has
                        reasonable grounds to believe that any Member is in breach of any the Terms,
                        Plus Afrik shall have the right to impose a penalty against the Member, or suspend
                        or terminate the Member’s account or subscription of any Service without any
                        liability to the Member.  Plus Afrik shall also have the right to restrict,
                        refuse or ban any and all current or future use of any other Service that may
                        be provided by Plus Afrik.</p>

                    <p>A Member shall be in breach of this contract if any
                        information provided by the Member is not current or complete or is untrue,
                        inaccurate, or misleading.</p>

                    <p>Plus Afrik reserves the right to cooperate fully with
                        governmental authorities in the investigation of any suspected criminal or
                        civil wrongdoing.  Furthermore, Plus Afrik may disclose the Member's identity
                        and contact information, if requested by a government or law enforcement body,
                        or as a result of a subpoena or other legal action.  Plus Afrik shall not be
                        liable for damages or results arising from such disclosure, and Member agrees
                        not to bring any action or claim against Plus Afrik for such disclosure.</p>

                    <p>If a Member is in breach of the Terms, Plus Afrik also
                        reserves the right to publish the records of such breach on the Sites.</p>

                    <p>Each Member agrees to indemnify Plus Afrik, our affiliates,
                        directors, employees, agents and representatives and to hold them harmless,
                        from any and all damages, losses, claims and liabilities (including legal costs
                        on a full indemnity basis) which may arise from your submission, posting or
                        display of any User Content, from your use of the Sites or Services, or from
                        your breach of the Terms.</p>

                    <p>Each Member further agrees that Plus Afrik is not
                        responsible, and shall have no liability to you or anyone else for any User
                        Content or other material transmitted over the Sites, including fraudulent,
                        untrue, misleading, inaccurate, defamatory, offensive or illicit material and
                        that the risk of damage from such material rests entirely with each Member. </p>

                    <p>&nbsp;</p>

                    <p><b>Transactions Between Buyers and Sellers</b></p>

                    <p>Plus Afrik provides electronic web-based platforms for
                        exchanging information between buyers and sellers of products and services.
                        Plus Afrik additionally will provide electronic web-based transaction platforms
                        for Members to place, accept, conclude, manage and fulfill orders for the
                        provision of products and services online within the Sites.</p>

                    <p>Plus Afrik does not represent either the seller or the buyer
                        in specific transactions. </p>

                    <p>Plus Afrik does not control and is not liable to or
                        responsible for the quality, safety, lawfulness or availability of the products
                        or services offered for sale on the Sites or the ability of the sellers to
                        complete a sale or the ability of buyers to complete a purchase.</p>

                    <p>Users are hereby made aware that there may be risks of
                        dealing with people acting under false pretenses. </p>

                    <p>Plus Afrik uses several techniques to verify the accuracy of
                        certain information our paying users provide us when they register for a paying
                        membership service on the site.  However, because user verification on the Internet
                        is difficult, Plus Afrik cannot and does not confirm each User's purported
                        identity (including, without limitation, paying Members). We encourage you to
                        use various means, as well as common sense, to evaluate with whom you are
                        dealing.</p>

                    <p>Each User acknowledges that they are fully assuming the
                        risks of conducting any purchase and sale transactions in connection with using
                        the Sites or Services, and that it is fully assuming the risks of liability or
                        harm of any kind in connection with subsequent activity of any kind relating to
                        products or services that are the subject of transactions using the Sites. Such
                        risks shall include, but are not limited to:</p>

                    <p> mis-representation of products and
                        services, fraudulent schemes, unsatisfactory quality, failure to meet
                        specifications, defective or dangerous products, unlawful products, delay or
                        default in delivery or payment, cost mis-calculations, breach of warranty,
                        breach of contract and transportation accidents. Such risks also include the
                        risks that the manufacture, importation, export, distribution, offer, display,
                        purchase, sale and/or use of products or services offered or displayed on the
                        Sites may violate or may be asserted to violate Third Party Rights, and the
                        risk that User may incur costs of defense or other costs in connection with
                        third parties’ assertion of Third Party Rights, or in connection with any
                        claims by any party that they are entitled to defense or indemnification in
                        relation to assertions of rights, demands or claims by Third Party Rights
                        claimants. Such risks also include the risks that consumers, other purchasers,
                        end-users of products or others claiming to have suffered injuries or harms
                        relating to products originally obtained by Users of the Sites as a result of
                        purchase and sale transactions in connection with using the Sites may suffer
                        harms and/or assert claims arising from their use of such products. All of the
                        foregoing risks are hereafter referred to as &quot;Transaction Risks&quot;. Each
                        User agrees that Plus Afrik shall not be liable or responsible for any damages,
                        claims, liabilities, costs, harms, inconveniences, business disruptions or
                        expenditures of any kind that may arise as a result of or in connection with
                        any Transaction Risks.</p>

                    <p>Users are solely responsible for all of the terms and
                        conditions of the transactions conducted on, through or as a result of use of
                        the Sites or Services, including, without limitation, terms regarding payment,
                        returns, warranties, shipping, insurance, fees, taxes, title, licenses, fines,
                        permits, handling, transportation and storage.</p>

                    <p>User agrees to provide all information and materials as may
                        be reasonably required by Plus Afrik in connection with your transactions
                        conducted on, through or as a result of use of the Sites or Services. Plus
                        Afrik has the right to suspend or terminate any User’s account if the User
                        fails to provide the required information and materials.</p>

                    <p>In the event that any User has a dispute with any party to a
                        transaction, such User agrees to release and indemnify Plus Afrik (and our
                        agents, affiliates, directors, officers and employees) from all claims,
                        demands, actions, proceedings, costs, expenses and damages (including without
                        limitation any actual, special, incidental or consequential damages) arising
                        out of or in connection with such transaction</p>

                    <p>&nbsp;</p>

                    <p><b>Notices and Service Messages</b></p>

                    <p>You're okay with us using our websites, mobile apps, and
                        email to provide you with important notices. This Agreement applies to mobile
                        applications as well. Also, you agree certain additional information can be
                        shared with us.</p>

                    <p><b>Your License to Plus Afrik</b></p>

                    <p>You own all of the content, feedback, and personal
                        information you provide to us, but you also grant us a non-exclusive license to
                        it.</p>

                    <p>We'll honor the choices you make about who gets to see your
                        information and content.</p>

                    <p>You promise to only provide information and content that you
                        have the right to share, and that your Plus Afrik profile will be truthful.</p>

                    <p>As between you and Plus Afrik, you own the content and
                        information that you submit or post to the Services and you are only granting
                        Plus Afrik the following non-exclusive license: A worldwide, transferable and
                        sublicensable right to use, copy, modify, distribute, publish, and process,
                        information and content that you provide through our Services, without any
                        further consent, notice and/or compensation to you or others. These rights are
                        limited in the following ways:</p>

                    <p>You can end this license for specific content by deleting
                        such content from the Services, or generally by closing your account, except
                        (a) to the extent you shared it with others as part of the Service and they
                        copied or stored it and (b) for the reasonable time it takes to remove from
                        backup and other systems.</p>

                    <p>By submitting suggestions or other feedback regarding our
                        Services to Plus Afrik, you agree that Plus Afrik can use and share such
                        feedback for any purpose without compensation to you.</p>

                    <p>You agree to only provide content or information if that
                        does not violate the law nor anyone's rights (e.g., without violating any
                        intellectual property rights or breaching a contract). </p>

                    <p>You also agree that your profile information will be
                        truthful. Plus Afrik may be required by law to remove certain information or
                        content in certain countries.</p>

                    <p><b>Service Availability</b></p>

                    <p>We may change or discontinue any of our Services. We can't
                        promise to store or keep showing any information and content you've posted.</p>

                    <p>We may change, suspend or end any Service, or change and
                        modify prices prospectively in our discretion and these changes may be
                        effective upon notice provided to you.</p>

                    <p>Plus Afrik is not a storage service. You agree that we have
                        no obligation to store, maintain or provide you a copy of any content or
                        information that you or others provide.</p>

                    <p>&nbsp;</p>

                    <p><b>Other Content, Sites and apps</b></p>

                    <p>When you see or use others' content and information posted
                        on our Services, it's at your own risk.</p>

                    <p>Third parties may offer their own products and services
                        through Plus Afrik, and we aren't responsible for those third-party activities.</p>

                    <p>By using the Services, you may encounter content or
                        information that might be inaccurate, incomplete, delayed, misleading, illegal,
                        offensive or otherwise harmful. Plus Afrik generally does not review content
                        provided by our Members. You agree that we are not responsible for third
                        parties' (including other Members') content or information or for any damages
                        as result of your use of or reliance on it.</p>

                    <p>You are responsible for deciding if you want to access or
                        use third party apps or sites that link from our Services. If you allow a third
                        party app or site to authenticate you or connect with your Plus Afrik account,
                        that app or site can access information on Plus Afrik related to you and your
                        connections. Third party apps and sites have their own legal terms and privacy
                        policies, and you may be giving others permission to use your information in
                        ways we would not. Plus Afrik is not responsible for these other sites and apps
                        -- use these at your own risk.</p>

                    <p>&nbsp;</p>

                    <p><b>Limits</b></p>

                    <p>We have the right to limit how you connect and interact on
                        our Services.</p>

                    <p>We're providing you notice about our intellectual property
                        rights.</p>

                    <p>Plus Afrik reserves the right to limit your use of the
                        Services, including the number of your connections and your ability to contact
                        other Members. Plus Afrik reserves the right to restrict, suspend, or terminate
                        your account if Plus Afrik believes that you may be in breach of this Agreement
                        or law or are misusing the Services.</p>

                    <p>Plus Afrik reserves all of its intellectual property rights
                        in the Services.</p>

                    <p>&nbsp;</p>

                    <p><b>Limitation of Liability</b></p>

                    <p> </p>

                    <p>TO THE MAXIMUM EXTENT PERMITTED BY LAW, THE SERVICES
                        PROVIDED BY PLUS AFRIK ON OR THROUGH THE SITES ARE PROVIDED &quot;AS IS&quot;,
                        &quot;AS AVAILABLE&quot; AND “WITH ALL FAULTS”, AND PLUS AFRIK HEREBY EXPRESSLY
                        DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
                        TO, ANY WARRANTIES OF CONDITION, QUALITY, DURABILITY, PERFORMANCE, ACCURACY,
                        RELIABILITY, MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. ALL SUCH
                        WARRANTIES, REPRESENTATIONS, CONDITIONS, AND UNDERTAKINGS ARE HEREBY EXCLUDED.</p>

                    <p>TO THE MAXIMUM EXTENT PERMITTED BY LAW, PLUS AFRIK MAKES NO
                        REPRESENTATIONS OR WARRANTIES ABOUT THE VALIDITY, ACCURACY, CORRECTNESS,
                        RELIABILITY, QUALITY, STABILITY, COMPLETENESS OR CURRENTNESS OF ANY INFORMATION
                        PROVIDED ON OR THROUGH THE SITES; PLUS AFRIK DOES NOT REPRESENT OR WARRANT THAT
                        THE MANUFACTURE, IMPORTATION, EXPORT, DISTRIBUTION, OFFER, DISPLAY, PURCHASE,
                        SALE AND/OR USE OF PRODUCTS OR SERVICES OFFERED OR DISPLAYED ON THE SITES DOES
                        NOT VIOLATE ANY THIRD PARTY RIGHTS; AND PLUS AFRIK MAKES NO REPRESENTATIONS OR
                        WARRANTIES OF ANY KIND CONCERNING ANY PRODUCT OR SERVICE OFFERED OR DISPLAYED
                        ON THE SITES.</p>

                    <p>Any material downloaded or otherwise obtained through the
                        Sites is done at each User's sole discretion and risk and each User is solely
                        responsible for any damage to Plus Afrik computer system or loss of data that
                        may result from the download of any such material. No advice or information,
                        whether oral or written, obtained by any User from Plus Afrik or through or
                        from the Sites shall create any warranty not expressly stated herein.</p>

                    <p>The Sites may make available to User services or products
                        provided by independent third parties.   No warranty or representation is made
                        with regard to such services or products.   In no event shall Plus Afrik and
                        our affiliates be held liable for any such services or products.</p>

                    <p>Each User hereby agrees to indemnify and save Plus Afrik,
                        our affiliates, directors, officers and employees harmless, from any and all
                        losses, claims, liabilities (including legal costs on a full indemnity basis) which
                        may arise from such User's use of the Sites or Services (including but not
                        limited to the display of such User's information on the Sites) or from your
                        breach of any of the terms and conditions of the Terms. Each User hereby
                        further agrees to indemnify and save Plus Afrik, our affiliates, directors,
                        officers and employees harmless, from any and all losses, damages, claims,
                        liabilities (including legal costs on a full indemnity basis) which may arise
                        from User's breach of any representations and warranties made by User to Plus
                        Afrik.  </p>

                    <p>Each User hereby further agrees to indemnify and Plus Afrik,
                        our affiliates, directors, officers and employees harmless, from any and all
                        losses, damages, claims, liabilities (including legal costs on a full indemnity
                        basis) which may arise, directly or indirectly, as a result of any claims
                        asserted by Third Party Rights claimants or other third parties relating to
                        products offered or displayed on the Sites. Each User hereby further agrees
                        that Plus Afrik is not responsible and shall have no liability to you, for any
                        material posted by others, including defamatory, offensive or illicit material
                        and that the risk of damages from such material rests entirely with each User.
                        Plus Afrik reserves the right, to assume the exclusive defense and control of
                        any matter otherwise subject to indemnification by you, in which event you
                        shall cooperate with Plus Afrik in asserting any available defenses.</p>

                    <p>Plus Afrik shall not be liable for any special, direct,
                        indirect, punitive, incidental or consequential damages or any damages
                        whatsoever (including but not limited to damages for loss of profits or
                        savings, business interruption, loss of information), whether in contract,
                        negligence, tort, equity or otherwise or any other damages resulting from any
                        of the following.</p>

                    <p>a) the use or the inability to use
                        the Sites or Services;</p>

                    <p>b) any defect in goods, samples,
                        data, information or services purchased or obtained from a User or any other
                        third party through the Sites;</p>

                    <p>c) violation of Third Party Rights
                        or claims or demands that User's manufacture, importation, export,
                        distribution, offer, display, purchase, sale and/or use of products or services
                        offered or displayed on the Sites may violate or may be asserted to violate
                        Third Party Rights; or claims by any party that they are entitled to defense or
                        indemnification in relation to assertions of rights, demands or claims by Third
                        Party Rights claimants;</p>

                    <p>d) unauthorized access by third
                        parties to data or private information of any User;</p>

                    <p>e) statements or conduct of any
                        User of the Sites; or;</p>

                    <p>f) any matters relating to Services
                        however arising, including negligence.</p>

                    <p>Notwithstanding any of the foregoing provisions, the aggregate
                        liability of Plus Afrik, our employees, agents, affiliates, representatives or anyone
                        acting on our behalf with respect to each User for all claims arising from the
                        use of the Sites or Services during any calendar year shall be limited to the
                        greater of:</p>

                    <p>(a) the amount
                        of fees the User has paid to Plus Afrik or our affiliates during the calendar
                        year and </p>

                    <p>(b) the maximum
                        amount permitted in the applicable law. The preceding sentence shall not
                        preclude the requirement by the User to prove actual damages. All claims
                        arising from the use of the Sites or Services must be filed within one (1) year
                        from the date the cause of action arose or such longer period as prescribed
                        under any applicable law governing this Term of Use..</p>

                    <p>The limitations and exclusions of liability to you under the
                        Terms shall apply to the maximum extent permitted by law and shall apply
                        whether or not Plus Afrik has been advised of or should have been aware of the
                        possibility of any such losses arising.</p>

                    <p>&nbsp;</p>

                    <p><b>Force Majeure</b></p>

                    <p>Under no circumstances shall Plus Afrik be held liable for
                        any delay or failure or disruption of the content or services delivered through
                        the Sites resulting directly or indirectly from acts of nature, forces or
                        causes beyond our reasonable control, including without limitation, Internet
                        failures, computer, telecommunications or any other equipment failures,
                        electrical power failures, strikes, labor disputes, riots, insurrections, civil
                        disturbances, shortages of labor or materials, fires, flood, storms,
                        explosions, acts of God, war, governmental actions, orders of domestic or foreign
                        courts or tribunals or non-performance of third parties.</p>

                    <p> </p>

                    <p><b>Intellectual Property Rights</b></p>

                    <p>Plus Afrik is the sole owner or lawful licensee of all the
                        rights and interests in the Sites and the Site Content. The Sites and Site
                        Content embody trade secrets and other intellectual property rights protected
                        under worldwide copyright and other laws. All title, ownership and intellectual
                        property rights in the Sites and Site Content shall remain with Plus Afrik, our
                        affiliates or licensors of the Site Content, as the case may be. All rights not
                        otherwise claimed under the Terms or by Plus Afrik are hereby reserved.</p>

                    <p>&nbsp;</p>

                    <p><b>General Provisions</b></p>

                    <p>Subject to any Additional Agreements, the Terms constitute
                        the entire agreement between you and Plus Afrik with respect to and govern your
                        use of the Sites and Services, superseding any prior written or oral agreements
                        in relation to the same subject matter herein.</p>

                    <p>Plus Afrik and you are independent contractors, and no
                        agency, partnership, joint venture, employee-employer or franchiser-franchisee
                        relationship is intended or created by the Terms.</p>

                    <p>If any provision of the Terms is held to be invalid or
                        unenforceable, such provision shall be deleted and the remaining provisions
                        shall remain valid and be enforced.</p>

                    <p>Headings are for reference purposes only and in no way
                        define, limit, construe or describe the scope or extent of such section.</p>

                    <p>Plus Afrik’s failure to enforce any right or failure to act
                        with respect to any breach by you under the Terms will not constitute a waiver
                        of that right nor a waiver of Plus Afrik’s right to act with respect to
                        subsequent or similar breaches.</p>

                    <p>Plus Afrik shall have the right to assign the Terms
                        (including all of our rights, titles, benefits, interests, and obligations and
                        duties in the Terms to any person or entity (including any affiliates Plus
                        Afrik). You may not assign, in whole or part, the Terms to any person or
                        entity.</p>
                </ReadableText>
            </section>
        )
    }
}

PrivacyPolicy.propTypes = {}

export default PrivacyPolicy