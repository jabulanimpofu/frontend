import React, {Component} from 'react'
import {connect} from 'react-redux'
import PricingTable from '../Components/PricingTable'
import Jumbotron from '../Components/Jumbotron'

const prices = [
    {
        id: 1,
        price: '$49.00',
        period: 'year',
        name: 'BASIC PACKAGE',
        color: 'cyan',
        features: [
            'Identity verification',
            'Authentication',
            'Verified icon',
            'Gold seller badge',
            'Unlimited Product posting',
            'Ability to INVITE ideal customers',
            '200 free invites per year on subscription',
            'Products displayed in the mall',
            'Priority ranking in the mall 3rd',
        ]

    },
    {
        id: 2,
        price: '$199',
        period: 'year',
        name: 'STANDARD PACKAGE',
        color: 'green',
        features: [
            'Authentication',
            'Verified icon',
            'Gold seller badge',
            'Unlimited Product posting',
            'Ability to INVITE ideal customers',
            '500 free invites per year on subscription',
            'Products displayed in the mall',
            'Priority ranking in the mall 2nd',
        ]

    },
    {
        id: 3,
        price: '$599',
        period: 'year',
        name: 'PREMIUM PACKAGE',
        color: 'amber',
        features: [
            'Identity verification',
            'Authentication',
            'Verified icon',
            'Gold seller badge',
            'Unlimited Product posting',
            'Personalized customer service',
            'Ability to INVITE ideal customers',
            '2000 free customer invites on subscription',
            'Products displayed in the mall',
            'Priority ranking in the mall 1st',
        ]

    },
]

class GoldSeller extends Component {

    componentDidMount() {

    }

    componentDidUpdate(prevProps) {
    }

    render() {
        return (
            <section id="main" className="" style={{padding: 0}}>
                <section id="content">
                    <div className="container c'boxed">
                        <Jumbotron>
                            <h1>Gold Seller Program!</h1>
                            <p>Gold Seller is a premium Membership plan on that helps you build you business image, increase
                                exposure and maximize business opportunities.</p>

                            <p>To become a Gold Seller members must subscribe to the service and pass a verification and
                                authentication process after which they will receive a gold seller badge.</p>

                            <p> Stand out from your competitors, gain immediate trust and recognition as a legitimate and
                                serious
                                trading partner. Become a Gold Seller today..</p>
                        </Jumbotron>
                        <PricingTable prices={prices}/>
                    </div>
                </section>
            </section>
        )
    }
}

GoldSeller.propTypes = {}

const mapStateToProps = (state, ownProps) => {
    return {}
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(GoldSeller)