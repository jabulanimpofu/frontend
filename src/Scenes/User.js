import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import {Link, Route} from 'react-router-dom'
import Recent from '../Components/Recent'
import ProfilePosts from '../Components/ProfilePosts'
import RecommendedSellers from '../Containers/RecommendedSellers'
import FeaturedProducts from '../Containers/FeaturedProducts'
import Collections from '../Components/Collections'
import {AddReview} from '../Components/Reviews'
import Endorsements from '../Components/Endorsements'
import ProductsSearch from '../Components/ProductsSearch'

const products = {
    data: [1, 2, 3, 4, 5, 6,].map((review, index) => ({
        object: {},
        id: index
    }))
}

import {
    save,
    comment,
    share,
    like,
    follow,
    invite,
    getActive,
    getFeatured,
    getRecentActivity,
    getUser,
} from '../Actions/Api'

class ProfileHeader extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const {user, match: {path, url}, location: {pathname}} = this.props
        const _profile_pic_url =  "/img/profile-pics/profile-pic-2.jpg"
        const _cover_pic_url = '/img/headers/1.png'
        return (
            <div className="card" id="profile-main">

                <div className="pm-overview c-overflow">
                    <div className="pmo-pic">
                        <div className="p-relative">
                            <a href="">
                                <img className="img-responsive" src={_profile_pic_url}
                                     alt=""/>
                            </a>

                            <div className="dropdown pmop-message">
                                <a data-toggle="dropdown" href=""
                                   className="btn palette-White bg btn-float z-depth-1">
                                    <i className="zmdi zmdi-comment-text-alt"></i>
                                </a>

                                <div className="dropdown-menu">
                                    <textarea placeholder="Write message..."></textarea>

                                    <button className="btn bgm-green btn-float"><i
                                        className="zmdi zmdi-mail-send"></i></button>
                                </div>
                            </div>
                        </div>


                        <div className="pmo-stat">
                            <h2 className="m-0 ">{user.data.name}</h2>
                            <h4 className="" style={{margin: '0 0 8px 0'}}>{user.data.account_type}</h4>
                            <div style={{margin: '0 0 8px 0'}}>
                                <Link to={`${path}/reviews`} className="btn btn-default btn-icon-text waves-effect"><i
                                    className="zmdi zmdi-thumb-up"></i>0 Verified reviews
                                </Link>
                            </div>
                            <div style={{margin: '0 0 8px 0'}}>
                                <Link to={`${path}/followers`} className="btn btn-default btn-icon-text waves-effect"><i
                                    className="zmdi zmdi-thumb-up"></i>1 Followers
                                </Link>
                            </div>
                            <div style={{margin: '0 0 8px 0'}}>
                                <Link to={`${path}/endorsements`} className={`btn btn-icon-text waves-effect ${pathname === path+'/endorsements' ? 'btn-primary' : 'btn-default'}`}><i
                                    className="zmdi zmdi-thumb-up"></i>1 endorsements
                                </Link>

                            </div>
                        </div>
                    </div>
                </div>

                <div className="pm-body clearfix" style={{
                    backgroundImage: `url('${_cover_pic_url}')`,
                    backgroundRepeat: 'no-repeat',
                    backgroundSize: 'cover',
                    backgroundPosition: 'center'
                }}>
                    <div className="bottom-nav btn-group btn-group-justified" role="group" aria-label="...">
                        <div className="btn-group" role="group">
                            <Link to={`${url}/home`} type="button" className={`btn btn-icon-text waves-effect ${pathname === url+'/home' || pathname === url  ? 'btn-primary' : 'btn-default'}`}>My feed</Link>
                        </div>
                        <div className="btn-group" role="group">
                            <Link to={`${url}/products`} type="button" className={`btn btn-icon-text waves-effect ${pathname === url+'/products' ? 'btn-primary' : 'btn-default'}`}>Products</Link>
                        </div>
                        <div className="btn-group" role="group">
                            <Link to={`${url}/services`} type="button" className={`btn btn-icon-text waves-effect ${pathname === url+'/services' ? 'btn-primary' : 'btn-default'}`}>Services</Link>
                        </div>
                        <div className="btn-group" role="group">
                            <Link to={`${url}/collections`} type="button" className={`btn btn-icon-text waves-effect ${pathname === url+'/collections' ? 'btn-primary' : 'btn-default'}`}>Saved Items</Link>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}


const ProfileContactInfo = (props) => {
    return (
        <div className="card">
            <div className="card-header">
                <h2>Contact Information
                    <small>Fusce eget dolor id justo luctus commodo vel pharetra nisi. Donec velit libero</small>
                </h2>
            </div>
            <div className="card-body card-padding">
                <div className="pmo-contact">
                    <ul>
                        <li className="ng-binding"><i className="zmdi zmdi-phone"></i> 00971123456789</li>
                        <li className="ng-binding"><i className="zmdi zmdi-email"></i> malinda.h@gmail.com</li>
                        <li className="ng-binding"><i className="zmdi zmdi-facebook-box"></i> malinda.hollaway</li>
                        <li className="ng-binding"><i className="zmdi zmdi-twitter"></i> @malinda (twitter.com/malinda)
                        </li>
                        <li>
                            <i className="zmdi zmdi-pin"></i>
                            <address className="m-b-0 ng-binding">
                                44-46 Morningside Road,<br/>
                                Edinburgh,<br/>
                                Scotland
                            </address>
                        </li>
                    </ul>
                </div>

                <a className="pmo-map" href="">
                    <img src="/img/demo/map.png" alt=""/>
                </a>
            </div>
        </div>
    )
}

class Profile extends Component {

    constructor(props) {
        super(props)
        this.state = {
            profile_picture: null,
            cover_picture: null,
        }
    }

    componentDidMount() {
        const {
            view,
            match,
            profile_id,
            user_id,
            doGetActive,
            doGetFeatured,
            doGetUser,
        } = this.props

        // alert(`${match.url}/home`)

        //has id
        if (user_id) {
            doGetActive()
            doGetFeatured()
        }

        doGetUser(profile_id)
    }

    componentDidUpdate(prevProps) {
        const {
            profile_id,
            user_id,
            doGetActive,
            doGetFeatured,
        } = this.props

        //new id
        if (user_id && (prevProps.user_id !== user_id)) {
            doGetActive()
            doGetFeatured()
        }
    }

    render() {
        const {profile, user, match, location, recent_activity, active_users, featured, doComment, doSave, doShare, doLike, doFollow, doConnect, doGetRecentActivity} = this.props
        return (
            <section id="main" className="show-recent">
                <Recent profile={profile} doGetRecentActivity={doGetRecentActivity} recent_activity={recent_activity}/>
                <section id="content">
                    <div className="container">

                        <ProfileHeader user={user} match={match}  location={location}/>

                        <div className="row">
                            <div className="col-lg-4 hidden-md hidden-sm hidden-xs">
                                <ProfileContactInfo/>
                                <div>
                                    <div className="card-body card-padding">
                                        <span className="block" style={{textAlign: 'center'}}><a href="#">Copyright PlusAfrik 2016</a></span>
                                        <span className="block"><a href="#">About</a> | <a href="#">Terms</a> | <a
                                            href="#">Privacy</a> | <a
                                            href="#">Feedback</a></span>
                                        <span className="block"><a href="#">Product Listing</a> | <a href="#">Advertising Policy</a></span>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-8 co-xs-12">
                                <Route path={`${match.url}`} exact={true} component={ProfilePosts}/>
                                <Route path={`${match.url}/home`} component={ProfilePosts}/>
                                <Route path={`${match.url}/products`}  render={(props) => {
                                    return (<ProductsSearch products={products} title="viewing 100 products" />)
                                }}/>
                                <Route path={`${match.url}/posts`} component={ProfilePosts}/>
                                <Route path={`${match.url}/services`}  render={(props) => {
                                    return (<ProductsSearch products={products} title="viewing 100 services" />)
                                }}/>
                                <Route path={`${match.url}/collections`} component={Collections}/>
                                <Route path={`${match.url}/reviews`} component={AddReview}/>
                                <Route path={`${match.url}/endorsements`} component={Endorsements}/>
                            </div>
                        </div>

                    </div>
                </section>
            </section>
        )
    }

}

Profile.propTypes = {}

const mapStateToProps = (state, ownProps) => {
    const {collections, profile, user} = state
    const user_id = profile.data.id
    const {profile_id, view} = ownProps.match.params
    const {recent_activity, active_users, featured} = collections

    return {
        user_id,
        profile_id,
        view,
        recent_activity,
        active_users,
        featured,
        user,
        profile
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        doGetUser(id){
            return dispatch(getUser(id))
        },
        doGetRecentActivity(){
            return dispatch(getRecentActivity())
        },
        doGetActive(){
            return dispatch(getActive())
        },
        doGetFeatured(type){
            return dispatch(getFeatured(type))
        },

        doLike(post_id){
            return dispatch(like(post_id))
        },
        doShare(post_id){
            return dispatch(share(post_id))
        },
        doSave(post_id){
            return dispatch(save(post_id))
        },
        doComment(post_id, comment_text){
            return dispatch(comment(post_id, comment_text))
        },
        doFollow(user_id){
            alert(user_id)
            return dispatch(follow(user_id))
        },
        doConnect(user_id){
            return dispatch(invite(user_id))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile)