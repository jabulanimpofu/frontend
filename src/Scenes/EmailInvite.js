import React, {Component} from 'react'
import {Contact} from '../Components/SettingsCommunity'
import MainMenu from '../Components/MainMenu'
import Recent from '../Components/Recent'
import {connect} from 'react-redux'
import {getTimeline, getProductCategories, getSubCategories, getProducts, getServices, getPosts, createPost, save, comment, share, like, follow, invite, loadPhotos, loadPhoto, getActive, getFeatured, getRecentActivity } from '../Actions/Api'
import {clearNewTimeline} from '../Actions/App'


const products = {
    data: [1, 2, 3, 4, 5, 6,].map((review, index) => ({
        object: {},
        id: index
    }))
}

class Companies extends Component {
    constructor(props) {
        super(props)
        this.state = {
            selected: {},
            select: 'Select'
        }
    }

    componentDidMount() {
    }

    componentDidUpdate(prevProps) {

    }

    onFieldChange(field, value) {
        this.setState({[field]: value})
    }

    onSelect(user) {
        let {selected} = this.state
        if (selected[user.id]) {
            selected = Object.assign({}, selected)
            delete selected[user.id]
        }
        else {
            selected = {...selected, [user.id]: user}
        }

        this.setState({selected})
    }

    onSelectAll(){
        const contacts = [0, 1, 2, 3, 4, 5, 6, 6]
        const selected = contacts.reduce((selected, contact, index) => {
            selected = {...selected, [contact]: {}}
            return selected
        }, {})

        this.setState({selected, select: "Deselect"})

    }

    onDeselectSelectAll(){
        const contacts = [0, 1, 2, 3, 4, 5, 6, 6]
        const selected = {}
        this.setState({selected, select: "Select"})
    }

    render() {
        const { selected, select } = this.state
        const contacts = [0, 1, 2, 3, 4, 5, 6, 6]
        const _contacts = contacts.map((id, index) => {
            const user = {
                id,
                name: "some name",
                icon: "/img/contacts/10.jpg"
            }
            return (
                <Contact key={index} user={user} selected={!!selected[user.id]}  onSelect={this.onSelect.bind(this, user)}/>
            )
        })

        return (
            <section id="main" className="show-recent show-menu">
                <MainMenu/>
                <Recent />
                <section id="content">
                    <div className="lb-header palette-Light-Blue-50 bg">
                        <h3>Invite Customers</h3>
                        Invite the people in your email address book
                    </div>
                    <div>
                        <h5 onClick={e => {
                            e.preventDefault()
                            if(select === "Select")
                                this.onSelectAll.bind(this)()
                            else
                                this.onDeselectSelectAll.bind(this)()
                        }}>{select} All <label className="checkbox checkbox-inline m-r-20"><input type="checkbox" value="option1" className={select === "Select" ? "" : "checked"} onChange={e => {
                            e.preventDefault()
                            if(select === "Select")
                                this.onSelectAll.bind(this)()
                            else
                                this.onDeselectSelectAll.bind(this)()
                        }}/><i className="input-helper"></i></label></h5>
                    </div>
                    <div className="row" style={{marginTop: 32}}>
                        {_contacts}
                    </div>
                </section>
            </section>
        )
    }
}

Companies.propTypes = {}

const mapStateToProps = (state, ownProps) => {
    const user_id = state.profile.data.id
    const {collections, filter, profile} = state
    const {timeline, product_categories, sub_categories, active_users, featured, recent_activity} = collections

    return {
        active_users,
        timeline,
        user_id,
        product_categories,
        sub_categories,
        featured,
        recent_activity,
        filter,
        profile
    }
}


const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        doGetProductCategories(){
            return dispatch(getProductCategories())
        },
        doGetSubCategories(){
            return dispatch(getSubCategories())
        },
        doGetPosts(){
            return dispatch(getPosts())
        },
        doGetProducts(){
            return dispatch(getProducts())
        },
        doGetServices(){
            return dispatch(getServices())
        },
        doGetTimeline(){
            return dispatch(getTimeline())
        },
        doClearNewTimeline(){
            return dispatch(clearNewTimeline())
        },

        doCreatePost(post){
            return dispatch(createPost(post))
        },
        doLike(post_id){
            return dispatch(like(post_id))
        },
        doShare(post_id){
            return dispatch(share(post_id))
        },
        doSave(post_id){
            return dispatch(save(post_id))
        },
        doComment(post_id, comment_text){
            return dispatch(comment(post_id, comment_text))
        },
        doFollow(user_id){
            alert(user_id)
            return dispatch(follow(user_id))
        },
        doConnect(user_id){
            return dispatch(invite(user_id))
        },


        doGetActive(){
            return dispatch(getActive())
        },
        doGetFeatured(type){
            return dispatch(getFeatured(type))
        },
        doGetRecentActivity(){
            return dispatch(getRecentActivity())
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Companies)