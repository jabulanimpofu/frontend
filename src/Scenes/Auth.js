import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import {login, register, reset, email} from '../Actions/Api'
import SettingsIndividual from '../Components/SettingsIndividual'
import SettingsBusiness from '../Components/SettingsBusiness'
import SettingsCommunity from '../Components/SettingsCommunity'
import SettingsRecommendedUsers from '../Components/SettingsRecommendedUsers'

const Login = ({email, password, onFieldChange, show, login, isFetching}) => {
    let content = (
        <div className="lb-body">
            <div className="form-group fg-float">
                <div className="fg-line">
                    <input placeholder="Email"
                           type="text"
                           className="input-lg form-control fg-input"
                           name="email"
                           value={email}
                           onChange={onFieldChange.bind(undefined, 'email')}/>
                </div>
            </div>

            <div className="form-group fg-float">
                <div className="fg-line">
                    <input value={password}
                           onChange={onFieldChange.bind(undefined, 'password')}
                           type="password"
                           placeholder="Password"
                           className="input-lg form-control fg-input"/>
                </div>
            </div>

            <button className="btn btn-block btn-primary btn-lg" onClick={login}>Sign in</button>

            <div className="m-t-20 text-center">
                <a onClick={show.bind(undefined, "register")} className="palette-Blue text d-block m-b-5" href="">Create
                    an
                    account</a>
                <a onClick={show.bind(undefined, "reset")} href="" className="palette-Blue text">Forgot
                    password?</a>
            </div>
        </div>
    )
    if (isFetching)
        content = (
            <div className="lb-body">
                <div className="text-center card-padding">
                    <div className="preloader pl-xxl">
                        <svg className="pl-circular" viewBox="25 25 50 50">
                            <circle className="plc-path" cx="50" cy="50" r="20"></circle>
                        </svg>
                    </div>
                </div>
            </div>
        )

    return (
        <div className="l-block toggled" id="l-login">
            <div className="lb-header palette-Light-Blue-50 bg">
                <h3>Anyone can sell.</h3>
                Sign in to know your customers
            </div>
            {content}
        </div>
    )
}

const Register = ({name, email, password, account_type, accept_terms, onFieldChange, show, register, isFetching}) => {
    let content = (
        <div className="lb-body container">
            <div className="row">
                <div className="col-xs-12 col-md-6">

                    <ul className="list-group alt">
                        <li className="list-group-item m-b">
                            <div className="media">
                                    <span className="pull-left thumb-sm"><img src="/img/icons/radar-icon.png" alt=""
                                                                              className=""/></span>
                                <div className="media-body">
                                    <h4 style={{margin: 0}}><a href="#">Find Customers</a></h4>
                                    <h5 className="text-muted">and create relationships</h5>
                                </div>
                            </div>
                        </li>
                        <li className="list-group-item m-b">
                            <div className="media">
                                    <span className="pull-left thumb-sm"><img src="/img/icons/connect-icon.png" alt=""
                                                                              className=""/></span>
                                <div className="media-body">
                                    <h4 style={{margin: 0}}><a href="#">Connect with existing customers</a></h4>
                                    <h5 className="text-muted">and build trust</h5>
                                </div>
                            </div>
                        </li>
                        <li className="list-group-item m-b">
                            <div className="media">
                                    <span className="pull-left thumb-sm"><img src="/img/icons/radar-icon.png" alt=""
                                                                              className=""/></span>
                                <div className="media-body">
                                    <h4 style={{margin: 0}}><a href="#">Post unlimited products and services</a>
                                    </h4>
                                    <h5 className="text-muted">like a pro</h5>
                                </div>
                            </div>
                        </li>
                        <li className="list-group-item m-b">
                            <div className="media">
                                    <span className="pull-left thumb-sm"><img src="/img/icons/radar-icon.png" alt=""
                                                                              className=""/></span>
                                <div className="media-body">
                                    <h4 style={{margin: 0}}><a href="#">Its all free and</a></h4>
                                    <h5 className="text-muted">always will be</h5>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div className="col-xs-12 col-md-6">
                    <form>
                        <div className="form-group fg-float">
                            <div className="fg-line">
                                <input value={name} onChange={onFieldChange.bind(undefined, 'name')} type="text"
                                       placeholder="Name"
                                       className="input-lg form-control fg-input"/>
                            </div>
                        </div>

                        <div className="form-group fg-float">
                            <div className="fg-line">
                                <input value={email} onChange={onFieldChange.bind(undefined, 'email')} type="text"
                                       placeholder="email"
                                       className="input-lg form-control fg-input"/>
                            </div>
                        </div>

                        <div className="form-group fg-float">
                            <div className="fg-line">
                                <input value={password} onChange={onFieldChange.bind(undefined, 'password')}
                                       type="password"
                                       placeholder="Password"
                                       className="input-lg form-control fg-input"/>
                            </div>
                        </div>

                        <div className="form-group fg-float">
                            <div className="fg-line row">
                                <div className="radio m-b-15 col-xs-6">
                                    <label>
                                        <input value="individual"
                                               onChange={onFieldChange.bind(undefined, 'account_type')}
                                               type="radio"
                                               className={account_type === "individual" ? "checked" : ""}
                                               name="account_type"/>
                                        <i className="input-helper"></i>
                                        Individual
                                    </label>
                                </div>

                                <div className="radio m-b-15 col-xs-6" style={{marginTop: 10}}>
                                    <label>
                                        <input value="company"
                                               onChange={onFieldChange.bind(undefined, 'account_type')}
                                               type="radio"
                                               className={account_type === "company" ? "checked" : ""}
                                               name="account_type"/>
                                        <i className="input-helper"></i>
                                        Company
                                    </label>
                                </div>

                            </div>
                        </div>

                        <div className="checkbox m-b-30">
                            <label>
                                <input type="checkbox" value=""/>
                                <i className="input-helper"></i>
                                Accept the license agreement
                            </label>
                        </div>
                    </form>

                    <button className="btn btn-block btn-primary btn-lg" onClick={register}>Sign Up</button>

                    <div className="m-t-30  text-center">
                        <a onClick={show.bind(undefined, "login")} className="palette-Blue text d-block m-b-5"
                           href="">Already
                            have an account?</a>
                        <a onClick={show.bind(undefined, "reset")} href="" className="palette-Blue text">Forgot
                            password?</a>
                    </div>
                </div>
            </div>

        </div>
    )
    if (isFetching)
        content = (
            <div className="lb-body">
                <div className="text-center card-padding">
                    <div className="preloader pl-xxl">
                        <svg className="pl-circular" viewBox="25 25 50 50">
                            <circle className="plc-path" cx="50" cy="50" r="20"></circle>
                        </svg>
                    </div>
                </div>
            </div>
        )
    return (
        <div className="l-block l-block-double toggled" id="l-register">
            <div className="lb-header palette-Light-Blue-50 bg">
                <h3>Connect with your business</h3>
            </div>

            {content}
        </div>
    )

}

const Reset = ({email, onFieldChange, show, reset, isFetching}) => {
    let content = (
        <div className="lb-body">
            <p className="m-b-30">Lorem ipsum dolor fringilla enim feugiat commodo sed ac lacus.</p>

            <div className="form-group fg-float">
                <div className="fg-line">
                    <input value={email} onChange={onFieldChange.bind(undefined, 'email')} type="text"
                           placeholder="Email"
                           className="input-lg form-control fg-input"/>
                </div>
            </div>

            <button className="btn btn-block btn-primary btn-lg" onClick={reset}>Reset Password</button>

            <div className="m-t-30  text-center">
                <a onClick={show.bind(undefined, "login")} className="palette-Blue text d-block m-b-5" href="">Already
                    have an account?</a>
                <a onClick={show.bind(undefined, "register")} href="" className="palette-Blue text">Create an
                    account</a>
            </div>
        </div>
    )
    if (isFetching)
        content = (
            <div className="lb-body">
                <div className="text-center card-padding">
                    <div className="preloader pl-xxl">
                        <svg className="pl-circular" viewBox="25 25 50 50">
                            <circle className="plc-path" cx="50" cy="50" r="20"></circle>
                        </svg>
                    </div>
                </div>
            </div>
        )
    return (
        <div className="l-block toggled" id="l-forget-password">
            <div className="lb-header palette-Light-Blue-50 bg">
                <h3>Forgot password</h3>
            </div>

            {content}
        </div>
    )
}

const Individual = ({email, onFieldChange, show, reset, isFetching}) => {
    let content = (
        <div className="lb-body">
            <SettingsIndividual />

            <button className="btn btn-block btn-primary btn-lg" onClick={reset}>Save</button>

        </div>
    )
    if (isFetching)
        content = (
            <div className="lb-body">
                <div className="text-center card-padding">
                    <div className="preloader pl-xxl">
                        <svg className="pl-circular" viewBox="25 25 50 50">
                            <circle className="plc-path" cx="50" cy="50" r="20"></circle>
                        </svg>
                    </div>
                </div>
            </div>
        )
    return (
        <div className="l-block l-block-double toggled" id="l-individual-account-settings">
            <div className="lb-header palette-Light-Blue-50 bg">
                <h3>Change Account Settings</h3>
            </div>

            {content}
        </div>
    )
}

const Business = ({email, onFieldChange, show, reset, isFetching}) => {
    let content = (
        <div className="lb-body">

            <SettingsBusiness />

            <button className="btn btn-block btn-primary btn-lg" onClick={reset}>Save</button>

        </div>
    )
    if (isFetching)
        content = (
            <div className="lb-body">
                <div className="text-center card-padding">
                    <div className="preloader pl-xxl">
                        <svg className="pl-circular" viewBox="25 25 50 50">
                            <circle className="plc-path" cx="50" cy="50" r="20"></circle>
                        </svg>
                    </div>
                </div>
            </div>
        )
    return (
        <div className="l-block l-block-double toggled" id="l-individual-account-settings">
            <div className="lb-header palette-Light-Blue-50 bg">
                <h3>Change Account Settings</h3>
            </div>

            {content}
        </div>
    )
}

const Community = ({email, onFieldChange, show, reset, isFetching}) => {
    let content = (
        <div className="lb-body">

            <SettingsCommunity />

            <button className="btn btn-block btn-primary btn-lg" onClick={reset}>Skip</button>

        </div>
    )
    if (isFetching)
        content = (
            <div className="lb-body">
                <div className="text-center card-padding">
                    <div className="preloader pl-xxl">
                        <svg className="pl-circular" viewBox="25 25 50 50">
                            <circle className="plc-path" cx="50" cy="50" r="20"></circle>
                        </svg>
                    </div>
                </div>
            </div>
        )
    return (
        <div className="l-block l-block-double toggled" id="l-individual-account-settings">

            <div className="lb-header palette-Light-Blue-50 bg">
                <h3>Find Customers</h3>
                Invite the people in your email address book
            </div>

            {content}

        </div>
    )
}

const RecommendedUsers = ({email, onFieldChange, show, reset, isFetching}) => {
    let content = (
        <div className="lb-body">

            <SettingsRecommendedUsers />

            <button className="btn btn-block btn-primary btn-lg" onClick={reset}>Skip</button>

        </div>
    )
    if (isFetching)
        content = (
            <div className="lb-body">
                <div className="text-center card-padding">
                    <div className="preloader pl-xxl">
                        <svg className="pl-circular" viewBox="25 25 50 50">
                            <circle className="plc-path" cx="50" cy="50" r="20"></circle>
                        </svg>
                    </div>
                </div>
            </div>
        )
    return (
        <div className="l-block l-block-double toggled" id="l-individual-account-settings">

            <div className="lb-header palette-Light-Blue-50 bg">
                <h3>Recommended users</h3>
                Follow some or all of these recommended users
            </div>

            {content}

        </div>
    )
}

class Auth extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            name: '',
            account_type: 'individual',
            accept_terms: '',
            show: 'login'
        }
    }

    componentDidMount() {
        const clearForm = () => {
            this.setState({
                email: '',
                password: '',
                name: '',
                account_type: '',
                accept_terms: '',
            })
        }
    }

    onFiledChange(field, event) {
        event.preventDefault()
        this.setState({[field]: event.target.value});
    }

    login() {
        const {email, password} = this.state
        const {doLogin} = this.props
        doLogin(email, password)

    }

    register() {
        const {email, password, name, account_type, accept_terms} = this.state
        const {doRegister} = this.props
        doRegister(email, password, name, account_type, accept_terms)

    }

    email() {
        const {email} = this.state
        const {doEmail} = this.props
        doEmail(email)
    }

    reset() {
        const {password} = this.state
        const {doReset} = this.props
        doReset(password)
    }

    onShow(show, e) {
        e.preventDefault()
        this.setState({show})
    }

    render() {
        const {email, password, name, account_type, accept_terms, show} = this.state
        const {profile: {isFetching}} = this.props
        let _content = null

        switch (show) {
            case "register": {
                _content = <Register email={email}
                                     isFetching={isFetching}
                                     password={password}
                                     account_type={account_type}
                                     onFieldChange={this.onFiledChange.bind(this)}
                                     show={this.onShow.bind(this)}
                                     register={this.register.bind(this)}/>

            }
                break
            case "reset": {
                _content = <Reset email={email}
                                  isFetching={isFetching}
                                  password={password}
                                  onFieldChange={this.onFiledChange.bind(this)}
                                  show={this.onShow.bind(this)}
                                  reset={this.reset.bind(this)}/>

            }
                break
            case "individual": {
                _content = <Individual email={email}
                                       isFetching={isFetching}
                                       password={password}
                                       onFieldChange={this.onFiledChange.bind(this)}
                                       show={this.onShow.bind(this)}
                                       reset={this.reset.bind(this)}/>

            }
                break
            case "business": {
                _content = <Business email={email}
                                     isFetching={isFetching}
                                     password={password}
                                     onFieldChange={this.onFiledChange.bind(this)}
                                     show={this.onShow.bind(this)}
                                     reset={this.reset.bind(this)}/>

            }
                break
            case "community": {
                _content = <Community email={email}
                                      isFetching={isFetching}
                                      password={password}
                                      onFieldChange={this.onFiledChange.bind(this)}
                                      show={this.onShow.bind(this)}
                                      reset={this.reset.bind(this)}/>

            }
                break
            case "recommended": {
                _content = <RecommendedUsers email={email}
                                             isFetching={isFetching}
                                             password={password}
                                             onFieldChange={this.onFiledChange.bind(this)}
                                             show={this.onShow.bind(this)}
                                             reset={this.reset.bind(this)}/>

            }
                break
            case "login":
            default: {
                _content = <Login email={email}
                                  isFetching={isFetching}
                                  password={password}
                                  onFieldChange={this.onFiledChange.bind(this)}
                                  show={this.onShow.bind(this)}
                                  login={this.login.bind(this)}/>

            }
        }

        return (
            <div className="login-container">
                <div className="login">{_content}</div>
                <div className="text-center wrapper clearfix login-footer" id="footer" data-ma-footer="plus-afrik">
                    <p>
                        <a href="#">
                            <small className="text-white">About PlusAfrik</small>
                        </a> | <a href="#">
                        <small className="text-white">Terms and Conditions</small>
                    </a> | <a href="#">
                        <small className="text-white">Privacy Policy</small>
                    </a>
                    </p>
                </div>
            </div>
        )
    }
}

Auth.propTypes = {
    doLogin: PropTypes.func.isRequired,
    doEmail: PropTypes.func.isRequired,
    doReset: PropTypes.func.isRequired,
    doRegister: PropTypes.func.isRequired,
}

const mapStateToProps = (state, ownProps) => {
    const {profile} = state
    return {
        profile
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        doLogin(email, password){
            return dispatch(login(email, password))
        },
        doRegister(email, password, name, account_type){
            return dispatch(register(email, password, name, account_type))
        },
        doEmail(email_address){
            return dispatch(email(email_address))
        },
        doReset(password){
            return dispatch(reset(password))
        },

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Auth)