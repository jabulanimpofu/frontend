import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import {Link, Route} from 'react-router-dom'
import Recent from '../Components/Recent'
import RecommendedSellers from '../Containers/RecommendedSellers'
import FeaturedProducts from '../Containers/FeaturedProducts'
import SettingsBusiness from '../Components/SettingsBusiness'
import SettingsCommunity from '../Components/SettingsCommunity'
import SettingsIndividual from '../Components/SettingsIndividual'
import {
    getActive,
    getFeatured,
    getRecentActivity,
    getUser,
    follow,
    invite
} from '../Actions/Api'

class Settings extends Component {

    componentDidMount() {
        const {
            view,
            match,
            profile_id,
            user_id,
            doGetActive,
            doGetFeatured,
        } = this.props

        // alert(`${match.url}/home`)

        //has id
        if (user_id) {
            doGetActive()
            doGetFeatured()
        }
    }

    componentDidUpdate(prevProps) {
        const {
            profile_id,
            user_id,
            doGetActive,
            doGetFeatured,
        } = this.props

        //new id
        if (user_id && (prevProps.user_id !== user_id)) {
            doGetActive()
            doGetFeatured()
        }
    }

    render() {
        const {user, match, recent_activity, active_users, featured, doFollow, doConnect, doGetRecentActivity} = this.props
        return (
            <section id="main" className="show-recent">
                <Recent doGetRecentActivity={doGetRecentActivity} recent_activity={recent_activity}/>
                <div className="container">
                    <div className="row">
                        <div className="col-md-8 co-xs-12">
                            <Route path={`${match.url}/community`} component={SettingsCommunity}/>
                            <Route path={`${match.url}/business`} component={SettingsBusiness}/>
                            <Route exact path={`${match.url}`} component={SettingsIndividual}/>
                        </div>
                        <div className="col-md-4 hidden-sm">
                            <RecommendedSellers active_users={active_users}
                                                doFollow={doFollow}
                                                doConnect={doConnect}/>
                            <FeaturedProducts products={featured}/>
                            <div>
                                <div className="card-body card-padding">
                                    <span className="block" style={{textAlign: 'center'}}><a href="#">Copyright PlusAfrik 2016</a></span>
                                    <span className="block"><a href="#">About</a> | <a href="#">Terms</a> | <a
                                        href="#">Privacy</a> | <a
                                        href="#">Feedback</a></span>
                                    <span className="block"><a href="#">Product Listing</a> | <a href="#">Advertising Policy</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

Settings.propTypes = {
    
}

const mapStateToProps = (state, ownProps) => {
    const {collections, profile, user} = state
    const user_id = profile.data.id
    const {profile_id, view} = ownProps.match.params
    const {recent_activity, active_users, featured} = collections

    return {
        user_id,
        profile_id,
        view,
        recent_activity,
        active_users,
        featured,
        user
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        doGetUser(id){
            return dispatch(getUser(id))
        },
        doGetRecentActivity(){
            return dispatch(getRecentActivity())
        },
        doGetActive(){
            return dispatch(getActive())
        },
        doGetFeatured(type){
            return dispatch(getFeatured(type))
        },
        doFollow(user_id){
            alert(user_id)
            return dispatch(follow(user_id))
        },
        doConnect(user_id){
            return dispatch(invite(user_id))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Settings)
