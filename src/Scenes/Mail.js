import React, {Component} from 'react'
import {connect} from 'react-redux'
import Messages from '../Components/Messages'
import Recent from '../Components/Recent'
import {
    getActive,
    getFeatured,
    getRecentActivity,
    getUser,
    getMessages,
} from '../Actions/Api'

class Mail extends Component {

    componentDidMount() {
        const {
            user_id,
            doGetActive,
            doGetFeatured,
        } = this.props

        //has id
        if (user_id) {
            doGetActive()
            doGetFeatured()
        }
    }

    componentDidUpdate(prevProps) {
        const {
            user_id,
            doGetActive,
            doGetFeatured,
        } = this.props

        //new id
        if (user_id && (prevProps.user_id !== user_id)) {
            doGetActive()
            doGetFeatured()
        }
    }

    render() {
        const {recent_activity, doGetRecentActivity} = this.props
        return (
            <section id="main" className="show-recent">
                <Recent doGetRecentActivity={doGetRecentActivity} recent_activity={recent_activity}/>
                <Messages/>
            </section>
        )
    }
}

Mail.propTypes = {}

const mapStateToProps = (state, ownProps) => {
    const {collections, profile, messages} = state
    const user_id = profile.data.id
    const {profile_id, view} = ownProps.match.params
    const {recent_activity, active_users, featured} = collections

    return {
        user_id,
        view,
        recent_activity,
        active_users,
        featured,
        messages
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        doGetMessages(id){
            return dispatch(getMessages(id))
        },
        doGetUser(id){
            return dispatch(getUser(id))
        },
        doGetRecentActivity(){
            return dispatch(getRecentActivity())
        },
        doGetActive(){
            return dispatch(getActive())
        },
        doGetFeatured(type){
            return dispatch(getFeatured(type))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Mail)