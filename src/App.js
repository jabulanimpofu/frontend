import React, {PropTypes} from 'react'
import {Provider} from 'react-redux'
import {BrowserRouter as Router, Route} from 'react-router-dom'

import AppContainer from './Containers/AppContainer'
import CookiePolicy from './Scenes/CookiePolicy'
import PrivacyPolicy from './Scenes/PrivacyPolicy'
import Terms from './Scenes/Terms'
import Notification from './Scenes/Notification'
import ProductListingPolicy from './Scenes/ProductListingPolicy'
import Companies from './Scenes/Companies'
import Products from './Scenes/Products'
import Profile from './Scenes/Profile'
import Auth from './Scenes/Auth'
import Home from './Scenes/Home'
import Mail from './Scenes/Mail'
import Settings from './Scenes/Settings'
import GoldSeller from './Scenes/GoldSeller'
import IdealCustomer from './Scenes/IdealCustomer'
import Mall from './Scenes/Mall'
import User from './Scenes/User'
import EmailInvite from './Scenes/EmailInvite'
import {IndividualWizard, CompanyWizard} from './Scenes/Wizard'

import '../bower_components/animate.css/animate.min.css'
import '../bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css'
import '../bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css'
import '../bower_components/lightgallery/dist/css/lightgallery.min.css'
import '../bower_components/google-material-color/dist/palette.css'
import '../bower_components/bootstrap-select/dist/css/bootstrap-select.css'
import '../bower_components/chosen/chosen.min.css'
import './less/app.less'

const App = ({store, history}) => {
    return (
        <Provider store={store}>
            <Router history={history}>
                    <AppContainer>
                        <Route path="/auth" component={Auth}/>
                        <Route exact path="/" component={Home}/>
                        <Route path="/home" component={Home}/>
                        <Route path="/user/:profile_id" component={User}/>
                        <Route path="/settings" component={Settings}/>
                        <Route path="/profile" component={Profile}/>
                        <Route path="/companies" component={Companies}/>
                        <Route path="/people" component={Companies}/>
                        <Route path="/products" component={Products}/>
                        <Route path="/mall" component={Mall}/>
                        <Route path="/notification" component={Notification}/>
                        <Route path="/mail" component={Mail}/>
                        <Route path="/gold_seller" component={GoldSeller}/>
                        <Route path="/ideal_customer" component={IdealCustomer}/>
                        <Route path="/email_invite" component={EmailInvite}/>
                        <Route path="/individual_wizard" component={IndividualWizard}/>
                        <Route path="/company_wizard" component={CompanyWizard}/>

                        <Route path="/privacy_policy" component={PrivacyPolicy}/>
                        <Route path="/cookie_policy" component={CookiePolicy}/>
                        <Route path="/product_listing_policy" component={ProductListingPolicy}/>
                        <Route path="/terms" component={Terms}/>
                    </AppContainer>
            </Router>
        </Provider>
    )
}

App.propTypes = {
    store: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
}
export default App
