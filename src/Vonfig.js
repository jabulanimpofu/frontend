export default {
    name: 'GetStream.io - React Example App',
    version: '1.0.0',
    env: 'DEVELOPMENT',
    port: 8000,
    jwt: {
        secret: 'VALUE',
    },
    db: {
        name: 'cabin',
        username: 'root',
        password: 'lucidoccum',
        host: 'localhost',
        port: 3306,
    },
    mapbox: {
        accessToken: 'pk.eyJ1IjoiamFidWxhbmkiLCJhIjoiY2oxdTkwZWswMDAwYjJ3bWpmNW5rM2xqbCJ9.ThDj_eEBrCLJO1Nxn1ppeQ',
    },
    s3: {
        key: "AKIAI3ZUGZTNKVJVX72A",
        secret: "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",
        bucket: "plusafriks3",
    },
    stream: {
        appId: 24134,
        key: 'yrq2de8w8wxm',
        secret: '9cp52jmr4c84q84hmhth4ezsa8zrtdrxv57bnkgxvbs454bkckvvr5f567fjwxcn'
    },
    api: {
        baseUrl: "http://plus-react.herokuapp.com",
    },
    imgix: {
        baseUrl: 'https://plusafrikimg.imgix.net',
    },
    algolia: {
        appId: '8XR2192PHP',
        searchOnlyKey: 'a9927d15a331c53b32763d5285858fb8',
        apiKey: 'ca02293775a4bdaabb6940556bf6bc6a',
    },
    keen: {
        projectId: '58fc4295be8c3e260bcadb45',
        writeKey: '65B2963883F35771CA823084656EB827F0C68D6EEA0DCBBCEDBDB57B65F93F10A2C111A249DA749CFB94194A4F90139B369B7F65FFD1DC948B5ADE3B155DBA3E29FC2D04D9FC286068F8B353B9545841B17598106313E83B31A17B0B80829417',
        readKey: process.env.KEEN_READ_KEY,
    }
}