import {API, API_FLAGS, HTTP_METHODS, GENERAL} from '../Constants'
const {REQUEST} = GENERAL
const {GET, POST} = HTTP_METHODS
import $ from '../../bower_components/jquery/dist/jquery'
import uuid from 'uuid/v4'

export const login = (email, password) => (dispatch, getState) => {
    return dispatch({
        [REQUEST]: {
            endpoint: '/users/login',
            method: POST,
            type: API.LOGIN,
            body: {
                email,
                password
            }
        }
    })

}

export const register = (email, password, name, account_type) => (dispatch, getState) => {
    return dispatch({
        [REQUEST]: {
            endpoint: '/users/register',
            method: POST,
            type: API.REGISTER,
            body: {
                email,
                password,
                name,
                account_type
            }
        }
    })
}

export const email = (email) => (dispatch, getState) => {
    return dispatch({
        [REQUEST]: {
            endpoint: 'email',
            method: POST,
            type: API.EMAIL,
            body: {
                email
            }
        }
    })
}

export const reset = (password) => (dispatch, getState) => {
    return dispatch({
        [REQUEST]: {
            endpoint: 'reset',
            method: POST,
            type: API.RESET,
            body: {
                password
            }
        }
    })
}

export const getActive = () => (dispatch, getState) => {
    const body = {
        user_id: getState().profile.data.id
    }
    return dispatch({
        [REQUEST]: {
            endpoint: 'active',
            method: GET,
            type: API.GET_ACTIVE,
            body
        }
    })
}

export const getFeatured = (type) => (dispatch, getState) => {
    const body = {
        user_id: getState().profile.data.id
    }
    if(type)
        body.type = type
    return dispatch({
        [REQUEST]: {
            endpoint: 'featured',
            method: GET,
            type: API.GET_FEATURED,
            body
        }
    })
}

export const getRecentActivity = (type) => (dispatch, getState) => {
    const body = {
        user_id: getState().profile.data.id
    }
    if(type) body.type = type
    return dispatch({
        [REQUEST]: {
            endpoint: 'recent_activity',
            method: GET,
            type: API.GET_RECENT_ACTIVITY,
            body
        }
    })
}

export const getCategories = () => (dispatch, getState) => {
    return dispatch({
        [REQUEST]: {
            endpoint: 'categories',
            method: GET,
            type: API.GET_CATEGORIES,
        }
    })
}

export const getProductCategories = () => (dispatch, getState) => {
    return dispatch({
        [REQUEST]: {
            endpoint: 'product_categories',
            method: GET,
            type: API.GET_PRODUCT_CATEGORIES,
        }
    })
}

export const getSubCategories = () => (dispatch, getState) => {
    return dispatch({
        [REQUEST]: {
            endpoint: `sub_categories`,
            method: GET,
            type: API.GET_SUB_CATEGORIES,
        }
    })
}

export const getProducts = () => (dispatch, getState) => {
    const user_id = getState().profile.data.id
    const body = {
        // type: 'product',
        user_id,
    }
    return dispatch({
        [REQUEST]: {
            endpoint: 'posts',
            method: GET,
            type: API.GET_PRODUCTS,
            body
        }
    })
}

export const getServices = () => (dispatch, getState) => {
    const user_id = getState().profile.data.id
    const body = {
        // type: 'service',
        user_id
    }
    return dispatch({
        [REQUEST]: {
            endpoint: 'posts',
            method: GET,
            type: API.GET_SERVICES,
            body
        }
    })
}

export const getPosts = () => (dispatch, getState) => {
    const user_id = getState().profile.data.id
    const body = {
        // type: 'post',
        user_id
    }
    return dispatch({
        [REQUEST]: {
            endpoint: 'posts',
            method: GET,
            type: API.GET_POSTS,
            body
        }
    })
}

export const getTimeline = () => (dispatch, getState) => {
    const user_id = getState().profile.data.id
    return dispatch({
        [REQUEST]: {
            endpoint: 'posts',
            method: GET,
            type: API.GET_TIMELINE,
            body: {
                user_id,
            },
        },
    })
}

export const getNotifications = () => (dispatch, getState) => {
    const user_id = getState().profile.data.id
    return dispatch({
        [REQUEST]: {
            endpoint: 'notifications',
            method: GET,
            type: API.GET_NOTIFICATIONS,
            body: {
                user_id
            }
        }
    })
}

export const getMessages = () => (dispatch, getState) => {
    return dispatch({
        [REQUEST]: {
            endpoint: 'messages',
            method: GET,
            type: API.GET_MESSAGES,
        }
    })
}

export const getProfile = () => (dispatch, getState) => {
    return dispatch({
        [REQUEST]: {
            endpoint: `profile`,
            method: GET,
            type: API.GET_PROFILE,
        }
    })
}

export const getUser = (id) => (dispatch, getState) => {
    const user_id = getState().profile.data.id
    return dispatch({
        [REQUEST]: {
            endpoint: `users/${id}`,
            method: GET,
            type: API.GET_USER,
            body: {
                user_id,
            }
        }
    })
}

export const createPost = (post) => (dispatch, getState) => {
    const data = new FormData()
    const user_id = getState().profile.data.id
    data.append('user_id', user_id)
    data.append('type', 'post')
    data.append('body', post.body)

    if (post.image) {
        data.append('image', post.image, post.image.name)
    }

    return dispatch({
        [REQUEST]: {
            endpoint: 'posts',
            method: POST,
            type: API.CREATE_POST,
            body: data
        }
    })
}

export const createProduct = (product) => (dispatch, getState) => {
    const data = new FormData()
    data.append('image', product.image, product.image.name)
    data.append('name', product.name)
    data.append('body', product.body)
    data.append('price', product.price)
    data.append('category', product.category)
    data.append('sub_category', product.sub_category)

    const user_id = getState().profile.data.id
    data.append('user_id', user_id)
    data.append('type', 'product')

    return dispatch({
        [REQUEST]: {
            endpoint: 'posts',
            method: POST,
            type: API.CREATE_PRODUCT,
            body: data,
        }
    })
}

export const createService = (service) => (dispatch, getState) => {
    const data = new FormData()
    data.append('image', service.image, service.image.name)
    data.append('name', service.name)
    data.append('body', service.body)
    data.append('price', service.price)
    data.append('category', service.category)
    data.append('sub_category', service.sub_category)
    data.append('rate', service.rate)

    const user_id = getState().profile.data.id
    data.append('user_id', user_id)
    data.append('type', 'service')

    return dispatch({
        [REQUEST]: {
            endpoint: 'posts',
            method: POST,
            type: API.CREATE_SERVICE,
            body: data,
        }
    })
}

export const follow = (user_id) => (dispatch, getState) => {
    const follower_id = getState().profile.data.id
    return dispatch({
        [REQUEST]: {
            endpoint: 'followers',
            method: POST,
            type: API.FOLLOW,
            body: {user_id, follower_id}
        }
    })
}

export const invite = (user_id) => (dispatch, getState) => {
    const follower_id = getState().profile.data.id
    return dispatch({
        [REQUEST]: {
            endpoint: 'invites',
            method: POST,
            type: API.FOLLOW,
            body: {user_id, follower_id}
        }
    })
}

export const comment = (post_id, body) => (dispatch, getState) => {
    return dispatch({
        [REQUEST]: {
            endpoint: 'comments',
            method: POST,
            type: API.COMMENT,
            body: {post_id, body}
        }
    })
}

export const save = (post_id) => (dispatch, getState) => {
    return dispatch({
        [REQUEST]: {
            endpoint: 'saves',
            method: POST,
            type: API.SAVE,
            body: {post_id}
        }
    })
}

export const like = (post_id) => (dispatch, getState) => {
    return dispatch({
        [REQUEST]: {
            endpoint: 'likes',
            method: POST,
            type: API.LIKE,
            body: {post_id}
        }
    })
}

export const share = (post_id) => (dispatch, getState) => {
    return dispatch({
        [REQUEST]: {
            endpoint: 'share',
            method: POST,
            type: API.SHARE,
            body: {post_id}
        }
    })
}

export const upload = (file) => (dispatch, getState) => {
    const token = getState().profile.token
    const data = new FormData()
    data.append('files', file)

    const id = uuid()
    file = {...file, id}

    dispatch({
        type: API.UPLOAD,
        file
    })

    $.ajax({
        url: `https://plus-backend.herokuapp.com/api/upload?token=${token}`,
        type: 'POST',
        data: data,
        cache: false,
        dataType: 'json',
        mimeType: "multipart/form-data",
        processData: false,
        contentType: false,
        xhr: function () {
            //upload Progress
            var xhr = $.ajaxSettings.xhr();
            if (xhr.upload) {
                xhr.upload.addEventListener('progress', function (event) {
                    let progress = 0;
                    const position = event.loaded || event.position;
                    const total = event.total;
                    if (event.lengthComputable) {
                        progress = Math.ceil(position / total * 100);
                    }

                    dispatch({
                        type: API.UPLOAD + '_' + API_FLAGS.PROGRESS,
                        id,
                        progress,
                    })
                    console.log(progress)
                }, true);
            }
            return xhr;
        },
        success: function (data, textStatus, jqXHR) {
            console.log(data)
            console.log(textStatus)
            console.log(jqXHR)

            dispatch({
                type: API.UPLOAD + '_' + API_FLAGS.SUCCESS,
                id,
                images: data.images,
            })
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown)
            console.log(textStatus)
            console.log(jqXHR)

            dispatch({
                type: API.UPLOAD + '_' + API_FLAGS.ERROR,
                id,
            })
        }
    })
}

export const createUpload = (post) => (dispatch, getState) => {
    const {id} = getState().profile.data
    const {caption, hashtags, location, file} = post
    const data = new FormData()
    data.append('image', file, file.name)
    data.append('user_id', id)
    data.append('caption', caption)
    data.append('hashtags', hashtags)
    data.append('location', location)

    return dispatch({
        [REQUEST]: {
            endpoint: 'uploads',
            method: POST,
            type: API.CREATE_POST,
            body: data
        }
    })
}

export const loadPhotos = (last_id) => (dispatch, getState) => {
    const body = {
        user_id: getState().profile.data.id
    }
    if (last_id) body.last_id = last_id

    return dispatch({
        [REQUEST]: {
            endpoint: 'uploads',
            method: GET,
            type: API.GET_POSTS,
            body
        }
    })
}

export const loadPhoto = (id) => (dispatch, getState) => {
    const body = {
        id
    }
    return dispatch({
        [REQUEST]: {
            endpoint: 'upload',
            method: GET,
            type: API.GET_POST,
            body
        }
    })
}