import {API_FLAGS} from '../Constants'

import stream from 'getstream'
import config from '../../config'

let client = null
const feeds = {
    timeline: null,
    notification: null,
    messages: null,
}


export const connectToStream = () => (dispatch, getState) => {
    const {profile} = getState()

    // instantiate a new client (client side)
    client = stream.connect(config.stream.key, null, config.stream.appId)
}

export const subscribeToFeed = (feedAction, feedSlug, fetchAction, userId, token, siteId, options) => (dispatch, getState) => {
    const {profile} = getState()
    userId = userId ? userId : profile.data.id
    token = profile.tokens[feedSlug]

    // follow 'timeline_flat' feed
    feeds[feedSlug] = client.feed(feedSlug, userId, token)
    feeds[feedSlug]
        .subscribe(data => {
            dispatch({
                type: feedAction + '_' + API_FLAGS.STREAM,
                payload: data
            })
            if (feedAction)
                dispatch(fetchAction)
            console.log('*******************************************')
            console.log('incoming...')
            console.log(feedSlug)
            console.log(feedAction)
            console.log(data)
        })
        .then(() => {
            console.log(`Full (${feedSlug}): Connected to faye channel, waiting for realtime updates`)
        }, (err) => {
            console.error(`Full (${feedSlug}): Could not estabilsh faye connection`, err);
        })
        .catch((e) => {
            console.log(e)
        })
}