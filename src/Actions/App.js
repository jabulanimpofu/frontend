import {API, API_FLAGS, HTTP_METHODS, GENERAL} from '../Constants'
const {REQUEST} = GENERAL
const {GET, POST} = HTTP_METHODS
// import $ from '../../bower_components/jquery/dist/jquery'
// import uuid from 'uuid/v4'

export const logout = () => ({type: GENERAL.LOGOUT})

export const clearNewTimeline = () => ({type: GENERAL.CLEAR_NEW_TIMELINE})

export const setFilter = (filter) => ({type: GENERAL.SET_FILTER, filter})

export const resetFilter = () => ({type: GENERAL.RESET_FILTER})