import fetch from 'isomorphic-fetch';
import isPlainObject from 'lodash.isplainobject';
import Config from '../Vonfig'
import uuid from 'uuid/v4'

import {GENERAL, HTTP_METHODS} from '../Constants'
const {REQUEST} = GENERAL
const {GET, POST} = HTTP_METHODS

// const API_ROOT = "http://private-98530-plusafrik.apiary-mock.com/"
const API_ROOT = Config.api.baseUrl

import {isRSAA, validateRSAA} from '../Util/request_validation';
import {InvalidRSAA, RequestError} from '../Util/request_errors' ;
import {normalizeTypeDescriptors, actionWith, getUrl, getTypes, getHeaders} from '../Util/request';

export default ({getState}) => (next) => async (action) => {
    // Do not process actions without a [REQUEST] property
    if (!isRSAA(action)) {
        return next(action);
    }


    const {token} = getState().profile
    const request = Object.assign({}, action[REQUEST], {types: getTypes(action[REQUEST].type)});
    delete request.type;
    action[REQUEST] = request;

    // Try to dispatch an error request FSA for invalid RSAAs
    const validationErrors = validateRSAA(action);
    if (validationErrors.length) {

        if (request.types && Array.isArray(request.types)) {
            let requestType = request.types[0];
            if (requestType && requestType.type) {
                requestType = requestType.type;
            }
            next({
                type: requestType,
                payload: new InvalidRSAA(validationErrors),
                error: true
            });
        }
        return;
    }

    // Parse the validated RSAA action
    let {endpoint, headers, body, extra} = request;
    const {method, credentials, bailout, types} = request;
    const [requestType, successType, failureType] = normalizeTypeDescriptors(types)
    extra = isPlainObject(extra) ? {...extra, id: uuid()} : {id: uuid()}

    // Should we bail out?
    try {
        if ((typeof bailout === 'boolean' && bailout) ||
            (typeof bailout === 'function' && bailout(getState()))) {
            return;
        }
    } catch (e) {
        return next(await actionWith(
            {
                ...requestType,
                payload: new RequestError('[REQUEST].bailout function failed'),
                error: true,
                extra
            },
            [action, getState()]
        ));
    }

    // Process [REQUEST].endpoint function
    if (typeof endpoint === 'function') {
        try {
            endpoint = endpoint(getState());
        } catch (e) {
            return next(await actionWith(
                {
                    ...requestType,
                    payload: new RequestError('[REQUEST].endpoint function failed'),
                    error: true,
                    extra
                },
                [action, getState()]
            ));
        }
    }

    // Process [REQUEST].body
    if (isPlainObject(body) && method === POST) {
        try {
            body = JSON.stringify(body)
        } catch (e) {
            return next(await actionWith(
                {
                    ...requestType,
                    payload: new RequestError('[REQUEST].body object is not a plain object'),
                    error: true,
                    extra
                },
                [action, getState()]
            ));
        }
    }

    // Process [REQUEST].headers function
    if (typeof headers === 'function') {
        try {
            headers = headers(getState());
        } catch (e) {
            return next(await actionWith(
                {
                    ...requestType,
                    payload: new RequestError('[REQUEST].headers function failed'),
                    error: true,
                    extra
                },
                [action, getState()]
            ));
        }
    }

    // Process [REQUEST].headers object
    try {
        headers = getHeaders(headers, token)
        if (body instanceof FormData) {
            console.log('form data')
        }
        else {
            headers.append('Content-Type', 'application/json')
        }

    } catch (e) {
        return next(await actionWith(
            {
                ...requestType,
                payload: new RequestError('[REQUEST].headers function failed'),
                error: true,
                extra
            },
            [action, getState()]
        ));
    }

    // We can now dispatch the request FSA
    next(await actionWith(
        requestType,
        [action, getState()]
    ));

    try {
        //get the final endpoint
        let options = {method, credentials, headers}
        let url = undefined

        if (method !== GET && body) {
            options.body = body
        }
        if (isPlainObject(body) && method === GET) {
            url = getUrl(API_ROOT, endpoint, body)
        }
        else {
            url = getUrl(API_ROOT, endpoint)
        }

        var res = await fetch(url, options);
    } catch (e) {
        // The request was malformed, or there was a network error
        return next(await actionWith(
            {
                ...requestType,
                payload: new RequestError(e.message),
                error: true,
                extra
            },
            [action, getState()]
        ));
    }

    // Process the server response
    if (res.ok) {
        return next(await actionWith(
            successType,
            [action, getState(), res],
            extra
        ));
    } else {
        return next(await actionWith(
            {
                ...failureType,
                error: true,
                extra
            },
            [action, getState(), res]
        ));
    }
}

