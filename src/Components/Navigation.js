import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import {getCategories} from '../Actions/Api'

const active = 0

class Navigation extends Component {
    componentDidMount(){
        this.props.doGetCategories()
    }
    componentDidUpdate(prevProps){

    }
    render() {
        const {categories} = this.props
        const _categories = categories.map((cateogory, index) => {
            if(index === active)
                return (
                    <li key={index} className="navigation__active">
                        <a href="#">{cateogory}</a>
                    </li>
                )
            return (<li  key={index}><a href="#">{cateogory}</a></li>)
        })
        return (
            <aside id="navigation">
                <div className="navigation__header">
                    <i className="zmdi zmdi-long-arrow-left" data-mae-action="block-close"></i>
                    <h2>Categories</h2>
                </div>

                <div className="navigation__menu c-overflow">
                    <ul>
                        {_categories}
                    </ul>
                </div>
            </aside>
        )
    }
}

Navigation.propTypes = {
    categories: PropTypes.array.isRequired,
    doGetCategories: PropTypes.func.isRequired,
}

const mapStateToProps = (state, ownProps) => {
    const categories = state.collections.categories
    return {
        categories
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        doGetCategories(){
            return dispatch(getCategories())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Navigation)