import React, {Component, PropTypes} from 'react'
import Rating from './Rating'
import Config from '../../config'
import {Link} from 'react-router-dom'


class Company extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const {index, onShowDetails, onSelect} = this.props
        return (
            <div className="col-md-4 col-sm-4 col-xs-4" key={index} onClick={e => {
                e.preventDefault()
                onSelect()
            }}>
                <div className="c-item">
                    <div className="ci-avatar">
                        <img src="/img/product.jpg" alt=""/>
                    </div>

                    <div className="c-info">
                        <strong>Guzha Inc</strong>
                        <small>Phasellus at ultricies neque, quis malesuada augue. Donec eleifend condimentum nisl eu
                            consectetur.
                        </small>
                        <small><b>24k</b> Followers</small>
                        <small><b>2k</b> Endorsements</small>
                        <Rating rating={4}/>
                    </div>

                    <div className="btn-group btn-group-justified" role="group" aria-label="...">
                        <div className="btn-group" role="group">
                            <button type="button" className="btn btn-default btn-icon-text waves-effect"><i
                                className="zmdi zmdi-favorite"></i> follow</button>
                        </div>
                        <div className="btn-group" role="group">
                            <button type="button" className="btn btn-default btn-icon-text waves-effect"><i
                                className="zmdi zmdi-download"></i> invite</button>
                        </div>
                    </div>

                </div>
            </div>
        )
    }

}

class CompanyListItem extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const {index, doFollow} = this.props
        const user = {
            id: 8,
            name: "some name",
            location: "harare, zimbabwe"
        }
        return (
            <div className="card row" key={index}  style={{marginLeft: 0, marginRight: 0}}>
                <div className="col-md-8 plus-card-left">
                    <div className="card-header" style={{paddingLeft: 0, paddingRight: 0}}>
                        <div className="media">
                            <div className="pull-left">
                                <img className="avatar-img a-lg" src="/img/profile-pics/2.jpg" alt=""/>
                            </div>

                            <div className="media-body m-t-5">
                                <h2><Link to={`/user/${user.id}`}>{user.name}</Link>
                                    <small>Graphic Designer</small>
                                    <small>Guzha Advertising</small>
                                    <small><b>From</b> {user.location}</small>
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div className="card-header ch-img"
                         style={{backgroundImage: 'url(/img/demo/note.png)', height: 250}}></div>
                    <div className="card-body card-padding"
                         style={{paddingTop: 30, paddingLeft: 0, paddingRight: 0}}>
                        <p>Donec ullamcorper nulla non metus auctor fringilla. Cras justo odio, dapibus ac facilisis
                            in, egestas eget quam. Fusce dapibus, tellus ac cursus commodo, tortor mauris
                            condimentum nibh, ut fermentum massa justo sit amet risus. Vestibulum id ligula porta
                            felis euismod semper. Nulla vitae elit libero, a pharetra </p>
                    </div>


                </div>
                <div className="col-md-4 plus-card-right">
                    <h5>verified reviews</h5>
                    <Rating rating={4}/>
                    <h5>10k followers</h5>
                    <h5>About</h5>
                    <p>
                        Phasellus at ultricies neque, quis malesuada augue. Donec eleifend condimentum nisl eu consectetur. Integer eleifend, nisl venenatis consequat iaculis, lectus arcu malesuada sem, dapibus porta quam lacus eu neque. Phasellus at ultricies neque, quis malesuada augue. Donec eleifend condimentum nisl eu consectetur. Integer eleifend, nisl venenatis consequat iaculis, lectus arcu malesuada sem, dapibus porta quam lacus eu neque
                    </p>

                    <div className="btn-group btn-group-justified" role="group" aria-label="...">
                        <div className="btn-group" role="group">
                            <button type="button" className={`btn btn-icon-text waves-effect btn-default`}
                                    onClick={(e) => {
                                        e.preventDefault()
                                        doFollow()
                                    }}><i
                                className="zmdi zmdi-favorite"></i>follow
                            </button>
                        </div>
                        <div className="btn-group" role="group">
                            <button type="button" className={`btn btn-icon-text waves-effect btn-default`}
                                    onClick={(e) => {
                                        e.preventDefault()
                                        doFollow()
                                    }}><i
                                className="zmdi zmdi-download"></i>invite
                            </button>
                        </div>

                    </div>


                </div>
            </div>
        )
    }
}

class Companies extends Component {
    constructor(props) {
        super(props)
        this.state = {
            modal: 'add_to_collection',
            active: null
        }
    }

    onSelect(active) {
        this.setState({active})
    }

    render() {
        const {posts, grid, doComment, doLike, doShare, doSave} = this.props
        const {active} = this.state
        let active_item = null
        const list = !Array.isArray(posts.data) ? [] : posts.data.reduce((list, post, index) => {
            if (isNaN(active)) {
                list.push(post)
                return list
            }

            //if is the active one
            if (post.id === active) {
                active_item = post
            }
            else {
                list.push(post)
            }

            if (active_item && !(index % 3)) {
                list.push(active_item)
                active_item = null
            }
            return list
        }, [])

        let oldestNew = -1
        let _posts = list.reduce((_posts, post, index) => {
            if (post.isNew) {
                oldestNew = index
            }

            if (grid && !isNaN(active) && active === post.id) {
                _posts.push(<div className="clearfix" key={"clearfix1"}/>)
                _posts.push(
                    <div key={index} style={{backgroundColor: 'rgba(0,0,0,0.75)', padding: 16, paddingBottom: 1}}>
                        <CompanyListItem post={post}
                                         onSelect={this.onSelect.bind(this, post.id)}
                                         index={index}/>
                    </div>
                )
                _posts.push(<div className="clearfix" key={"clearfix2"}/>)
                return _posts
            }

            if (grid) {
                _posts.push(<Company post={post}
                                     onSelect={this.onSelect.bind(this, post.id)}
                                     key={index}
                                     index={index}/>)
                return _posts

            }


            _posts.push(<CompanyListItem post={post}
                                         onSelect={this.onSelect.bind(this, post.id)}
                                         onAddToCollection={this.onAddCollection.bind(this)}
                                         key={index}
                                         index={index}/>)
            return _posts
        }, [])

        const _new = oldestNew > -1 ? (
            <div className="card col-xs-12" onClick={this.props.clearNewTimeline}>
                <div className="card-header">
                    <h2>You have {oldestNew + 1} new posts.</h2>
                </div>
            </div>
        ) : null


        const className = grid ? "contacts c-profile clearfix row card card-padding" : "container"

        return (
            <div className={className} style={{marginLeft: 0, marginRight: 0}}>
                {_new}
                {_posts}
            </div>

        )
    }

}

Companies.propTypes = {
    posts: PropTypes.object.isRequired,
    doComment: PropTypes.func.isRequired,
    doLike: PropTypes.func.isRequired,
    doShare: PropTypes.func.isRequired,
    doSave: PropTypes.func.isRequired,
}

Company.propTypes = {
    post: PropTypes.object.isRequired,
    doComment: PropTypes.func.isRequired,
    doLike: PropTypes.func.isRequired,
    doShare: PropTypes.func.isRequired,
    doSave: PropTypes.func.isRequired,
}

CompanyListItem.propTypes = {
    post: PropTypes.object.isRequired,
    doComment: PropTypes.func.isRequired,
    doLike: PropTypes.func.isRequired,
    doShare: PropTypes.func.isRequired,
    doSave: PropTypes.func.isRequired,
}

export {Companies, Company, CompanyListItem}