import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import {} from '../Actions/Api'

const Contact = ({user: {name, icon, id}, selected, onSelect}) => {
    return (
        <div className="col-xs-3">
            <div className="thumbnail">
                <img src={icon} style={{height: '88px', width: '88px'}} alt=""/>
                <div className="caption">
                    <h4>{name}</h4>
                </div>
                <button onClick={(e) => {
                    e.preventDefault()
                    onSelect()
                }} className={`btn btn-block ${selected ? 'btn-default' : 'btn-primary'}`}>{selected ? "unfollow" : "follow"}</button>
            </div>
        </div>
    )
}


class SettingsCommunity extends Component {
    constructor(props) {
        super(props)
        this.state = {
            selected: {}
        }
    }

    componentDidMount() {
    }

    componentDidUpdate(prevProps) {

    }

    onSelect(user) {
        let {selected} = this.state
        if (selected[user.id]) {
            selected = Object.assign({}, selected)
            delete selected[user.id]
        }
        else {
            selected = {...selected, [user.id]: user}
        }

        this.setState({selected})
    }

    render() {
        const {selected} = this.state


        const _contacts = [0, 1, 2, 3, 4, 5, 6, 6].map((id, index) => {
            const user = {
                id,
                name: "some name",
                icon: "/img/contacts/10.jpg"
            }
            return (
                <Contact key={index} user={user} selected={!!selected[user.id]}  onSelect={this.onSelect.bind(this, user)}/>
            )
        })
        return (

            <div className="container">
                <div className="row">
                    {_contacts}
                </div>

            </div>
        )
    }
}

SettingsCommunity.propTypes = {}

const mapStateToProps = (state, ownProps) => {
    const {profile} = state
    const user_id = profile.data.id

    return {
        user_id,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingsCommunity)