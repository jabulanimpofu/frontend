import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import {Link} from 'react-router-dom'
import {Scrollbars} from 'react-custom-scrollbars'



import {getTimeline, getProductCategories, getSubCategories, getProducts, getServices, getPosts, createPost, save, comment, share, like, follow, invite, loadPhotos, loadPhoto, getActive, getFeatured, getRecentActivity } from '../Actions/Api'





const RecentActivityItem = ({actor, object, verb, description, index: key}) => {
    return (
        <Link to={`/user/${actor.id}`} className="list-group-item media" key={key}>
            <div className="pull-left">
                <img className="avatar-img mCS_img_loaded" src="/img/profile-pics/1.jpg" alt=""/>
            </div>

            <div className="media-body">
                <div className="lgi-heading"><Link to={`/user/${actor.id}`}>{actor.name}</Link></div>
                <small className="lgi-text">{`${verb}ed - ${description}`}</small>
            </div>
        </Link>

    )
}

class Recent extends Component {
    componentDidMount() {
        this.props.doGetRecentActivity()
    }

    componentDidUpdate(prevProps) {
        const {profile, doGetRecentActivity} = this.props
        if (prevProps.profile.data.id !== profile.data.id || prevProps.profile.tokens.recent_activity !== profile.tokens.recent_activity) {
            doGetRecentActivity()
        }

    }

    renderEmail() {
        return (
            <div className="thumbnail" style={{marginRight: 15, marginLeft: 15}}>
                <img src="/img/badge.png" style={{height: '88px', width: '88px'}} alt=""/>
                <div className="caption">
                    <h4>Gold Seller Programme</h4>
                    <div className="m-b-5">
                        <Link to="/gold_seller" className="btn btn-block btn-sm btn-primary waves-effect" role="button">Become a
                            gold seller</Link>
                    </div>
                    <div className="list-group-item media">
                        <div className="pull-left">
                            <img className="avatar-img mCS_img_loaded" src={'/img/gmail.png'} alt=""/>
                        </div>

                        <div className="media-body">
                            <h5>More customers via email</h5>
                            <Link to="/email_invite" className="btn btn-primary btn-sm btn-group-justified" style={{marginRight: 8}}>
                                Connect
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        )
        return (
            <div className="card" style={{marginRight: 15, marginLeft: 15}}>


            </div>
        )
    }

    renderInvite() {
        return (
            <div className="card" style={{marginRight: 15, marginLeft: 15}}>
                <div className="card-header">
                    <h5>More customers via email</h5>
                </div>

                <div className="list-group lg-alt">
                    <div className="list-group-item media">
                        <div className="pull-left">
                            <img className="avatar-img mCS_img_loaded" src={null} alt=""/>
                        </div>

                        <div className="media-body">
                            <Link to="/email_invite"  className="btn btn-primary btn-sm btn-group-justified" style={{marginRight: 8}}>
                                Connect
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    renderFeed(recent_activity) {
        const _recent = !Array.isArray(recent_activity.data) ? [] : recent_activity.data.map((activity, key) => {
            const {object} = activity
            let description = (object.name ? object.name : object.body) + ''
            const props = Object.assign({}, activity, {key, description, index: key})
            return (<RecentActivityItem {...props}  />)
        })

        return (
            <Scrollbars className="list-group lg-alt recent-activity" style={{width: '100%', height: '40%'}}>
                {_recent}
            </Scrollbars>
        )
    }

    render() {
        const {recent_activity} = this.props
        const _email = this.renderEmail()

        const _recent = this.renderFeed(recent_activity)

        return (
            <aside id="s-recent" className="sidebar">
                <div className="smm-header">
                    <i className="zmdi zmdi-long-arrow-left" data-ma-action="sidebar-close"></i>
                </div>
                {_email}
                {_recent}
            </aside>
        )
    }
}

Recent.propTypes = {
    profile: PropTypes.object.isRequired,
    recent_activity: PropTypes.object.isRequired,
    doGetRecentActivity: PropTypes.func.isRequired,
}

const mapStateToProps = (state, ownProps) => {
    const {collections: {recent_activity}, profile} = state
    return {
        recent_activity,
        profile
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    doGetRecentActivity(){
        return dispatch(getRecentActivity())
    },
}}

export default connect(mapStateToProps, mapDispatchToProps)(Recent)