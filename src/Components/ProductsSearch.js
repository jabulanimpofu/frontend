import React, {Component, PropTypes} from 'react'
import Rating from './Rating'
import {Products, ProductsSearchToolbar} from './Product'


class ProductsSearch extends Component {
    constructor(props) {
        super(props)
        this.state = {
            grid: true
        }
    }


    toggleGrid(grid) {
        grid = !!grid
        this.setState({grid})
    }

    render() {
        const {products, view, title} = this.props
        const {grid} = this.state

        return (
            <section id="content">
                <div className="container">
                    <div>
                        <ProductsSearchToolbar grid={grid} toggleView={this.toggleGrid.bind(this)} title={title}/>
                        <Products posts={products} grid={grid} />
                    </div>
                </div>
            </section>
        )
    }
}

ProductsSearch.propTypes = {
    posts: PropTypes.array.isRequired,
    view: PropTypes.string.isRequired,
}

ProductsSearch.defaultProps = {
    posts: [],
    view: 'list',
    title: 'viewing 100 products'
}

export default ProductsSearch