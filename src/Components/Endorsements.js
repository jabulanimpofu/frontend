import React, {Component, PropTypes} from 'react'

const Endorsement = ({endorsement}) => {
    return (
        <div className="col-md-6 col-sm-6 col-xs-6">
            <div className="c-item" style={{padding: 8}}>
                <div className="media">
                    <div className="pull-left">
                        <img className="avatar-img mCS_img_loaded" src="/img/profile-pics/1.jpg" alt=""/>
                    </div>

                    <div className="media-body">
                        <div className="lgi-heading">David Villa Jacobs</div>
                        <small className="lgi-text">Sorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Etiam mattis lobortis sapien non posuere
                        </small>
                    </div>
                </div>

                <div>

                </div>
            </div>
        </div>
    )
}

export class AddEndorsement extends Component {
    render() {
        return (
            <div>
                <Endorsements/>
            </div>)
    }
}

class Endorsements extends Component {
    render() {

        const _endorsements = [1, 2, 3, 4, 5, 6,].map((endorsement, index) => {
            return (
                <Endorsement endorsement={endorsement} key={index}/>
            )
        })

        return (
            <div className="card container">
                <div className="card-header text-center">
                    <h2>Endorsements</h2>
                </div>

                <div className="card-body card-padding">

                    <div className="row" style={{marginBottom: 4}}>
                        <div className="col-xs-6">
                            <span style={{fontSize: 18}}>Authentic Entity</span>
                            <div className="pull-right">
                                <button className="btn btn-primary waves-effect">Endorse</button>
                            </div>
                        </div>
                        <div className="col-xs-6">
                            <div className=" media">
                                <div className="pull-right">20</div>
                                <div className="media-body">
                                    <div className="progress">
                                        <div className="progress-bar progress-bar-danger" role="progressbar"
                                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
                                             style={{width: '20%'}}>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row" style={{marginBottom: 8}}>
                        <div className="col-xs-6">
                            <span style={{fontSize: 18}}>Quality Service/Product</span>
                            <div className="pull-right">
                                <button className="btn btn-primary waves-effect">Endorse</button>
                            </div>
                        </div>
                        <div className="col-xs-6">
                            <div className=" media">
                                <div className="pull-right">60</div>
                                <div className="media-body">
                                    <div className="progress">
                                        <div className="progress-bar progress-bar-info" role="progressbar"
                                             aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
                                             style={{width: '60%'}}>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="contacts c-profile clearfix row">
                        {_endorsements}
                    </div>
                </div>
            </div>
        )
    }
}

Endorsements.propTypes = {}

export default Endorsements