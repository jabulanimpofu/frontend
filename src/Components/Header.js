import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
const $ = window.$
import {API} from '../Constants'
const {GET_TIMELINE, GET_PRODUCTS, GET_SERVICES, GET_POSTS, GET_TIMELINE_LOCATION} = API
import {setFilter, resetFilter} from '../Actions/App'
import {getNotifications, getMessages} from '../Actions/Api'
import {Link} from 'react-router-dom'
import {HeaderSelectField as Select} from './Form/'

const filters = [
    {
        label: 'Products',
        value: GET_PRODUCTS,
        path: '/products/products',
        icon: 'zmdi-shopping-cart'
    },
    {
        label: 'Services',
        value: GET_SERVICES,
        path: '/products/services',
        icon: 'zmdi-case'
    },
    {
        label: 'Companies',
        value: 'companies',
        path: '/companies',
        icon: 'zmdi-store'
    },
    {
        label: 'People',
        value: 'people',
        path: '/people',
        icon: 'zmdi-account-circle'
    },
    {
        label: 'Places',
        value: GET_TIMELINE_LOCATION,
        path: `/products/${GET_TIMELINE_LOCATION.toLowerCase()}`,
        icon: 'zmdi-pin',
    },
]

const Notifications = ({notifications}) => {
    const _recent = !Array.isArray(notifications) ? [] : notifications.map((notification, index) => {
        const {actor, object, verb} = notification
        let description = (object.name ? object.name : object.body) + ''
        return (
            <Link to={`/mail/${notification.id}`} className="list-group-item media" key={index}>
                <div className="pull-left">
                    <img className="avatar-img mCS_img_loaded" src="/img/profile-pics/1.jpg" alt=""/>
                </div>

                <div className="media-body">
                    <div className="lgi-heading">{actor.name}</div>
                    <small className="lgi-text">{`${verb}ed - ${description}`}</small>
                </div>
            </Link>

        )
    })

    return (
        <div className="dropdown-menu pull-right">
            <ul className="sua-menu list-inline list-unstyled palette-Light-Blue-200 bg">
                <li><a href=""><i className="zmdi zmdi-time"></i> Archived</a></li>
                <li><a href=""><i className="zmdi zmdi-check-all"></i> Mark all</a></li>
                <li><a href="" data-ma-action="sidebar-close"><i className="zmdi zmdi-close"></i> Close</a></li>
            </ul>
            {_recent}
        </div>
    )
}

const Messages = ({messages}) => {
    const _recent = !Array.isArray(messages) ? [] : messages.map((notification, index) => {
        const {actor, object, verb} = notification
        let description = (object.name ? object.name : object.body) + ''
        return (
            <Link to={`/mail/${notification.id}`} className="list-group-item media" key={index}>
                <div className="pull-left">
                    <img className="avatar-img mCS_img_loaded" src="/img/profile-pics/1.jpg" alt=""/>
                </div>

                <div className="media-body">
                    <div className="lgi-heading">{actor.name}</div>
                    <small className="lgi-text">{`${verb}ed - ${description}`}</small>
                </div>
            </Link>

        )
    })

    return (
        <div className="dropdown-menu pull-right">
            <ul className="sua-menu list-inline list-unstyled palette-Light-Blue-200 bg">
                <li><a href=""><i className="zmdi zmdi-time"></i> Archived</a></li>
                <li><a href=""><i className="zmdi zmdi-check-all"></i> Mark all</a></li>
                <li><a href="" data-ma-action="sidebar-close"><i className="zmdi zmdi-close"></i> Close</a></li>
            </ul>
            {_recent}
        </div>
    )
}

const Filter = ({filters, isLoggedIn, search, pathname}) => {
    if (!isLoggedIn || !filters)
        return null

    const _filters = filters.map((filter, index) => {
        return (
            <div className="col-xs-2" key={index} style={{padding: 0, textAlign: 'center'}}>
                <Link to={filter.path}
                      style={{color: pathname === filter.path ? '#ff9800' : '#ffffff'}}>
                    <h5 className="filter-icon"><i className={`zmdi ${filter.icon}`}></i></h5>
                    <h5 className="filter-title">{filter.label}</h5>
                </Link>
            </div>
        )
    })

    _filters.unshift(
        <div className="col-xs-2" key={_filters.length} style={{padding: 0, textAlign: 'center'}}>
            <Link to={'/home'} style={{color: pathname === '/home' ? '#ff9800' : '#ffffff'}}>
                <h5 className="filter-icon"><i className={`zmdi zmdi-home`}></i></h5>
                <h5 className="filter-title">Home</h5>
            </Link>
        </div>
    )

    return (
        <div className="h-filter row">
            {_filters}
        </div>
    )
}

const Search = ({term, onChangeField, search}) => {
    return (
        <form className="p-relative">
            <input type="text" className="hs-input" placeholder="Search for people, files & reports"
                   value={term} onChange={onChangeField.bind(undefined, 'term')}/>
            <i className="zmdi zmdi-search hs-reset" data-ma-action="search-clear"></i>
            <i className="zmdi hs-search" data-ma-action="search-clear"></i>
        </form>
    )
}

const PlaceForm = ({place, onChangeField, show}) => {
    if (!show)
        return null
    const field_names = Object.keys(place)
    const _fields = field_names.map((field_name, index) => {
        return (
            <div className="col-xs-12 col-md-3" key={index}>
                <Select
                    label={field_name.toUpperCase()}
                    name={field_name}
                    options={filters}
                    onChange={(value) => {
                        place = Object.assign({}, place, {[field_name]: value})
                        onChangeField("place", place)
                    }}
                />
            </div>
        )
    })

    return (
        <div className="h-filter row">
            {_fields}
        </div>
    )
}

const Menu = ({loggedIn, messages, notifications}) => {
    if (loggedIn)
        return (
            <div className="pull-right">
                <span className="btn btn-primary btn-lg">sign up</span>
            </div>
        )

    return (
        <ul className="pull-right h-menu">
            <li className="hm-search-trigger">
                <a href="" data-ma-action="search-open">
                    <i className="hm-icon zmdi zmdi-search"></i>
                </a>
            </li>

            <li className="dropdown hidden-xs hidden-sm hm-notifications">
                <a data-toggle="dropdown" href="">
                    <i className="hm-icon zmdi zmdi-email"></i>
                </a>
                <Messages messages={messages}/>
            </li>

            <li className="dropdown hidden-xs hidden-sm hm-notifications">
                <a data-toggle="dropdown" href="">
                    <i className="hm-icon zmdi zmdi-notifications"></i>
                </a>
                <Notifications notifications={notifications}/>
            </li>

            <li className="dropdown hm-profile">
                <a data-toggle="dropdown" href="">
                    <img src="/img/profile-pics/1.jpg" alt=""/>
                </a>

                <ul className="dropdown-menu pull-right dm-icon">
                    <li>
                        <Link to={`/profile`}><i className="zmdi zmdi-account"></i> View Profile</Link>
                    </li>
                    <li>
                        <Link to={`/settings`}><i className="zmdi zmdi-settings"></i> Settings</Link>
                    </li>
                    <li>
                        <Link to={`/settings`}><i className="zmdi zmdi-time-restore"></i> Logout</Link>
                    </li>
                </ul>
            </li>
        </ul>
    )
}

class Header extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        const {
            user, doGetNotifications, doGetMessages,
        } = this.props

        if (user.id) {
            doGetNotifications()
            // doGetMessages()
        }

        /* --------------------------------------------------------
         Top Search
         ----------------------------------------------------------*/

        /* Bring search reset icon when focused */
        $('body').on('focus', '.hs-input', function () {
            $('.h-search').addClass('focused');
        });

        /* Take off reset icon if input length is 0, when blurred */
        $('body').on('blur', '.hs-input', function () {
            var x = $(this).val();

            if (!x.length > 0) {
                $('.h-search').removeClass('focused');
            }
        });


        $('body').on('click', '[data-ma-action]', function (e) {
            e.preventDefault();

            var action = $(this).data('ma-action');
            var $this = $(this);

            switch (action) {

                /*-------------------------------------------
                 Mainmenu and Notifications open/close
                 ---------------------------------------------*/

                /* Open Sidebar */
                case 'sidebar-open':

                    var target = $(this).data('ma-target');

                    $this.addClass('toggled');
                    $('#main').append('<div data-ma-action="sidebar-close" className="sidebar-backdrop animated fadeIn" />')

                    if (target == 'main-menu') {
                        $('#s-main-menu').addClass('toggled');
                    }
                    if (target == 'user-alerts') {
                        $('#s-user-alerts').addClass('toggled');
                    }

                    $('body').addClass('o-hidden');

                    break;

                /* Close Sidebar */
                case 'sidebar-close':

                    $('[data-ma-action="sidebar-open"]').removeClass('toggled');
                    $('.sidebar').removeClass('toggled');
                    $('.sidebar-backdrop').remove();
                    $('body').removeClass('o-hidden');

                    break;



                /*----------------------------------
                 Header Search
                 -----------------------------------*/

                /* Clear Search */
                case 'search-clear':

                    /* For mobile only */
                    $('.h-search').removeClass('toggled');

                    /* For all */
                    $('.hs-input').val('');
                    $('.h-search').removeClass('focused');

                    break;

                /* Open search */
                case 'search-open':

                    $('.h-search').addClass('toggled');
                    $('.hs-input').focus();

                    break;



                /*----------------------------------
                 Main menu
                 -----------------------------------*/

                /* Toggle Sub menu */
                case 'submenu-toggle':

                    $this.next().slideToggle(200);
                    $this.parent().toggleClass('toggled');

                    break;



                /*----------------------------------
                 Messages
                 -----------------------------------*/
                case 'message-toggle':

                    $('.ms-menu').toggleClass('toggled');
                    $this.toggleClass('toggled');

                    break;



                /*-------------------------------------------------
                 Action Header Search (used in listview.html)s
                 -------------------------------------------------*/

                //Open action header search
                case 'ah-search-open':
                    const x = $(this).closest('.action-header').find('.ah-search');

                    x.fadeIn(300);
                    x.find('.ahs-input').focus();

                    break;

                //Close action header search
                case 'ah-search-close':
                    x.fadeOut(300);
                    setTimeout(function () {
                        x.find('.ahs-input').val('');
                    }, 350);

                    break;

            }
        });
    }

    componentDidUpdate(prevProps) {
        const {user, doGetNotifications, doGetMessages, filter} = this.props

        if (user.id && user.id !== prevProps.user.id) {
            doGetNotifications()
            // doGetMessages()
        }
    }

    onChangeFilter(field_name, value) {
        const {filter, doSetFilter} = this.props
        doSetFilter({...filter, [field_name]: value})
    }

    render() {
        const {user, isLoggedIn, notifications, messages, filter: {term, place}, location: {pathname}} = this.props

        return (
            <header id="header" className="media header-light" data-ma-header="plus-afrik">
                <div className="pull-left h-logo">
                    <Link to={`/home`} className="hidden-xs">
                    </Link>

                    <div className="menu-collapse" data-ma-action="sidebar-open" data-ma-target="main-menu">
                        <div className="mc-wrap">
                            <div className="mcw-line top palette-White bg"></div>
                            <div className="mcw-line center palette-White bg"></div>
                            <div className="mcw-line bottom palette-White bg"></div>
                        </div>
                    </div>
                </div>

                <Menu isLoggedIn={isLoggedIn} notifications={notifications} messages={messages} user={user}/>

                <div className="h-search">
                    <Search isLoggedIn={isLoggedIn} term={term}
                            onChangeField={this.onChangeFilter.bind(this)}/>
                </div>

                <div className="clearfix"/>
                <Filter isLoggedIn={isLoggedIn} filters={filters} pathname={pathname}/>
                <PlaceForm isLoggedIn={isLoggedIn}
                           show={pathname === `/products/${GET_TIMELINE_LOCATION.toLowerCase()}`}
                           place={place}
                           onChangeField={this.onChangeFilter.bind(this)}/>

            </header>
        )
    }

    renderSearch() {

    }
}

Header.propTypes = {
    isLoggedIn: PropTypes.bool,
    user: PropTypes.object.isRequired,
    search: PropTypes.bool.isRequired,
    filter: PropTypes.object.isRequired,
    doSetFilter: PropTypes.func.isRequired,
    doResetFilter: PropTypes.func.isRequired,
    doGetNotifications: PropTypes.func.isRequired,
    doGetMessages: PropTypes.func.isRequired,
}

Header.defaultProps = {
    search: false,
}

const mapStateToProps = (state, ownProps) => {
    const {profile, profile: {data: user, token}, notifications: notifications_raw, messages, filter} = state
    const isLoggedIn = !!token
    const notifications = notifications_raw.data.activities
    return {
        user, isLoggedIn, filter, notifications, messages, profile
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        doSetFilter(filter){
            return dispatch(setFilter(filter))
        },
        doResetFilter(){
            return dispatch(resetFilter())
        },
        doGetNotifications(){
            return dispatch(getNotifications())
        },
        doGetMessages(){
            return dispatch(getMessages())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header)