import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import {} from '../Actions/Api'

const award = {
    organisation: '',
    award: '',
    year: '',
}

const ProviderOption = ({name, icon, onClick}) => {
    return (
        <div className="col-xs-3">
            <div onClick={(e) => {
                e.preventDefault()
                onClick()
            }} className="thumbnail">
                <img src={icon} style={{height: '88px', width: '88px'}} alt=""/>
                <div className="caption">
                    <h4>{name}</h4>
                </div>
            </div>
        </div>
    )
}

export const Contact = ({user: {name, icon, id}, selected, onSelect}) => {
    const _button = selected ?
        (<button onClick={(e) => e.preventDefault()} className={`btn btn-block btn-default`}>invited</button>) :
        (<button onClick={(e) => {
            e.preventDefault()
            onSelect()
        }} className={`btn btn-block btn-primary`}>invite</button>)
    return (
        <div className="col-xs-3">
            <div className="thumbnail">
                <img src={icon} style={{height: '88px', width: '88px'}} alt=""/>
                <label className="checkbox checkbox-inline" style={{position: 'absolute', right: 0, top: 0}}><input type="checkbox" value="option1" onChange={(e) => {
                        e.preventDefault()
                        onSelect()
                    }} className={selected ? "checked" : ""}/><i className="input-helper"></i></label>
                <div className="caption">
                    <h4>{name}</h4>
                </div>
                {_button}
            </div>
        </div>
    )
}


class SettingsCommunity extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            category: '',
            date: '',
            address: '',
            website: '',
            email: '',
            twitter: '',
            facebook: '',
            linkedin: '',
            skype: '',
            organisation: '',
            capacity: '',
            awards: [],
            user_name: '',
            password: '',
            cpassword: '',
            country: '',
            phone_number: '',
            description: '',
            selected: {}
        }
    }

    componentDidMount() {
    }

    componentDidUpdate(prevProps) {

    }

    onFieldChange(field, value) {
        this.setState({[field]: value})
    }
    onSelect(user) {
        let {selected} = this.state
        if (selected[user.id]) {
            selected = Object.assign({}, selected)
            delete selected[user.id]
        }
        else {
            selected = {...selected, [user.id]: user}
        }

        this.setState({selected})
    }

    onSelectMultiple(){

    }

    render() {
        const {view, profile_id, timeline, doComment, doSave, doShare, doLike, doFollow, doConnect} = this.props
        const {
            name,
            category,
            date,
            address,
            website,
            email,
            street_address,
            city,
            province,
            twitter,
            facebook,
            linkedin,
            skype,
            organisation,
            capacity,
            awards,
            user_name,
            password,
            cpassword,
            country,
            phone_number,
            business_type,
            description,
            selected,
        } = this.state

        const contacts = [0, 1, 2, 3, 4, 5, 6, 6]


        const _contacts = contacts.map((id, index) => {
            const user = {
                id,
                name: "some name",
                icon: "/img/contacts/10.jpg"
            }
            return (
                <Contact key={index} user={user} selected={!!selected[user.id]}  onSelect={this.onSelect.bind(this, user)}/>
            )
        })
        return (

            <div className="container">
                <div className="row">
                    <ProviderOption name="Gmail" icon="/img/icons/gmail_icon.png" onClick={() => alert('clicked')}/>
                    <ProviderOption name="Outlook" icon="/img/icons/outlook_icon.png"
                                    onClick={() => alert('clicked')}/>
                    <ProviderOption name="Yahoo" icon="/img/icons/yahoo_icon.png"
                                    onClick={() => alert('clicked')}/>
                    <ProviderOption name="Other" icon="/img/icons/mail_icon.png"
                                    onClick={() => alert('clicked')}/>
                </div>
                <div className="row">
                    {_contacts}
                </div>

            </div>
        )
    }
}

SettingsCommunity.propTypes = {}

const mapStateToProps = (state, ownProps) => {
    const {profile} = state
    const user_id = profile.data.id

    return {
        user_id,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingsCommunity)