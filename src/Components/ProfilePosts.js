import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import {API} from '../Constants'
const {GET_POSTS, GET_SERVICES, GET_PRODUCTS, GET_TIMELINE_LOCATION, GET_TIMELINE} = API
import {
    getTimeline,
    getProducts,
    getServices,
    getPosts,
    save,
    comment,
    share,
    like,
    follow,
    invite,
} from '../Actions/Api'
import {setFilter, resetFilter, clearNewTimeline} from '../Actions/App'
import {Posts} from './Post'
import CreatePost from './CreatePost'

class ProfilePosts extends Component {

    componentDidMount() {
        const {
            view,
            profile_id,
            filter,
            user_id,
            doGetPosts,
            doGetProducts,
            doGetServices,
            doGetTimeline,
            doSetType,
        } = this.props

        //has id
        if (user_id) {
            if ((view === 'home' || !view) && (filter.type === GET_TIMELINE || !filter.type))
                doGetTimeline()
            if (view === 'products' && filter.type === GET_PRODUCTS)
                doGetProducts()
            else if (view === 'posts' && filter.type === GET_POSTS)
                doGetPosts()
            else if (view === 'services' && filter.type === GET_SERVICES)
                doGetServices()
            else if (view === 'services' && filter.type === GET_SERVICES)
                doGetServices()
            else {
                if (!view || view === "home")
                    doSetType(GET_TIMELINE, filter)
                if (view === "products")
                    doSetType(GET_PRODUCTS, filter)
                if (view === "services")
                    doSetType(GET_SERVICES, filter)
                if (view === "posts")
                    doSetType(GET_POSTS, filter)
            }
        }
    }

    componentDidUpdate(prevProps) {
        const {
            profile_id,
            user_id,
            doGetPosts,
            doGetProducts,
            doGetServices,
            doGetTimeline,
        } = this.props

        //new id
        if (user_id && ((prevProps.user_id !== user_id) || (prevProps.filter.type !== this.props.filter.type))) {
            if (!this.props.filter.type || this.props.filter.type === GET_TIMELINE)
                doGetTimeline()
            if (this.props.filter.type === GET_PRODUCTS)
                doGetProducts()
            if (this.props.filter.type === GET_SERVICES)
                doGetServices()
            if (this.props.filter.type === GET_POSTS)
                doGetPosts()
        }
    }

    render() {
        const {view, profile_id, match: {url}, timeline, doComment, doSave, doShare, doLike, doFollow, doConnect} = this.props
        return (
            <div className="">
                <CreatePost />
                <Posts posts={timeline}
                       doComment={doComment}
                       doSave={doSave}
                       doShare={doShare}
                       doLike={doLike}/>
            </div>
        )
    }
}

ProfilePosts.propTypes = {}

const mapStateToProps = (state, ownProps) => {
    const {collections: {timeline}, profile, filter, user} = state
    const user_id = profile.data.id
    const {2: profile_id, 3: view} = ownProps.match.path.split('/')

    return {
        filter,
        timeline,
        user_id,
        profile_id,
        user,
        view
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        doSetType(type, filter){
            if (type !== filter.type) {
                filter = Object.assign({}, filter, {type})
                return dispatch(setFilter(filter))
            }
        },
        doSetFilter(filter){
            return dispatch(setFilter(filter))
        },
        doResetFilter(){
            return dispatch(resetFilter())
        },
        doGetPosts(){
            return dispatch(getPosts())
        },
        doGetProducts(){
            return dispatch(getProducts())
        },
        doGetServices(){
            return dispatch(getServices())
        },
        doGetTimeline(){
            return dispatch(getTimeline())
        },
        doClearNewTimeline(){
            return dispatch(clearNewTimeline())
        },

        doLike(post_id){
            return dispatch(like(post_id))
        },
        doShare(post_id){
            return dispatch(share(post_id))
        },
        doSave(post_id){
            return dispatch(save(post_id))
        },
        doComment(post_id, comment_text){
            return dispatch(comment(post_id, comment_text))
        },
        doFollow(user_id){
            alert(user_id)
            return dispatch(follow(user_id))
        },
        doConnect(user_id){
            return dispatch(invite(user_id))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePosts)