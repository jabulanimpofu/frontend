import React, {Component, PropTypes} from 'react'
const $ = window.$
import CompaniesSearch from '../Components/CompaniesSearch'

function sparklineLine(id, value, width, height, lineColor, fillColor, lineWidth, maxSpotColor, minSpotColor, spotColor, spotRadius, hSpotColor, hLineColor) {
    $('.'+id).sparkline(value, {
        type: 'line',
        width: width,
        height: height,
        lineColor: lineColor,
        fillColor: fillColor,
        lineWidth: lineWidth,
        maxSpotColor: maxSpotColor,
        minSpotColor: minSpotColor,
        spotColor: spotColor,
        spotRadius: spotRadius,
        highlightSpotColor: hSpotColor,
        highlightLineColor: hLineColor
    });
}

const products = {
    data: [1, 2, 3, 4, 5, 6,].map((review, index) => ({
        object: {},
        id: index
    }))
}

class Followers extends Component {
    componentDidMount() {
        // console.log($(''))
        // sparklineLine('sparkline-1', [9, 5, 6, 3, 9, 7, 5, 4, 6, 5, 6, 4, 9], '100%', 50, 'rgba(255,255,255,0.6)', 'rgba(0,0,0,0)', 1.5, '#fff', '#fff', '#fff', 5, '#fff', '#fff');
    }

    sparkline(_sparkline){
        this._sparkline = _sparkline
        console.log(_sparkline)

        if(_sparkline){
            console.log($(_sparkline))
            sparklineLine('sparkline-1', [9, 5, 6, 3, 9, 7, 5, 4, 6, 5, 6, 4, 9], '100%', 50, 'rgba(0,0,0,0)', 'rgba(255,255,255,0.6)', 1.5, '#ff0', '#ff0', '#ff0', 5, '#ff0', '#ff0');
        }
    }

    render() {
        const _followers = [1, 2, 3, 4, 5, 6,].map((review, index) => {
            return (
                <div review={review} key={index}/>
            )
        })

        const _recent_followers = [1, 2, 3, 4,].map((rating, index) => {
            return (
                <div rating={rating} key={index}/>
            )
        })

        return (
            <div className="card">
                <div className="card-header p-b-0">
                    <h2>Most Recent Followers
                        <small>Followers</small>
                    </h2>
                </div>
                <div className="card-body">
                    <div className="sparkline-1 p-30" ref={this.sparkline.bind(this)}></div>

                    <ul className="rs-list">
                        <li>
                            <a href="">
                                <div className="avatar-char">B</div>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <img className="avatar-img" src="/img/profile-pics/5.jpg" alt=""/>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <div className="avatar-char">L</div>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <div className="avatar-char">A</div>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <img className="avatar-img" src="/img/profile-pics/4.jpg" alt=""/>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <div className="avatar-char">Z</div>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <div className="avatar-char">I</div>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <div className="avatar-char">S</div>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <div className="avatar-char">C</div>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <div className="avatar-char">W</div>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <img className="avatar-img" src="/img/profile-pics/3.jpg" alt=""/>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <div className="avatar-char">A</div>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <img className="avatar-img" src="/img/profile-pics/9.jpg" alt=""/>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <div className="avatar-char">N</div>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <div className="avatar-char">X</div>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <div className="avatar-char">V</div>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <img className="avatar-img" src="/img/profile-pics/7.jpg" alt=""/>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <img className="avatar-img" src="/img/profile-pics/6.jpg" alt=""/>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <img className="avatar-img" src="/img/profile-pics/8.jpg" alt=""/>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <div className="avatar-char">F</div>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <div className="avatar-char">E</div>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <div className="avatar-char">A</div>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <div className="avatar-char">A</div>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <div className="avatar-char">M</div>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <div className="avatar-char">O</div>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <div className="avatar-char">I</div>
                            </a>
                        </li>
                    </ul>

                    <CompaniesSearch products={products}/>


                </div>
            </div>
        )
    }
}

Followers.propTypes = {}

export default Followers