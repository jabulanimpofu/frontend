import React, {Component, PropTypes} from 'react'
import FloatingActionButton from './FloatingActionButton'
import {TextArea} from './Form/'

const CreateReview = ({onFieldChange, onSubmit, rating, review}) => {
    const stars = []
    for (let i = 0; i < 5; i++) {
        if (i < rating)
            stars.push(
                <li key={i}><a data-wpba="video" href="#" onClick={
                    (e) => {
                        e.preventDefault()
                        onFieldChange("rating", i + 1)
                    }
                }><i className="zmdi zmdi-star"></i></a></li>
            )
        else
            stars.push(
                <li key={i}><a data-wpba="video" href="#" onClick={
                    (e) => {
                        e.preventDefault()
                        onFieldChange("rating", i + 1)
                    }
                }><i className="zmdi zmdi-star-outline"></i></a></li>
            )
    }
    return (
        <div className="card">
            <div className="card-body">

                <div className="wall-posting animated ">
                    <div className="card-body card-padding">
                                <TextArea
                                    name="title"
                                    label="Write Something...here"
                                    value={review}
                                    onChange={onFieldChange}/>
                    </div>
                    <ul className="list-unstyled clearfix wpb-actions">
                        <button className="btn btn-primary btn-sm pull-right"
                                onClick={onSubmit}>add review
                        </button>
                        <li className="wpba-attrs">
                            <ul className="list-unstyled list-inline">
                                {stars}
                            </ul>
                        </li>

                    </ul>
                </div>

                <FloatingActionButton/>
            </div>
        </div>
    )
}

const Review = ({review}) => {
    return (
        <div className="col-md-6 col-sm-6 col-xs-6">
            <div className="c-item" style={{padding: 8}}>
                <div className="media">
                    <div className="pull-left">
                        <img className="avatar-img mCS_img_loaded" src="/img/profile-pics/1.jpg" alt=""/>
                    </div>

                    <div className="media-body">
                        <div className="lgi-heading">David Villa Jacobs</div>
                        <small className="lgi-text">Sorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Etiam mattis lobortis sapien non posuere
                        </small>
                    </div>
                </div>

                <p>Donec ullamcorper nulla non metus auctor fringilla. Cras justo odio, dapibus ac facilisis in,
                    egestas eget quam. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh,
                    ut fermentum massa justo sit amet risus. Vestibulum id ligula porta felis euismod semper.
                    Nulla vitae elit libero, a pharetra </p>

                <div className="c-footer">
                    Was this helpful?
                    <button style={{width: 'initial'}} className="waves-effect pull-right"><i
                        className="zmdi zmdi-thumb-up"></i> 30
                    </button>
                    <button style={{width: 'initial'}} className="waves-effect pull-right"><i
                        className="zmdi zmdi-thumb-down"></i> 21
                    </button>
                </div>
            </div>
        </div>
    )
}
const Rating = ({rating}) => {
    return (
        <div className="col-md-6 col-sm-6 col-xs-6">
            <div className="c-item" style={{padding: 8}}>
                <div className="media">
                    <div className="pull-left">
                        <img className="avatar-img mCS_img_loaded" src="/img/profile-pics/1.jpg" alt=""/>
                    </div>

                    <div className="media-body">
                        <div className="lgi-heading">David Villa Jacobs</div>
                        <small className="lgi-text">Sorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Etiam mattis lobortis sapien non posuere
                        </small>
                    </div>
                </div>

                <div className="rl-star">
                    <i className="zmdi zmdi-star active"></i>
                    <i className="zmdi zmdi-star active"></i>
                    <i className="zmdi zmdi-star active"></i>
                    <i className="zmdi zmdi-star"></i>
                    <i className="zmdi zmdi-star"></i>
                </div>

            </div>
        </div>
    )
}

export class AddReview extends Component {
    constructor(props) {
        super(props)
        this.state = {
            rating: 0,
            review: ''
        }
    }

    render() {
        const {rating, review} = this.state
        return (
            <div>
                <CreateReview onFieldChange={this.onFieldChange.bind(this)} onSubmit={this.onSubmit.bind(this)}
                              rating={rating} review={review}/>
                <Reviews />
            </div>
        )
    }

    onSubmit() {

    }

    onFieldChange(field_name, value) {
        this.setState({[field_name]: value})
    }
}

class Reviews extends Component {
    render() {
        const _reviews = [1, 2, 3, 4, 5, 6,].map((review, index) => {
            return (
                <Review review={review} key={index}/>
            )
        })

        const _ratings = [1, 2, 3, 4,].map((rating, index) => {
            return (
                <Rating rating={rating} key={index}/>
            )
        })

        return (
            <div className="card container">
                <div className="row">
                    <div className="rating-list col-xs-6">
                        <div className="card-header text-center">
                            <h2>Average Rating 3.0</h2>
                            <div className="rl-star">
                                <i className="zmdi zmdi-star active"></i>
                                <i className="zmdi zmdi-star active"></i>
                                <i className="zmdi zmdi-star active"></i>
                                <i className="zmdi zmdi-star"></i>
                                <i className="zmdi zmdi-star"></i>
                            </div>
                        </div>
                        <div className="card-body">
                            <div className="list-group lg-alt">
                                <div className="list-group-item media">
                                    <div className="pull-left">
                                        1 <i className="zmdi zmdi-star"></i>
                                    </div>

                                    <div className="pull-right">20</div>

                                    <div className="media-body">
                                        <div className="progress">
                                            <div className="progress-bar progress-bar-danger" role="progressbar"
                                                 aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
                                                 style={{width: '20%'}}>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="list-group-item media">
                                    <div className="pull-left">
                                        2 <i className="zmdi zmdi-star"></i>
                                    </div>

                                    <div className="pull-right">45</div>

                                    <div className="media-body">
                                        <div className="progress">
                                            <div className="progress-bar progress-bar-warning" role="progressbar"
                                                 aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"
                                                 style={{width: '45%'}}>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="list-group-item media">
                                    <div className="pull-left">
                                        3 <i className="zmdi zmdi-star"></i>
                                    </div>

                                    <div className="pull-right">60</div>

                                    <div className="media-body">
                                        <div className="progress">
                                            <div className="progress-bar progress-bar-warning" role="progressbar"
                                                 aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
                                                 style={{width: '60%'}}>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="list-group-item media">
                                    <div className="pull-left">
                                        4 <i className="zmdi zmdi-star"></i>
                                    </div>

                                    <div className="pull-right">78</div>

                                    <div className="media-body">
                                        <div className="progress">
                                            <div className="progress-bar progress-bar-success" role="progressbar"
                                                 aria-valuenow="78" aria-valuemin="0" aria-valuemax="100"
                                                 style={{width: '78%'}}>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="list-group-item media">
                                    <div className="pull-left">
                                        5 <i className="zmdi zmdi-star"></i>
                                    </div>

                                    <div className="pull-right">22</div>

                                    <div className="media-body">
                                        <div className="progress">
                                            <div className="progress-bar progress-bar-info" role="progressbar"
                                                 aria-valuenow="22" aria-valuemin="0" aria-valuemax="100"
                                                 style={{width: '22%'}}>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="rating-list col-xs-6">
                        <div className="card-header text-center">
                            <h2 style={{marginBottom: 35}}>Verified</h2>
                        </div>
                        <div className="card-body">
                            <div className="list-group lg-alt">
                                <div className="list-group-item media">
                                    <div className="pull-left">
                                        <div className="side-badge">
                                            <img src="/img/icons/gold_supplier.jpg" alt=""/>
                                        </div>
                                    </div>

                                    <div className="media-body">
                                        <div className="lgi-heading">Gold Seller</div>
                                        <small className="lgi-text">20 Reviews</small>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div className="col-xs-12">
                        <ul className="tab-nav tn-justified" role="tablist">
                            <li role="presentation" className="active">
                                <a className="col-sx-4" href="#tab-1" aria-controls="tab-1" role="tab"
                                   data-toggle="tab">
                                    All Reviews
                                </a>
                            </li>
                            <li role="presentation">
                                <a className="col-xs-4" href="#tab-2" aria-controls="tab-2" role="tab"
                                   data-toggle="tab">
                                    Star Rating
                                </a>
                            </li>
                        </ul>
                        <div className="tab-content p-0">
                            <div role="tabpanel" className="wall-posting tab-pane animated fadeIn in active" id="tab-1">
                                <div className="card-body card-padding">
                                    <div className="contacts c-profile clearfix row">
                                        {_reviews}
                                    </div>
                                </div>

                            </div>
                            <div role="tabpanel" className="wall-posting tab-pane animated fadeIn" id="tab-2">
                                <div className="card-body card-padding">
                                    <div className="contacts c-profile clearfix row">
                                        {_ratings}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

Reviews.propTypes = {}

export default Reviews