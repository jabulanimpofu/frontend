import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import {createPost, createProduct, createService, createUpload, getProductCategories, getSubCategories} from '../Actions/Api'
import Dropzone from 'react-dropzone'
import FloatingActionButton from './FloatingActionButton'

import {TextAreaField, TextArea, TextField, SelectField} from './Form/'

const unit_options = [
    {
        label: 'Some label',
        value: 'some value'
    },
    {
        label: 'Some second label',
        value: 'some second value'
    },
]

const dropzone_style = {
    paddingTop: '25%',
}

const dropzone_inactive_style = {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    borderWidth: 4,
    borderColor: 'rgb(102, 102, 102)',
    borderStyle: 'dashed',
    borderRadius: 5
}

const getDisplayCategories = (raw) => {
    return !Array.isArray(raw.data) ? [] : raw.data.map(category=> {
        return {
            label: category.name,
            value: category.id,
            slug: category.category_slug
        }
    })
}

const getDisplaySubCategories = (raw, cat_id) => {
    return !Array.isArray(raw.data) ? [] : raw.data.reduce((sub_categories, sub_category) => {
        if(sub_category.product_category_id === cat_id)
            sub_categories.push({
                label: sub_category.sub_category_name,
                value: sub_category.id,
            })
        return sub_categories
    }, [])
}

class CreatePost extends Component {

    constructor(props) {
        super(props)
        this.state = {
            title: '',
            description: '',
            category: '',
            sub_category: '',
            price: '',
            unit: '',
            images: [],
            categories: [],
            sub_categories: []
        }
    }

    componentDidMount() {
        const { doGetProductCategories, doGetSubCategories } = this.props
        doGetProductCategories()
        doGetSubCategories()
        this.setState({
            categories: getDisplayCategories(this.props.product_categories),
            sub_categories: getDisplaySubCategories(this.props.sub_categories, this.state.category),
        })
    }

    componentDidUpdate (prevProps, prevState) {
        if(this.state.category && this.state.category !== prevState.category ){
            this.setState({
                categories: getDisplayCategories(this.props.product_categories),
                sub_categories: getDisplaySubCategories(this.props.sub_categories, this.state.category),
            })
        }

        if(prevProps.product_categories !== this.props.product_categories || prevProps.sub_categories !== this.props.sub_categories ){
            this.setState({
                categories: getDisplayCategories(this.props.product_categories),
                sub_categories: getDisplaySubCategories(this.props.sub_categories, this.state.category),
            })
        }
    }

    onDrop(images) {
        this.setState({images})
        // this.props.doUpload(images[0])
    }

    onOpenClick(e) {
        e.preventDefault()
        this.dropzone.open();
    }

    onFieldChange(field, value) {
        this.setState({[field]: value})
    }

    onSubmit(type) {
        const {title, description, category, sub_category, price, unit, images} = this.state
        const {createPost, createService, createProduct} = this.props
        const image = images[0]

        if (type === 'post') {
            const payload = {
                body: title,
                image
            }
            return createPost(payload)
        }

        if (type === 'product') {
            const payload = {
                name: title,
                body: description,
                price,
                image,
                category,
                sub_category,
            }
            return createProduct(payload)
        }

        if (type === 'service') {
            const payload = {
                name: title,
                body: description,
                price,
                rate: unit,
                image,
                category,
                sub_category,
            }
            return createService(payload)
        }

    }

    removePicture(index, e){
        e.preventDefault()
        const {images: currentImages} = this.state
        const images = [
            ...currentImages.slice(0, index),
            ...currentImages.slice(index+1),
        ]
        this.setState({images})
    }

    renderImages() {
        const {images} = this.state
        const style = {marginRight: 0, marginLeft: 0, display: images.length ? 'block' : 'none'}

        const _images = images.map((image, index) => {
            const {type, size, webkitRelativePath, name, preview, lastModified, lastModifiedDate, progress, url} = image
            return (
                <div key={index} className="col-xs-3 upload_pic" style={{backgroundImage: `url(${preview})`}}
                >
                    <a href="" className="upload-edit" onClick={this.removePicture.bind(this, index)}>
                        <i className="zmdi zmdi-camera"></i> <span
                        className="hidden-xs">Remove</span>
                    </a>
                </div>
            )
        })

        const _dropzone = (
            <Dropzone className="col-xs-3" style={dropzone_style} ref={(dropzone) => this.dropzone = dropzone}
                      onDrop={this.onDrop.bind(this)}>
                <div style={dropzone_inactive_style}></div>
            </Dropzone>
        )

        return (
            <div className="wp-media row" style={style}>
                {_images}
                {_dropzone}
            </div>
        )
    }


    render() {
        const {title, description, category, sub_category, price, unit, sub_categories: sub_category_options, categories: category_options } = this.state
        const _images = this.renderImages.bind(this)()

        return (
            <div className="card">
                <div className="card-body">
                    <ul className="tab-nav tn-justified" role="tablist">
                        <li role="presentation" className="active">
                            <a className="col-sx-4" href="#tab-1" aria-controls="tab-1" role="tab" data-toggle="tab">
                                <i className="zmdi zmdi-comments icon-tab"></i> Share Update
                            </a>
                        </li>
                        <li role="presentation">
                            <a className="col-xs-4" href="#tab-2" aria-controls="tab-2" role="tab" data-toggle="tab">
                                <i className="zmdi zmdi-shopping-cart icon-tab"></i> Add Product
                            </a>
                        </li>
                        <li role="presentation">
                            <a className="col-xs-4" href="#tab-3" aria-controls="tab-3" role="tab" data-toggle="tab">
                                <i className="zmdi zmdi-case icon-tab"></i> Add Service
                            </a>
                        </li>
                    </ul>

                    <div className="tab-content p-0">

                        <div role="tabpanel" className="wall-posting tab-pane animated fadeIn in active" id="tab-1">
                            <div className="card-body card-padding">
                                <TextArea
                                    name="title"
                                    label="Write Something...here"
                                    value={title}
                                    onChange={this.onFieldChange.bind(this)}/>
                                {_images}
                            </div>
                            <ul className="list-unstyled clearfix wpb-actions">
                                <button className="btn btn-primary btn-sm pull-right"
                                        onClick={this.onSubmit.bind(this, 'post')}>post
                                </button>
                                <li className="wpba-attrs">
                                    <ul className="list-unstyled list-inline">
                                        <li><a data-wpba="image" href="#" onClick={this.onOpenClick.bind(this)}><i
                                            className="zmdi zmdi-image"></i></a></li>
                                        <li><a data-wpba="video" href="#" onClick={this.onOpenClick.bind(this)}><i
                                            className="zmdi zmdi-play-circle"></i></a></li>
                                    </ul>
                                </li>

                            </ul>
                        </div>

                        <div role="tabpanel" className="wall-posting tab-pane animated fadeIn" id="tab-2">
                            <div className="row card-body card-padding">

                                <TextField value={title} name="title" label="What are you selling?" size="col-xs-12"
                                           onChange={this.onFieldChange.bind(this)}/>

                                <SelectField value={category}
                                             options={category_options}
                                             name="category"
                                             label="Pick Category"
                                             size="col-xs-6"
                                             onChange={this.onFieldChange.bind(this)}/>

                                <SelectField value={sub_category} options={sub_category_options} name="sub_category"
                                             label="Pick Sub-Category" size="col-xs-6"
                                             onChange={this.onFieldChange.bind(this)}/>

                                <TextField value={price} name="price" label="How Much?" size="col-xs-12"
                                           onChange={this.onFieldChange.bind(this)}/>

                                <TextAreaField value={description} name="description" label="Product Description"
                                               size="col-xs-12" onChange={this.onFieldChange.bind(this)}/>

                                {_images}
                            </div>
                            <ul className="list-unstyled clearfix wpb-actions">
                                <button className="btn btn-primary btn-sm pull-right"
                                        onClick={this.onSubmit.bind(this, 'product')}>post
                                </button>
                                <li className="wpba-attrs">
                                    <ul className="list-unstyled list-inline">
                                        <li><a data-wpba="image" href="#" onClick={this.onOpenClick.bind(this)}><i
                                            className="zmdi zmdi-image"></i></a></li>
                                        <li><a data-wpba="video" href="#" onClick={this.onOpenClick.bind(this)}><i
                                            className="zmdi zmdi-play-circle"></i></a></li>
                                    </ul>
                                </li>

                            </ul>
                        </div>

                        <div role="tabpanel" className="wall-posting tab-pane animated fadeIn" id="tab-3">
                            <div className="row card-body card-padding">

                                <TextField value={title} name="title" label="What are you selling?" size="col-xs-12"
                                           onChange={this.onFieldChange.bind(this)}/>

                                <SelectField value={category} options={category_options} name="category"
                                             label="Pick Category" size="col-xs-12"
                                             onChange={this.onFieldChange.bind(this)}/>

                                <TextField value={price} name="price" label="Rate" size="col-xs-6"
                                           onChange={this.onFieldChange.bind(this)}/>

                                <SelectField value={unit} options={unit_options} name="unit" label="Unit"
                                             size="col-xs-6" onChange={this.onFieldChange.bind(this)}/>

                                <TextAreaField value={description} name="description" label="Service Description"
                                               size="col-xs-12" onChange={this.onFieldChange.bind(this)}/>

                                {_images}
                            </div>
                            <ul className="list-unstyled clearfix wpb-actions">
                                <button className="btn btn-primary btn-sm pull-right"
                                        onClick={this.onSubmit.bind(this, 'service')}>post
                                </button>
                                <li className="wpba-attrs">
                                    <ul className="list-unstyled list-inline">
                                        <li><a data-wpba="image" href="#" onClick={this.onOpenClick.bind(this)}><i
                                            className="zmdi zmdi-image"></i></a></li>
                                        <li><a data-wpba="video" href="#" onClick={this.onOpenClick.bind(this)}><i
                                            className="zmdi zmdi-play-circle"></i></a></li>
                                    </ul>
                                </li>

                            </ul>
                        </div>

                    </div>

                    <FloatingActionButton/>
                </div>
            </div>
        )
    }
}

CreatePost.propTypes = {
    createPost: PropTypes.func.isRequired,
    active_users: PropTypes.object.isRequired,
    product_categories: PropTypes.object.isRequired,
    doGetProductCategories: PropTypes.func.isRequired,
    doGetSubCategories: PropTypes.func.isRequired,
}

const mapStateToProps = (state, ownProps) => {
    return {
        active_users: state.collections.active_users,
        product_categories: state.collections.product_categories,
        sub_categories: state.collections.sub_categories,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        createPost(post){
            return dispatch(createPost(post))
        },
        createService(service){
            return dispatch(createService(service))
        },
        createProduct(product){
            return dispatch(createProduct(product))
        },
        createUpload(post){
            return dispatch(createUpload(post))
        },
        doGetProductCategories(){
            return dispatch(getProductCategories())
        },
        doGetSubCategories(){
            return dispatch(getSubCategories())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreatePost)