import React, {Component, PropTypes} from 'react'
import {Link} from 'react-router-dom'
import '../less/inc/pricing-table.less'

const Price = ({price}) => {
    const className = `bgm-${price.color}`
    const _features = price.features.map((feature, index) => {
        return (
            <div className="ptib-item" key={index}>
                {feature}
            </div>
        )
    })
    return (
        <div className="col-sm-4">
            <div className="card pt-item">
                <div className={`pti-header ${className}`}>
                    <h2>{price.price}
                        <small>| {price.period}</small>
                    </h2>
                    <div className="ptih-title">{price.name}</div>
                </div>

                <div className="pti-body">
                    {_features}
                </div>

                <div className="pti-footer">
                    <a href="" className={className}><i className="zmdi zmdi-check"></i></a>
                </div>
            </div>
        </div>
    )
}

class PricingTable extends Component {
    render() {
        const {prices} = this.props
        const _prices = prices.map((price, index) => {
            return <Price price={price} key={index}/>
        })
        return (
            <div className="row pricing-table">
                {_prices}
            </div>
        )
    }
}

PricingTable.propTypes = {}

export default PricingTable