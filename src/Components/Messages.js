import React, {Component, PropTypes} from 'react'
import RecommendedSellers from '../Containers/RecommendedSellers'
import FeaturedProducts from '../Containers/FeaturedProducts'
import {Link} from 'react-router-dom'

class Messages extends Component {
    render() {
        const {posts} = this.props
        return (
            <section id="content">
                <div className="container">
                    <div className="card clearfix" id="messages">
                        <div className="ms-menu">

                            <div className="list-group lg-alt m-t-10">
                                <a className="list-group-item media" href="">
                                    <div className="pull-left">
                                        <img src="/img/profile-pics/4.jpg" alt="" className="avatar-img"/>
                                    </div>
                                    <div className="media-body">
                                        <div className="lgi-heading">Davil Parnell</div>
                                        <small className="lgi-text">Fierent fastidii recteque ad pro</small>
                                    </div>
                                </a>

                                <a className="list-group-item media" href="">
                                    <div className="pull-left">
                                        <img src="/img/profile-pics/2.jpg" alt="" className="avatar-img"/>
                                    </div>
                                    <div className="media-body">
                                        <div className="lgi-heading">Ann Watkinson</div>
                                        <small className="lgi-text">Cum sociis natoque penatibus </small>
                                    </div>
                                </a>

                                <a className="list-group-item media" href="">
                                    <div className="pull-left">
                                        <img src="/img/profile-pics/3.jpg" alt="" className="avatar-img"/>
                                    </div>
                                    <div className="media-body">
                                        <div className="lgi-heading">Marse Walter</div>
                                        <small className="lgi-text">Suspendisse sapien ligula</small>
                                    </div>
                                </a>

                                <a className="list-group-item media" href="">
                                    <div className="pull-left">
                                        <img src="/img/profile-pics/2.jpg" alt="" className="avatar-img"/>
                                    </div>
                                    <div className="media-body">
                                        <div className="lgi-heading">Jeremy Robbins</div>
                                        <small className="lgi-text">Phasellus porttitor tellus nec</small>
                                    </div>
                                </a>

                                <a className="list-group-item media" href="">
                                    <div className="pull-left">
                                        <img src="/img/profile-pics/4.jpg" alt="" className="avatar-img"/>
                                    </div>
                                    <div className="media-body">
                                        <div className="lgi-heading">Reginald Horace</div>
                                        <small className="lgi-text">Quisque consequat arcu eget</small>
                                    </div>
                                </a>

                                <a className="list-group-item media" href="">
                                    <div className="pull-left">
                                        <img src="/img/profile-pics/5.jpg" alt="" className="avatar-img"/>
                                    </div>
                                    <div className="media-body">
                                        <div className="lgi-heading">Shark Henry</div>
                                        <small className="lgi-text">Nam lobortis odio et leo maximu</small>
                                    </div>
                                </a>

                                <a className="list-group-item media" href="">
                                    <div className="pull-left">
                                        <img src="/img/profile-pics/2.jpg" alt="" className="avatar-img"/>
                                    </div>
                                    <div className="media-body">
                                        <div className="lgi-heading">Paul Van Dack</div>
                                        <small className="lgi-text">Nam posuere purus sed velit auctor sodales</small>
                                    </div>
                                </a>

                                <a className="list-group-item media" href="">
                                    <div className="pull-left">
                                        <img src="/img/profile-pics/1.jpg" alt="" className="avatar-img"/>
                                    </div>
                                    <div className="media-body">
                                        <div className="lgi-heading">James Anderson</div>
                                        <small className="lgi-text">Vivamus imperdiet sagittis quam</small>
                                    </div>
                                </a>

                                <a className="list-group-item media" href="">
                                    <div className="pull-left">
                                        <img src="/img/profile-pics/3.jpg" alt="" className="avatar-img"/>
                                    </div>
                                    <div className="media-body">
                                        <div className="lgi-heading">Kane Williams</div>
                                        <small className="lgi-text">Suspendisse justo nulla luctus nec</small>
                                    </div>
                                </a>
                            </div>

                        </div>

                        <div className="ms-body">
                            <div className="list-group lg-alt">
                                <div className="list-group-item media">
                                    <div className="pull-left">
                                        <img className="avatar-img" src="/img/profile-pics/1.jpg" alt=""/>
                                    </div>

                                    <div className="media-body">
                                        <div>
                                            <div className="msb-item">
                                                Quisque consequat arcu eget odio cursus, ut tempor arcu vestibulum. Etiam ex arcu, porta a urna non, lacinia pellentesque orci. Proin semper sagittis erat, eget condimentum sapien viverra et. Mauris volutpat magna nibh, et condimentum est rutrum a. Nunc sed turpis mi. In eu massa a sem pulvinar lobortis.
                                            </div>
                                        </div>
                                        <small className="ms-date"><i className="zmdi zmdi-time"></i> 20/02/2015 at 09:00</small>
                                    </div>
                                </div>

                                <div className="list-group-item media">
                                    <div className="pull-right">
                                        <img className="avatar-img" src="/img/profile-pics/8.jpg" alt=""/>
                                    </div>
                                    <div className="media-body">
                                        <div>
                                            <div className="msb-item">
                                                Mauris volutpat magna nibh, et condimentum est rutrum a. Nunc sed turpis mi. In eu massa a sem pulvinar lobortis.
                                            </div>
                                        </div>
                                        <div>
                                            <div className="msb-item">
                                                Condimentum est rutrum lobortis.
                                            </div>
                                        </div>
                                        <div>
                                            <div className="msb-item">
                                                :)
                                            </div>
                                        </div>
                                        <small className="ms-date"><i className="zmdi zmdi-time"></i> 20/02/2015 at 09:30</small>
                                    </div>
                                </div>

                                <div className="list-group-item media">
                                    <div className="pull-left">
                                        <img className="avatar-img" src="/img/profile-pics/1.jpg" alt=""/>
                                    </div>
                                    <div className="media-body">
                                        <div>
                                            <div className="msb-item">
                                                Etiam ex arcumentum
                                            </div>
                                        </div>
                                        <small className="ms-date"><i className="zmdi zmdi-time"></i> 20/02/2015 at 09:33</small>
                                    </div>
                                </div>

                                <div className="list-group-item media">
                                    <div className="pull-right">
                                        <img className="avatar-img" src="/img/profile-pics/8.jpg" alt=""/>
                                    </div>
                                    <div className="media-body">
                                        <div>
                                            <div className="msb-item">
                                                Etiam nec facilisis lacus. Nulla imperdiet augue ullamcorper dui ullamcorper, eu laoreet sem consectetur. Aenean et ligula risus. Praesent sed posuere sem. Cum sociis natoque penatibus et magnis dis parturient montes,
                                            </div>
                                        </div>
                                        <small className="ms-date"><i className="zmdi zmdi-time"></i> 20/02/2015 at 10:10</small>
                                    </div>
                                </div>

                                <div className="list-group-item media">
                                    <div className="pull-left">
                                        <img className="avatar-img" src="/img/profile-pics/1.jpg" alt=""/>
                                    </div>
                                    <div className="media-body">
                                        <div>
                                            <div className="msb-item">
                                                Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam ac tortor ut elit sodales varius. Mauris id ipsum id mauris malesuada tincidunt. Vestibulum elit massa, pulvinar at sapien sed, luctus vestibulum eros. Etiam finibus tristique ante, vitae rhoncus sapien volutpat eget
                                            </div>
                                        </div>
                                        <small className="ms-date"><i className="zmdi zmdi-time"></i> 20/02/2015 at 10:24</small>
                                    </div>
                                </div>

                                <div className="ms-reply">
                                    <textarea placeholder="What's on your mind..."></textarea>

                                    <button><i className="zmdi zmdi-mail-send"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

Messages.propTypes = {

}

export default Messages