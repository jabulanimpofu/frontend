import React, {Component, PropTypes} from 'react'
import {Posts} from './Post'
import CreatePost from './CreatePost'
import RecommendedSellers from '../Containers/RecommendedSellers'
import FeaturedProducts from '../Containers/FeaturedProducts'
import CopyRight from './CopyRight'


class Wall extends Component {
    render() {
        const {posts, active_users, featured, createPost, product_categories, sub_categories, doComment, doSave, doShare, doLike, doFollow, doConnect, doGetProductCategories, doGetSubCategories} = this.props

        return (
            <section id="content">
                <div className="container">

                    <div className="row">
                        <div className="col-lg-8 col-md-8 co-xs-12">
                            <CreatePost />
                            <Posts posts={posts}
                                   doComment={doComment}
                                   doSave={doSave}
                                   doShare={doShare}
                                   doLike={doLike}/>

                        </div>

                        <div className="col-lg-4 col-md-4 hidden-sm" style={{
                            // position: '-webkit-sticky',
                            position: 'sticky',
                        }}>
                            <RecommendedSellers/>
                            <FeaturedProducts/>
                            <CopyRight />
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

Wall.propTypes = {
    active_users: PropTypes.object.isRequired,
    posts: PropTypes.object.isRequired,
    createPost: PropTypes.func.isRequired,
    clearNewTimeline: PropTypes.func.isRequired,
}

export default Wall