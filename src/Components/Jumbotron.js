import React, {Component, PropTypes} from 'react'

class Jumbotron extends Component {
    render() {
        return (
            <div  className="gold_seller">
                <div className=" bg_gold_seller jumbotron">
                    {this.props.children}
                </div>
            </div>
        )
    }
}

Jumbotron.propTypes = {}

export default Jumbotron