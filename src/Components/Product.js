import React, {Component, PropTypes} from 'react'
import {CollectionsList, CreateCollection} from './Collections'
import {Link} from 'react-router-dom'
const $ = window.$

export const ProductsSearchToolbar = ({grid, toggleView, title}) => {
    const onClick = (grid, e) => {
        e.preventDefault()
        toggleView(grid)
    }
    return (
        <div className="action-header palette-Plus-Afrik-Light bg clearfix">
            <div className="ah-label hidden-xs palette-Plus-Afrik-Light text">{title}</div>

            <ul className="ah-actions actions a-alt">
                <li>
                    <a href="" onClick={onClick.bind(undefined, true)}>
                        <i className="zmdi zmdi-view-module palette-Plus-Afrik-Light text"></i>
                    </a>
                </li>
                <li>
                    <a href="" onClick={onClick.bind(undefined, false)}>
                        <i className="zmdi zmdi-view-list palette-Plus-Afrik-Light text"></i>
                    </a>
                </li>
            </ul>
        </div>
    )
}

class Product extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const {index, onAddToCollection, onSelect} = this.props
        return (
            <div className="col-md-4 col-sm-4 col-xs-4" key={index} onClick={(e) => {
                e.preventDefault()
                onSelect()
            }}>
                <div className="c-item">
                    <div className="ci-avatar">
                        <img src="/img/product.jpg" alt=""/>
                    </div>

                    <div className="c-info">
                        <strong>Apple Macbook</strong>
                        <small><b>$1400</b></small>
                        <small>Phasellus at ultricies neque, quis malesuada augue. Donec eleifend condimentum nisl eu
                            consectetur.
                        </small>
                        <small><b>Sellers name</b></small>
                        <small><b>Harare, Zimbabwe</b></small>
                    </div>

                    <div className="btn-group btn-group-justified" role="group" aria-label="...">
                        <div className="btn-group" role="group">
                            <button type="button" className="btn btn-default btn-icon-text waves-effect"><i
                                className="zmdi zmdi-favorite"></i></button>
                        </div>
                        <div className="btn-group" role="group">
                            <span className="dropdown pmop-message" style={{width: '100%'}}>
                                <button data-toggle="dropdown" type="button"
                                        className="btn btn-default btn-icon-text waves-effect"><i
                                    className="zmdi zmdi-mail-send"></i></button>
                        <div className="dropdown-menu">
                            <textarea
                                placeholder={`Write message to X about product X...`}></textarea>

                            <button style={{width: 50}} className="btn bgm-green btn-float"><i
                                className="zmdi zmdi-mail-send"></i></button>
                        </div>
                    </span>
                        </div>
                        <div className="btn-group" role="group">
                            <button onClick={(e) => {
                                e.preventDefault()
                                onAddToCollection()
                            }} type="button" className="btn btn-default btn-icon-text waves-effect"><i
                                className="zmdi zmdi-download"></i></button>
                        </div>
                    </div>

                </div>
            </div>
        )
    }

}

class ProductListItem extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const {index, doFollow, onAddToCollection} = this.props
        return (
            <div className="card row" key={index} style={{marginLeft: 0, marginRight: 0}}>
                <div className="col-xs-12 col-sm-8 col-md-9 plus-card-left">
                    <div className="card-header lg" style={{paddingLeft: 0, paddingRight: 0}}>
                        <h2>
                            Pellentesque Ligula Fringilla

                            <small>by Malinda Hollaway on 19th June 2015 at 09:10 AM</small>
                        </h2>
                    </div>
                    <div className="card-header sm">
                        <div className="media">
                            <div className="pull-left">
                                <img className="avatar-img a-lg" src="/img/profile-pics/profile-pic-2.jpg" alt=""/>
                            </div>

                            <div className="media-body m-t-5">
                                <h2>Pellentesque Ligula Fringilla
                                    <small>Posted by Jabulan Mpofu</small>
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div className="card-header ch-img"
                         style={{backgroundImage: 'url(img/demo/note.png)', height: 250}}></div>
                    <div className="card-body card-padding"
                         style={{paddingTop: 30, paddingLeft: 0, paddingRight: 0}}>
                        <p>Donec ullamcorper nulla non metus auctor fringilla. Cras justo odio, dapibus ac facilisis
                            in, egestas eget quam. Fusce dapibus, tellus ac cursus commodo, tortor mauris
                            condimentum nibh, ut fermentum massa justo sit amet risus. Vestibulum id ligula porta
                            felis euismod semper. Nulla vitae elit libero, a pharetra </p>
                        <ul className="wall-attrs clearfix list-inline list-unstyled">
                            <li className="wa-stats">
                                <span className="active"><i className="zmdi zmdi-favorite"></i> 2432</span>
                                <span onClick={(e) => {
                                    e.preventDefault()
                                    onAddToCollection()
                                }}><i className="zmdi zmdi-download"></i> Save</span>
                                <span className="dropdown pmop-message">
                                <span data-toggle="dropdown"><i className="zmdi zmdi-mail-send"></i> Message</span>
                                <div className="dropdown-menu">
                                    <textarea placeholder={`Write message to about product...`}></textarea>
                                    <button className="btn bgm-green btn-float"><i className="zmdi zmdi-mail-send"></i></button>
                        </div>
                    </span>
                                <span onClick={(e) => {
                                    e.preventDefault()
                                    onAddToCollection()
                                }}><i className="zmdi zmdi-account-add"></i> Follow</span>
                            </li>
                        </ul>
                    </div>


                </div>
                <div className="hidden-xs col-sm-4 col-md-3 plus-card-right">
                    <div className="avatar-fullwidth"
                         style={{
                             backgroundImage: `url('/img/profile-pics/profile-pic-2.jpg')`,
                             backgroundColor: `white`
                         }}></div>
                    <h3>Nyash L Guzha</h3>
                    <h6>Graphic Designer</h6>
                    <h6>Guzha Advertising</h6>
                    <h6>Harare, Zimbabwe</h6>
                    <h6>15K</h6>
                    <h6>verified reviews</h6>
                    <h6>10K</h6>
                    <h6>Endorsements</h6>
                    <button className="btn btn-block btn-primary" onClick={(e) => {
                        e.preventDefault()
                        doFollow()
                    }}>follow
                    </button>

                </div>
            </div>
        )
    }
}

class Products extends Component {
    constructor(props) {
        super(props)
        this.state = {
            modal: 'add_to_collection',
            active: null
        }
    }

    onAddToCollection() {
        this.setState({modal: 'add_to_collection'})
        $(this.modal).modal('show')
    }

    onAddCollection() {
        this.setState({modal: 'add_collection'})
        $(this.modal).modal('show')
    }

    onSelect(active) {
        this.setState({active})
    }

    renderModal() {
        const {modal} = this.state
        switch (modal) {
            case 'add_to_collection':
                return (<CollectionsList collections={[0, 2, 3, 4, 5, 6, 7]}
                                         onAddCollection={this.onAddCollection.bind(this)}
                                         onSelectCollection={alert}/>)

            case 'add_collection':
                return (<CreateCollection />)
            default:
                return null
        }


    }

    render() {
        const {posts, grid, doComment, doLike, doShare, doSave} = this.props
        const {active} = this.state
        const _modal = this.renderModal.bind(this)()

        let active_item = null
        const list = !Array.isArray(posts.data) ? [] : posts.data.reduce((list, post, index) => {
            if (isNaN(active)) {
                list.push(post)
                return list
            }

            //if is the active one
            if (post.id === active) {
                active_item = post
            }
            else {
                list.push(post)
            }

            if (active_item && !(index % 3)) {
                list.push(active_item)
                active_item = null
            }
            return list
        }, [])

        if (active_item) {
            list.push(active_item)
        }

        let oldestNew = -1
        let _posts = list.reduce((_posts, post, index) => {
            if (post.isNew) {
                oldestNew = index
            }

            if (grid && !isNaN(active) && active === post.id) {
                _posts.push(<div className="clearfix" key={"clearfix1"}/>)
                _posts.push(
                    <div key={index} style={{backgroundColor: 'rgba(0,0,0,0.15)', padding: 16, paddingBottom: 1}}>
                        <ProductListItem post={post}
                                         onSelect={this.onSelect.bind(this, post.id)}
                                         onAddToCollection={this.onAddCollection.bind(this)}
                                         index={index}/>
                    </div>
                )
                _posts.push(<div className="clearfix" key={"clearfix2"}/>)
                return _posts
            }

            if (grid) {
                _posts.push(<Product post={post}
                                     onSelect={this.onSelect.bind(this, post.id)}
                                     onAddToCollection={this.onAddCollection.bind(this)}
                                     key={index}
                                     index={index}/>)
                return _posts

            }


            _posts.push(<ProductListItem post={post}
                                         onSelect={this.onSelect.bind(this, post.id)}
                                         onAddToCollection={this.onAddCollection.bind(this)}
                                         key={index}
                                         index={index}/>)
            return _posts
        }, [])

        const _new = oldestNew > -1 ? (
            <div className="card col-xs-12" onClick={this.props.clearNewTimeline}>
                <div className="card-header">
                    <h2>You have {oldestNew + 1} new posts.</h2>
                </div>
            </div>
        ) : null


        const className = grid ? "contacts c-profile clearfix row card card-padding" : "container"

        return (
            <div className={className} style={{marginLeft: 0, marginRight: 0}}>
                {_new}
                {_posts}
                <div className="modal" ref={modal => this.modal = modal}>
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h4 className="modal-title">Add to Collection</h4>
                            </div>
                            <div className="modal-body">
                                {_modal}
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-link waves-effect">Save changes</button>
                                <button type="button" className="btn btn-link waves-effect" data-dismiss="modal">Close
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }

}

Products.propTypes = {
    posts: PropTypes.object.isRequired,
    doComment: PropTypes.func.isRequired,
    doLike: PropTypes.func.isRequired,
    doShare: PropTypes.func.isRequired,
    doSave: PropTypes.func.isRequired,
}

Product.propTypes = {
    post: PropTypes.object.isRequired,
    doComment: PropTypes.func.isRequired,
    doLike: PropTypes.func.isRequired,
    doShare: PropTypes.func.isRequired,
    doSave: PropTypes.func.isRequired,
}

ProductListItem.propTypes = {
    post: PropTypes.object.isRequired,
    doComment: PropTypes.func.isRequired,
    doLike: PropTypes.func.isRequired,
    doShare: PropTypes.func.isRequired,
    doSave: PropTypes.func.isRequired,
}

export {Products, Product, ProductListItem}