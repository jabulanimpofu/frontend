import React, {Component, PropTypes} from 'react'

class Rating extends Component {
    render() {
        const {rating} = this.props
        const _rating = []

        for(let i = 1; i < 6; i++){
            if(rating >= i){
                _rating.push(<i className="zmdi zmdi-star active" key={i}></i>)
                continue
            }
            _rating.push(<i className="zmdi zmdi-star" key={i}></i>)
        }

        return (
            <div className="rating rl-star fullwidth">
                {_rating}
            </div>
        )
    }
}

Rating.propTypes = {
    rating: PropTypes.number.isRequired,
}

export default Rating