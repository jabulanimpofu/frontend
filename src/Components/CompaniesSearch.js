import React, {Component, PropTypes} from 'react'
import Rating from './Rating'
import {Products, ProductsSearchToolbar} from './Product'
import {Companies} from  './Company'


class CompaniesSearch extends Component {
    constructor(props) {
        super(props)
        this.state = {
            grid: true
        }
    }


    toggleGrid(grid) {
        grid = !!grid
        this.setState({grid})
    }

    render() {
        const {products, title} = this.props
        const {grid} = this.state

        return (
            <div>
                <ProductsSearchToolbar grid={grid} toggleView={this.toggleGrid.bind(this)} title={title}/>
                <Companies posts={products} grid={grid}/>
            </div>
        )
    }
}

CompaniesSearch.propTypes = {
    posts: PropTypes.array.isRequired,
    view: PropTypes.string.isRequired,
}

CompaniesSearch.defaultProps = {
    posts: [],
    view: 'list',
    title: "Viewing 100 companies below",
}

export default CompaniesSearch