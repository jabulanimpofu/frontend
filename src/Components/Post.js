import React, {Component, PropTypes} from 'react'
import Rating from './Rating'
import Config from '../../config'
import {Link} from 'react-router-dom'
import {CollectionsList, CreateCollection} from './Collections'
const $ = window.$

const post = {
    "actor": {
        "id": 8,
        "name": "Two Two",
        "account_type": "individual",
        "is_admin": null,
        "is_company": null,
        "email_md5": "74885b15e4895e2e63af23c5d3c85f70",
        "created_at": null,
        "modified_at": null,
        "following": 0
    },
    "foreign_id": {
        "id": 8,
        "body": "post 1 by two",
        "image": "",
        "created_at": "2017-04-26T17:42:28.000Z",
        "modified_at": "2017-04-26T17:42:28.000Z",
        "user_id": 8,
        "user_name": "Two Two",
        "user_account_type": "individual",
        "user_is_admin": null,
        "user_is_company": null,
        "user_email_md5": "74885b15e4895e2e63af23c5d3c85f70",
        "liked": 0
    },
    "id": "b7854900-2aa7-11e7-8080-80002d3ef1ca",
    "object": {
        "id": 8,
        "body": "post 1 by two",
        "image": "",
        "created_at": "2017-04-26T17:42:28.000Z",
        "modified_at": "2017-04-26T17:42:28.000Z",
        "user_id": 8,
        "user_name": "Two Two",
        "user_account_type": "individual",
        "user_is_admin": null,
        "user_is_company": null,
        "user_email_md5": "74885b15e4895e2e63af23c5d3c85f70",
        "liked": 0
    },
    "origin": "user_posts:8",
    "target": null,
    "time": "2017-04-26T17:42:28.496000",
    "to": [],
    "verb": "add"
}

const PostLoading = () => {
    return (
        <div className="card">
            <div className="card-body card-padding timeline-item">
                <div className="animated-background">
                    <div className="background-masker header-top"></div>
                    <div className="background-masker header-left"></div>
                    <div className="background-masker header-right"></div>
                    <div className="background-masker header-bottom"></div>
                    <div className="background-masker subheader-left"></div>
                    <div className="background-masker subheader-right"></div>
                    <div className="background-masker subheader-bottom"></div>
                    <div className="background-masker content-top"></div>
                    <div className="background-masker content-first-end"></div>
                    <div className="background-masker content-second-line"></div>
                    <div className="background-masker content-second-end"></div>
                    <div className="background-masker content-third-line"></div>
                    <div className="background-masker content-third-end"></div>
                </div>
            </div>
        </div>
    )
}

class Post extends Component {
    constructor(props) {
        super(props)
        this.state = {
            showCommentBox: false,
            comment_text: ''
        }
    }

    componentDidMount() {
        /*
         * Lightbox
         */
        if ($('.lightbox')[0]) {
            $('.lightbox').lightGallery({
                enableTouch: true
            });
        }
    }

    renderContentBox() {
        const {comment_text, showCommentBox} = this.state
        if (!showCommentBox)
            return 'Write Something...'

        return (
            <div>
                <div className="wcc-inner">
                    <textarea value={comment_text} onChange={(e) => this.setState({comment_text: e.target.value})}
                              className="wcci-text auto-size" placeholder="Write Something..."></textarea>
                </div>
                <div
                    className="m-t-15">
                    <button
                        className="btn btn-sm btn-primary"
                        onClick={() => this.props.doComment(comment_text)}> Post
                    </button>
                    <button
                        className="btn btn-sm btn-link"
                        onClick={() => this.setState({showCommentBox: false})}> Cancel
                    </button>
                </div>
            </div>
        )
    }

    renderImages(images) {
        if (!Array.isArray(images))
            return null

        const _images = images.map((image, index) => {
            return (
                <div key={index} className="wip-item"
                     data-src={`${Config.imgix.baseUrl}/${image}?auto=enhance&w=200&h=200&fit=crop&fm=png&dpr=2`}
                     style={{backgroundImage: `url(${Config.imgix.baseUrl}/${image}?auto=enhance&w=200&h=200&fit=crop&fm=png&dpr=2)`}}>
                    <img src={`${Config.imgix.baseUrl}/${image}?auto=enhance&w=200&h=200&fit=crop&fm=png&dpr=2`}
                         alt=""/>
                </div>
            )
        })

        return (
            <div className="wall-img-preview lightbox clearfix">
                {_images}
            </div>
        )

    }

    renderComments(comments) {
        if (!Array.isArray(comments))
            return null

        const _comments = [1, 2, 3].map((comment, index) => {
            return (
                <div className="media" key={index}>
                    <a href="" className="pull-left">
                        <img src="/img/profile-pics/8.jpg" alt="" className="avatar-img"/>
                    </a>

                    <div className="media-body">
                        <a href="" className="a-title">Benn Holder</a>
                        <small className="c-gray m-l-10">3rd July 2015</small>
                        <p className="m-t-5 m-b-0">Class aptent taciti sociosqu ad litora torquent per conubia
                            nostra...</p>
                    </div>

                    <ul className="actions">
                        <li className="dropdown" dropdown="">
                            <a href="" dropdown-toggle="" aria-haspopup="true" aria-expanded="false">
                                <i className="zmdi zmdi-more-vert"></i>
                            </a>

                            <ul className="dropdown-menu dropdown-menu-right">
                                <li>
                                    <a href="">Report</a>
                                </li>
                                <li>
                                    <a href="">Delete</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            )
        })

        return (
            <div className="wcl-list">
                {_comments}
            </div>
        )
    }

    renderOptions(post, user, sendMessage, onAddToCollection) {
        return (
            <ul className="wall-attrs clearfix list-inline list-unstyled">
                <li className="wa-stats">
                    <span className="active"><i className="zmdi zmdi-favorite"></i> 2432</span>

                    <span onClick={(e) => {
                        e.preventDefault()
                        onAddToCollection(post.id)
                    }}><i className="zmdi zmdi-download"></i> Save</span>
                    <span className="dropdown pmop-message">
                        <span data-toggle="dropdown"><i className="zmdi zmdi-mail-send"></i> Message</span>
                        <div className="dropdown-menu">
                            <textarea
                                placeholder={`Write message to ${user.name} about product ${post.id}...`}></textarea>

                            <button className="btn bgm-green btn-float"><i
                                className="zmdi zmdi-mail-send"></i></button>
                        </div>
                    </span>
                </li>
            </ul>
        )
    }

    renderTitle(post) {
        if (post.title)
            return (
                <h4>Apple Macbook
                    <small>Electronics</small>
                </h4>
            )
        return null
    }

    renderBadge(post) {
        const {
            name,
            body,
            price,
            rate,
            image,
            category,
            sub_category,
        } = post

        if (rate)
            return (<div className="post-type"><i className="zmdi zmdi-case"></i></div>)

        if (price)
            return (<div className="post-type"><i className="zmdi zmdi-shopping-cart"></i></div>)

        return (<div className="post-type"><i className="zmdi zmdi-comments"></i></div>)
    }

    renderPrice(post) {
        const {
            name,
            body,
            price,
            rate,
            image,
            category,
            sub_category,
        } = post

        if (rate)
            return (<small><b>{price}/{rate}</b></small>)

        if (price)
            return (<small><b>{price}/{rate}</b></small>)

        return null
    }

    renderProfilePic(user) {
        const {avatar} = user
        if (avatar)
            return (
                <div className="avatar-fullwidth"
                     style={{backgroundImage: `url(${avatar})`, backgroundColor: 'white'}}>
                </div>
            )

        return (
            <div className="avatar-fullwidth"
                 style={{backgroundImage: 'url(/img/account-circle.png)', backgroundColor: 'white'}}>
            </div>
        )
    }

    render() {
        const {showCommentBox} = this.state
        const {post: post_obj, onAddToCollection} = this.props
        const {object: post, actor: user, comments} = post_obj

        const _images = this.renderImages(post.image)

        const _comments = this.renderComments(comments)

        const _options = this.renderOptions(post, user, () => {}, onAddToCollection)

        const _title = this.renderTitle(post)

        const _price = this.renderPrice(post)

        const _badge = this.renderBadge(post)

        const _profile_pic = this.renderProfilePic(user)

        return (
            <div className="card" style={{display: post.isNew ? 'none' : 'block'}}>
                <div className="row card-body">
                    <div className="col-md-2 col-sm-2 hidden-xs card-left">
                        {_profile_pic}
                        <Rating rating={4}/>
                        <div className="side-badge">
                            <img src="/img/badge.png" style={{width: '100%'}} alt=""/>
                        </div>
                        <div className="side-followers">
                            <h4>{user.followers}</h4>
                            <h5>Followers</h5>
                        </div>
                    </div>
                    <div className="col-md-10 col-sm-10 card-right">

                        <div className="card-header card-padding-sm">
                            <h2><Link to={`/user/${user.id}`}>{user.name}</Link>
                                <small className="pull-right">Posted on {post.created_at}</small>
                                <small>Graphic Designer</small>
                                <small>Guzha Advertising</small>
                                <small><b>From</b> {user.location}</small>
                                {_price}
                            </h2>
                        </div>

                        <div className="card-body card-padding-sm" style={{position: 'relative'}}>
                            {_title}

                            <p>{post.body}</p>

                            {_images}

                            {_options}

                            <div style={{position: 'absolute', right: 0, bottom: 15}}>{_badge}</div>
                        </div>

                    </div>
                </div>

                <div className="wall-comment-list">
                    {_comments}
                    <div className="wcl-form">
                        <div className="wc-comment">
                            <div className="wcc-inner wcc-toggle" onClick={() => this.setState({showCommentBox: true})}>
                                {this.renderContentBox.bind(this)()}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class Posts extends Component {
    constructor(props) {
        super(props)
        this.state = {
            modal: 'add_to_collection'
        }
    }

    onAddToCollection() {
        this.setState({modal: 'add_to_collection'})
        $(this.modal).modal('show')
    }

    onAddCollection() {
        this.setState({modal: 'add_collection'})
        $(this.modal).modal('show')
    }

    renderModal() {
        const {modal} = this.state
        switch (modal) {
            case 'add_to_collection':
                return (<CollectionsList collections={[0, 2, 3, 4, 5, 6, 7]}
                                         onAddCollection={this.onAddCollection.bind(this)}
                                         onSelectCollection={alert}/>)

            case 'add_collection':
                return (<CreateCollection />)
            default:
                return null
        }


    }

    render() {
        const {posts, doComment, doLike, doShare, doSave} = this.props

        if (posts.isFetching)
            return (
                <div>
                    <PostLoading/>
                    <PostLoading/>
                </div>
            )

        const _modal = this.renderModal.bind(this)()

        let oldestNew = -1
        let _posts = !Array.isArray(posts.data) ? [] : posts.data.map((post, index) => {
            if (post.isNew) {
                oldestNew = index
            }
            return (<Post post={post}
                          key={index}
                          onAddToCollection={this.onAddToCollection.bind(this)}
                          doComment={doComment.bind(undefined, post.object.id)}
                          doSave={doSave.bind(undefined, post.object.id)}
                          doShare={doShare.bind(undefined, post.object.id)}
                          doLike={doLike.bind(undefined, post.object.id)}/>)
        })

        const _new = oldestNew > -1 ? (
            <div className="card" onClick={this.props.clearNewTimeline}>
                <div className="card-header">
                    <h2>You have {oldestNew + 1} new posts.</h2>
                </div>
            </div>
        ) : null

        return (
            <div>
                {_new}
                {_posts}
                <div className="modal" ref={modal => this.modal = modal}>
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h4 className="modal-title">Add to Collection</h4>
                            </div>
                            <div className="modal-body">
                                {_modal}
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-link waves-effect">Save changes</button>
                                <button type="button" className="btn btn-link waves-effect" data-dismiss="modal">Close
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

Posts.propTypes = {
    posts: PropTypes.object.isRequired,
    doComment: PropTypes.func.isRequired,
    doLike: PropTypes.func.isRequired,
    doShare: PropTypes.func.isRequired,
    doSave: PropTypes.func.isRequired,
}

Post.propTypes = {
    post: PropTypes.object.isRequired,
    doComment: PropTypes.func.isRequired,
    doLike: PropTypes.func.isRequired,
    doShare: PropTypes.func.isRequired,
    doSave: PropTypes.func.isRequired,
}

export {Posts, Post}