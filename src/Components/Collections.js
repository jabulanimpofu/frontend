import React, {Component, PropTypes} from 'react'
import {Products} from './Product'
import ProductsSearch from './ProductsSearch'
import {TextAreaField, TextArea, TextField, SelectField} from './Form/'

const products = {
    data: [1, 2, 3, 4, 5, 6,].map((review, index) => ({
        object: {},
        id: index
    }))
}

const Collection = () => {
    return (
        <div className="col-md-3 col-sm-3 col-xs-3">
            <div className="c-item">
                <a href="" className="ci-avatar">
                    <img src="/img/contacts/1.jpg" alt=""/>
                </a>

                <div className="c-info">
                    <strong>Collection name</strong>
                    <small>short descrition</small>
                </div>
            </div>
        </div>
    )
}

export const CollectionsList = ({collections, onAddCollection, onSelectCollection}) => {
    const _collections = collections.map((review, index) => {
        return (
            <div onClick={(e) => {
                e.preventDefault()
                console.log(review.id)
                onSelectCollection(review.id)
            }} className="col-md-3 col-sm-3 col-xs-3" key={index}>
                <div className="c-item">
                    <a href="" className="ci-avatar">
                        <img src="/img/contacts/1.jpg" alt=""/>
                    </a>

                    <div className="c-info">
                        <strong>Collection name</strong>
                        <small>short descrition</small>
                    </div>
                </div>
            </div>
        )
    })
    _collections.push(
        <div onClick={(e) => {
            e.preventDefault()
            onAddCollection()
        }} className="col-md-3 col-sm-3 col-xs-3" key={_collections.length}>
            <div className="c-item" style={{fontSize: 134, textAlign: 'center'}}>
                <i className="zmdi zmdi-plus-circle-o active"></i>
            </div>
        </div>)
    return (
        <div className="contacts c-profile clearfix row">
            {_collections}
        </div>
    )
}

export class CreateCollection extends Component {
    constructor(props) {
        super(props)
        this.state = {
            title: '',
            description: '',
        }
    }

    onFieldChange(field_name, value) {
        this.setState({[field_name]: value})
    }

    getData() {
        return this.state
    }

    render() {
        const {title, description} = this.state
        return (
            <div className="row">

                <TextField value={title} name="title" label="Collection name" size="col-xs-12"
                           onChange={this.onFieldChange.bind(this)}/>

                <TextAreaField value={description} name="description" label="Collection Description"
                               size="col-xs-12" onChange={this.onFieldChange.bind(this)}/>

            </div>
        )
    }

}

class Collections extends Component {
    constructor(props) {
        super(props)
        this.state = {
            collection: null
        }
        this.renderCollection = this.renderCollection.bind(this)
    }

    renderCollection(collection) {
        const products = {
            data: [1, 2, 3, 4, 5, 6,].map((review, index) => ({
                object: {},
                id: index
            }))
        }

        return (
            <div className="card-body card-padding">
                <ProductsSearch products={products} title={(
                    <a href="#" onClick={(e) => {
                        e.preventDefault()
                        this.setState({collection: null})
                    }}>Back to collections</a>
                )}/>
            </div>
        )
    }

    onSelectCollection(collection) {
        this.setState({collection: 9})
    }

    render() {
        const {collection} = this.state
        const products = {
            data: [1, 2, 3, 4, 5, 6,].map((review, index) => ({
                object: {},
                id: index
            }))
        }

        const collections = [1, 2, 3, 4, 5, 6,].map((collection, index) => {
            return {
                object: {},
                id: index
            }
        })


        const _collection_content = collection ? this.renderCollection(collection) : (
            <div className="card-body card-padding">
                <CollectionsList collections={collections}
                                 onSelectCollection={this.onSelectCollection.bind(this)}
                                 onAddCollection={alert}/>
            </div>
        )

        return (
            <div className="card">
                <ul className="tab-nav tn-justified" role="tablist">
                    <li role="presentation" className="active">
                        <a className="col-sx-4" href="#tab-1" aria-controls="tab-1" role="tab"
                           data-toggle="tab">
                            Collections
                        </a>
                    </li>
                    <li role="presentation">
                        <a className="col-xs-4" href="#tab-2" aria-controls="tab-2" role="tab"
                           data-toggle="tab">
                            Products
                        </a>
                    </li>
                    <li role="presentation">
                        <a className="col-xs-4" href="#tab-3" aria-controls="tab-3" role="tab"
                           data-toggle="tab">
                            Services
                        </a>
                    </li>
                </ul>
                <div className="tab-content p-0">
                    <div role="tabpanel" className="wall-posting tab-pane animated fadeIn in active" id="tab-1">
                        <div className="card-body card-padding">
                            {_collection_content}
                        </div>

                    </div>
                    <div role="tabpanel" className="wall-posting tab-pane animated fadeIn" id="tab-2">
                        <div className="card-body card-padding">
                            <ProductsSearch products={products} title="viewing 100 products"/>
                        </div>
                    </div>
                    <div role="tabpanel" className="wall-posting tab-pane animated fadeIn" id="tab-3">
                        <div className="card-body card-padding">
                            <ProductsSearch products={products} title="viewing 100 services"/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

Collections.propTypes = {}

export default Collections