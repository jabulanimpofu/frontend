import React, {Component, PropTypes} from 'react'
import {Link} from 'react-router-dom'

class CopyRight extends Component {
    render() {
        return (
            <div>
                <div className="card-body card-padding">
                    <span className="block" style={{textAlign: 'center'}}>
                        <Link to="/privacy_policy">Copyright PlusAfrik 2016</Link>
                    </span>
                    <span className="block">
                        <Link to="/privacy_policy">About</Link> | <Link to="/terms">Terms</Link> | <Link to="/privacy_policy">Privacy</Link>
                    </span>
                    <span className="block">
                        <Link to="/product_listing_policy">Product Listing</Link> | <Link to="/cookie_policy">Cookie Policy</Link>
                    </span>
                </div>
            </div>
        )
    }
}

CopyRight.propTypes = {}

export default CopyRight