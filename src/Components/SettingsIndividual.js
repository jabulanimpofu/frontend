import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import {} from '../Actions/Api'
import {TextAreaField, TextArea, TextField, SelectField, Datepicker} from './Form/'


const award = {
    organisation: '',
    award: '',
    year: '',
}

class SettingsAdmin extends Component {
    constructor(props) {
        super(props)
        this.state = {
            gender: '',
            title: '',
            name: '',
            category: '',
            date: '',
            country: '',
            street_address: '',
            city: '',
            province: '',
            email: '',
            position: '',
        }
    }

    componentDidMount() {
    }

    componentDidUpdate(prevProps) {

    }

    onFieldChange(field, value) {
        this.setState({[field]: value})
    }

    render() {
        const {view, profile_id, timeline, doComment, doSave, doShare, doLike, doFollow, doConnect} = this.props
        const {
            gender,
            title,
            name,
            category,
            date,
            country,
            street_address,
            city,
            province,
            email,
            position
        } = this.state

        return (
            <div className="card" id="profile-main">
                <div className="pm-overview-show-mobile">
                    <div className="pmo-pic">
                        <div className="p-relative">
                            <a href="">
                                <img className="img-responsive" src="/img/profile-pics/profile-pic-2.jpg"
                                     alt=""/>
                            </a>

                            <br/>
                            <a href="" className="pmop-edit" style={{position: 'static'}}>
                                <i className="zmdi zmdi-camera"></i> <span
                                className="hidden-xs">Update Profile Picture</span>
                            </a>
                        </div>

                    </div>
                </div>
                <div className="row card-body card-padding">


                    <TextField value={name} name="name" label="Name" size="col-xs-6"
                               onChange={this.onFieldChange.bind(this)}/>

                    <SelectField value={title} options={[]} name="title"
                                 label="Title" size="col-xs-3"
                                 onChange={this.onFieldChange.bind(this)}/>

                    <SelectField value={gender} options={[]} name="gender"
                                 label="Gender" size="col-xs-3"
                                 onChange={this.onFieldChange.bind(this)}/>


                    <TextField value={position} name="position" label="Position in company/Job Title" size="col-xs-6"
                               onChange={this.onFieldChange.bind(this)}/>


                    <Datepicker value={date} name="date" label="Date of Birth" size="col-xs-6"
                               onChange={this.onFieldChange.bind(this)}/>



                    <SelectField value={category} options={[]} name="category"
                                 label="Pick Category" size="col-xs-3"
                                 onChange={this.onFieldChange.bind(this)}/>

                    <div className="clearfix"/>

                    <TextField value={street_address} name="street_address" label="Street Address" size="col-xs-6"
                               onChange={this.onFieldChange.bind(this)}/>

                    <TextField value={city} name="city" label="City" size="col-xs-6"
                               onChange={this.onFieldChange.bind(this)}/>

                    <SelectField value={province} options={[]} name="province"
                                 label="Province" size="col-xs-6"
                                 onChange={this.onFieldChange.bind(this)}/>

                    <SelectField value={country} options={[]} name="country"
                                 label="Country" size="col-xs-6"
                                 onChange={this.onFieldChange.bind(this)}/>


                    <TextField value={email} name="email" label="Email" size="col-xs-6"
                               onChange={this.onFieldChange.bind(this)}/>

                </div>
            </div>
        )
    }
}

SettingsAdmin.propTypes = {}

const mapStateToProps = (state, ownProps) => {
    const {profile} = state
    const user_id = profile.data.id

    return {
        user_id,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingsAdmin)