import React, {Component} from 'react'

class Update extends Component {
    render() {
        return (
            <div className="card">
                <div className="card__header">
                    <h2>Praesent vitae justo purus <small>In hendrerit lorem nislac lacinia</small></h2>
                </div>

                <ul className="tab-nav tab-nav--justified tab-nav--icon">
                    <li className="active">
                        <a className="col-sx-4" href="#tab-1" data-toggle="tab">
                            <i className="zmdi zmdi-home icon-tab"></i>
                        </a>
                    </li>
                    <li>
                        <a className="col-xs-4" href="#tab-2" data-toggle="tab">
                            <i className="zmdi zmdi-pin icon-tab"></i>
                        </a>
                    </li>
                    <li>
                        <a className="col-xs-4" href="#tab-3" data-toggle="tab">
                            <i className="zmdi zmdi-star icon-tab"></i>
                        </a>
                    </li>
                </ul>

                <div className="card__body">
                    <div className="tab-content">
                        <div className="tab-pane fade in active" id="tab-1">
                            <div class="form-group">
                                <textarea class="form-control" rows="5" placeholder="Let us type some lorem ipsum...."></textarea>
                                <i class="form-group__bar"></i>
                            </div>
                        </div>

                        <div className="tab-pane fade" id="tab-2">
                            <p><img src="demo/img/headers/sm/2.png" className="img-responsive" alt=""/></p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sit amet dapibus tellus. Nullam aliquet dignissim semper. Cras sit amet ligula congue, dapibus enim id, dapibus tellus.
                        </div>

                        <div className="tab-pane fade" id="tab-3">
                            <p><img src="demo/img/headers/sm/4.png" className="img-responsive" alt=""/></p>
                            Serhoncus quis est sit amete in nisl molestie fringilla. Nunc vitae ante id magna feugiat condimentum. Maecenas sit amet urna massa.
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

Update.propTypes = {}

export default Update