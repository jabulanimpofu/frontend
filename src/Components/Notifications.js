import React, {Component} from 'react'

class Notifications extends Component {
    render() {
        return (
            <aside id="notifications">
                <ul className="tab-nav tab-nav--justified tab-nav--icon">
                    <li><a className="user-alert__messages" href="#notifications__messages" data-toggle="tab"><i className="zmdi zmdi-email"></i></a></li>
                    <li><a className="user-alert__notifications" href="#notifications__updates" data-toggle="tab"><i className="zmdi zmdi-notifications"></i></a></li>
                    <li><a className="user-alert__tasks" href="#notifications__tasks" data-toggle="tab"><i className="zmdi zmdi-playlist-plus"></i></a></li>
                </ul>

                <div className="tab-content">
                    <div className="tab-pane" id="notifications__messages">
                        <div className="list-group">
                            <a href="" className="list-group-item media">
                                <div className="pull-left">
                                    <img className="avatar-img" src="demo/img/profile-pics/1.jpg" alt=""/>
                                </div>

                                <div className="media-body">
                                    <div className="list-group__heading">David Villa Jacobs</div>
                                    <small className="list-group__text">Sorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mattis lobortis sapien non posuere</small>
                                </div>
                            </a>

                            <a href="" className="list-group-item media">
                                <div className="pull-left">
                                    <img className="avatar-img" src="demo/img/profile-pics/5.jpg" alt=""/>
                                </div>
                                <div className="media-body">
                                    <div className="list-group__heading">Candice Barnes</div>
                                    <small className="list-group__text">Quisque non tortor ultricies, posuere elit id, lacinia purus curabitur.</small>
                                </div>
                            </a>

                            <a href="" className="list-group-item media">
                                <div className="pull-left">
                                    <img className="avatar-img" src="demo/img/profile-pics/3.jpg" alt=""/>
                                </div>
                                <div className="media-body">
                                    <div className="list-group__heading">Jeannette Lawson</div>
                                    <small className="list-group__text">Donec congue tempus ligula, varius hendrerit mi hendrerit sit amet. Duis ac quam sit amet leo feugiat iaculis</small>
                                </div>
                            </a>

                            <a href="" className="list-group-item media">
                                <div className="pull-left">
                                    <img className="avatar-img" src="demo/img/profile-pics/4.jpg" alt=""/>
                                </div>
                                <div className="media-body">
                                    <div className="list-group__heading">Darla Mckinney</div>
                                    <small className="list-group__text">Duis tincidunt augue nec sem dignissim scelerisque. Vestibulum rhoncus sapien sed nulla aliquam lacinia</small>
                                </div>
                            </a>

                            <a href="" className="list-group-item media">
                                <div className="pull-left">
                                    <img className="avatar-img" src="demo/img/profile-pics/2.jpg" alt=""/>
                                </div>
                                <div className="media-body">
                                    <div className="list-group__heading">Rudolph Perez</div>
                                    <small className="list-group__text">Phasellus a ullamcorper lectus, sit amet viverra quam. In luctus tortor vel nulla pharetra bibendum</small>
                                </div>
                            </a>
                        </div>

                        <a href="" className="btn btn--float">
                            <i className="zmdi zmdi-plus"></i>
                        </a>
                    </div>

                    <div className="tab-pane" id="notifications__updates">
                        <div className="list-group">
                            <a href="" className="list-group-item media">
                                <div className="pull-right">
                                    <img className="avatar-img" src="demo/img/profile-pics/1.jpg" alt=""/>
                                </div>

                                <div className="media-body">
                                    <div className="list-group__heading">David Villa Jacobs</div>
                                    <small className="list-group__text">Sorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mattis lobortis sapien non posuere</small>
                                </div>
                            </a>

                            <a href="" className="list-group-item">
                                <div className="list-group__heading">Candice Barnes</div>
                                <small className="list-group__text">Quisque non tortor ultricies, posuere elit id, lacinia purus curabitur.</small>
                            </a>

                            <a href="" className="list-group-item">
                                <div className="list-group__heading">Jeannette Lawson</div>
                                <small className="list-group__text">Donec congue tempus ligula, varius hendrerit mi hendrerit sit amet. Duis ac quam sit amet leo feugiat iaculis</small>
                            </a>

                            <a href="" className="list-group-item media">
                                <div className="pull-right">
                                    <img className="avatar-img" src="demo/img/profile-pics/4.jpg" alt=""/>
                                </div>
                                <div className="media-body">
                                    <div className="list-group__heading">Darla Mckinney</div>
                                    <small className="list-group__text">Duis tincidunt augue nec sem dignissim scelerisque. Vestibulum rhoncus sapien sed nulla aliquam lacinia</small>
                                </div>
                            </a>

                            <a href="" className="list-group-item media">
                                <div className="pull-right">
                                    <img className="avatar-img" src="demo/img/profile-pics/2.jpg" alt=""/>
                                </div>
                                <div className="media-body">
                                    <div className="list-group__heading">Rudolph Perez</div>
                                    <small className="list-group__text">Phasellus a ullamcorper lectus, sit amet viverra quam. In luctus tortor vel nulla pharetra bibendum</small>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div className="tab-pane" id="notifications__tasks">
                        <div className="list-group">
                            <div className="list-group-item">
                                <div className="list-group__heading m-b-5">HTML5 Validation Report</div>

                                <div className="progress">
                                    <div className="progress-bar" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style={{width: 95+'%'}}>
                                        <span className="sr-only">95% Complete (success)</span>
                                    </div>
                                </div>
                            </div>
                            <div className="list-group-item">
                                <div className="list-group__heading m-b-5">Google Chrome Extension</div>

                                <div className="progress">
                                    <div className="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style={{width: 80+'%'}}>
                                        <span className="sr-only">80% Complete (success)</span>
                                    </div>
                                </div>
                            </div>
                            <div className="list-group-item">
                                <div className="list-group__heading m-b-5">Social Intranet Projects</div>

                                <div className="progress">
                                    <div className="progress-bar" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style={{width: 20+'%'}}>
                                        <span className="sr-only">20% Complete</span>
                                    </div>
                                </div>
                            </div>
                            <div className="list-group-item">
                                <div className="list-group__heading m-b-5">Bootstrap Admin Template</div>

                                <div className="progress">
                                    <div className="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style={{width: 60+'%'}}>
                                        <span className="sr-only">60% Complete (warning)</span>
                                    </div>
                                </div>
                            </div>
                            <div className="list-group-item">
                                <div className="list-group__heading m-b-5">Youtube Client App</div>

                                <div className="progress">
                                    <div className="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style={{width: 80 + '%'}}>
                                        <span className="sr-only">80% Complete (danger)</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a href="" className="btn btn--float">
                            <i className="zmdi zmdi-plus"></i>
                        </a>
                    </div>
                </div>
            </aside>
        )
    }
}

Notifications.propTypes = {}

export default Notifications