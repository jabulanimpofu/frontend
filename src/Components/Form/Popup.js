import React, {Component, PropTypes} from 'react'

const PopupMessage = ({placeholder, onSend}) => {
    return (
        <div className="">
            <textarea placeholder={placeholder}></textarea>
            <button className="btn bgm-green btn-float" onClick={(e) => {
                e.preventDefault()
                onSend()
            }}><i className="zmdi zmdi-mail-send"></i></button>
        </div>
    )
}

class Popup extends Component {
    render() {
        const {icon, title, children} = this.props
        return (
            <span className="dropdown pmop-message">
                        <span data-toggle="dropdown"><i className={`zmdi ${icon}`}></i> {title}</span>
                        <div className="dropdown-menu">
                            {children}
                        </div>
                    </span>
        )
    }
}

Popup.propTypes = {}

export default Popup