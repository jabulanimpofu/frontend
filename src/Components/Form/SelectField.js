import React, {Component, PropTypes} from 'react'
import './SelectField.css';
import Select from 'react-select';

class SelectField extends Component {
    componentDidMount() {

    }

    render() {
        const {value, name, label, size, options, onChange} = this.props

        return (
            <div className={size}>
                <div className="form-group">
                    <label>{label}</label>
                    <Select
                        name={name}
                        value={value}
                        options={options}
                        onChange={(val) => {
                            onChange(name, val.value)
                        }}
                    />
                </div>
            </div>
        )


        return (
            <div className={size}>
                <p className="f-500 c-black m-b-15">{label}</p>
                <select className="chosen" ref={select => this.select = select} name={name} value={value}
                        onChange={onChange.bind(undefined, name)} data-placeholder="Choose a Country...">
                    <option></option>

                </select>
            </div>
        )
    }

}

SelectField.defaultProps = {
    size: 'col-xs-12'
}

export default SelectField