import React, {Component, PropTypes} from 'react'
const $ = window.$
const autosize = window.autosize

class TextAreaField extends Component {
    componentDidMount(){
        /*
         * Auto Hight Textarea
         */
        if ($('.auto-size')[0]) {
            autosize($('.auto-size'));
        }
    }

    render(){
        const {value, name, label, size, onChange} = this.props
        return (
            <div className={size}>
                <div className="form-group">
                    <label>{label}</label>
                    <textarea className="form-control auto-size" rows="3"
                              name={name}
                              value={value}
                              onChange={(event) => onChange(name, event.target.value)}></textarea>
                    <i className="form-group__bar"></i>
                </div>
            </div>
        )
    }
}

TextAreaField.defaultProps = {
    size: 'col-xs-12'
}

export default TextAreaField