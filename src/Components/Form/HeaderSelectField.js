import React, {Component, PropTypes} from 'react'
import './HeadSelectField.css';
import Select from 'react-select';


class HeaderSelectField extends Component {
    componentDidMount() {

    }

    render() {
        const {value, name, label, size, options, onChange} = this.props
        return (
            <div>
                <Select
                    placeholder={label}
                    name={name}
                    value={value}
                    options={options}
                    onChange={(val) => {
                        onChange(name, val.value)
                    }}
                />
            </div>
        )
    }

}

HeaderSelectField.defaultProps = {

}

export default HeaderSelectField