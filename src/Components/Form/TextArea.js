import React, {Component, PropTypes} from 'react'

const TextArea = (props) => {
    const {value, name, label, onChange} = props
    return (
        <textarea className="wp-text"
                  data-auto-size
                  placeholder={label}
                  name={name}
                  value={value}
                  onChange={(event) => onChange(name, event.target.value)}/>
    )
}

TextArea.defaultProps = {
    size: 'col-xs-12',
    label: "Write Something...here",
}

export default  TextArea