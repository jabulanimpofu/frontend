import React, {Component, PropTypes} from 'react'
import { DateField, Calendar } from 'react-date-picker'
import 'react-date-picker/index.css'
import './datepicker.css'

const Datepicker = (props) => {
    const {value, name, label, size, onChange} = props
    return (
        <div className={size}>
            <div className="form-group">
                <label>{label}</label>
                <div>
                <DateField
                    dateFormat="YYYY-MM-DD"
                    name={name}
                    date={value}
                    onChange={(value) => onChange(name, value)}
                />
                </div>
                <i className="form-group__bar"></i>
            </div>
        </div>
    )
}

Datepicker.defaultProps = {
    size: 'col-xs-12'
}

export default Datepicker