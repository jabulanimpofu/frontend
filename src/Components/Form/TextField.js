import React, {Component, PropTypes} from 'react'

const TextField = (props) => {
    const {value, name, label, size, onChange} = props
    return (
        <div className={size}>
            <div className="form-group">
                <label>{label}</label>
                <input placeholder={label}
                       type="text"
                       className="form-control"
                       name={name}
                       value={value}
                       onChange={(event) => onChange(name, event.target.value)}/>
                <i className="form-group__bar"></i>
            </div>
        </div>
    )
}

TextField.defaultProps = {
    size: 'col-xs-12'
}

export default TextField