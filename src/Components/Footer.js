import React, {Component} from 'react'

class Footer extends Component {
    render() {
        return (
            <footer id="footer">
                Copyright &copy; 2017 PlusAfrik

                <ul className="footer__menu">
                    <li><a href="">About PlusAfrik</a></li>
                    <li>|</li>
                    <li><a href="">Terms and Conditions</a></li>
                    <li>|</li>
                    <li><a href="">Privacy Policy</a></li>
                </ul>
            </footer>
        )
    }
}

Footer.propTypes = {}

export default Footer