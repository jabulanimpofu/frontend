import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import {setFilter} from '../Actions/App'
import {getCategories} from '../Actions/Api'
import {Link} from 'react-router-dom'
import {Scrollbars} from 'react-custom-scrollbars'

class MainMenu extends Component {
    componentDidMount() {
        this.props.doGetCategories()
    }

    componentDidUpdate(prevProps) {

    }

    onSelect(category) {
        const {filter, doSetFilter} = this.props
        doSetFilter({...filter, category})
    }

    render() {
        const {categories, filter: {category: active}} = this.props
        const _categories = !Array.isArray(categories.data) ? [] : categories.data.map((category, index) => {
            return (
                <li key={index} className={category.id === active ? "active" : ""}>
                    <a href="" onClick={(e) => {
                        e.preventDefault()
                        this.onSelect.bind(this)(category.id)
                    }}>{category.name}</a>
                </li>
            )
        })
        return (
            <aside id="s-main-menu" className="sidebar">
                <div className="smm-header">
                    <i className="zmdi zmdi-long-arrow-left" data-ma-action="sidebar-close"></i>
                    Categories
                </div>

                <div className="smm-header-large">
                    Categories
                </div>

                <Scrollbars style={{width: '100%', height: '100%'}}>
                    <ul className="main-menu">{_categories}</ul>
                </Scrollbars>
            </aside>
        )
    }
}

MainMenu.propTypes = {
    categories: PropTypes.object.isRequired,
    filter: PropTypes.object.isRequired,
    doGetCategories: PropTypes.func.isRequired,
    doSetFilter: PropTypes.func.isRequired,
}

MainMenu.defaultProps = {
    categories: [],
}

const mapStateToProps = (state, ownProps) => {
    const {collections: {categories}, filter} = state
    return {
        categories, filter
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        doGetCategories(){
            return dispatch(getCategories())
        },
        doSetFilter(filter){
            return dispatch(setFilter(filter))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainMenu)