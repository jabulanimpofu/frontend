import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import {} from '../Actions/Api'
import {TextAreaField, TextArea, TextField, SelectField, Datepicker} from './Form/'

/*const d = (
 <div>
 <TextField value={phone_number} name="phone_number" label="Phone number" size="col-xs-6"
 onChange={this.onFieldChange.bind(this)}/>

 <TextField value={city} name="city" label="City" size="col-xs-6"
 onChange={this.onFieldChange.bind(this)}/>



 <TextField value={password} name="password" label="Password" size="col-xs-6"
 onChange={this.onFieldChange.bind(this)}/>

 <TextField value={cpassword} name="cpassword" label="Confirm Password" size="col-xs-6"
 onChange={this.onFieldChange.bind(this)}/>

 <TextField value={country} name="country" label="Country" size="col-xs-6"
 onChange={this.onFieldChange.bind(this)}/>
 </div>
 )*/

const award = {
    organisation: '',
    award: '',
    year: '',
}

class SettingsIndividual extends Component {
    constructor(props) {
        super(props)
        this.state = {
            trading_name: '',
            profession: '',
            gender: '',
            title: '',
            description: '',

            name: '',
            category: '',
            date: '',
            address: '',
            website: '',
            email: '',
            twitter: '',
            facebook: '',
            linkedIn: '',
            skype: '',
            organisation: '',
            capacity: '',
            awards: [],
            user_name: '',
            password: '',
            cpassword: '',
            country: '',
            phone_number: '',
        }
    }

    componentDidMount() {
    }

    componentDidUpdate(prevProps) {

    }

    onFieldChange(field, value) {
        this.setState({[field]: value})
    }

    render() {
        const {view, profile_id, timeline, doComment, doSave, doShare, doLike, doFollow, doConnect} = this.props
        const {
            trading_name,
            profession,
            gender,
            title,
            description,
            name,
            category,
            date,
            user_name,
            password,
            cpassword,
            country,
            phone_number,
            street_address,
            city,
            province,
            website,
            email,
            twitter,
            facebook,
            linkedin,
            skype
        } = this.state



        /*return (
            <div className="card" id="profile-main">
                <div className="pm-overview-show-mobile">
                    <div className="pmo-pic">
                        <div className="p-relative">
                            <a href="">
                                <img className="img-responsive" src="/img/profile-pics/profile-pic-2.jpg"
                                     alt=""/>
                            </a>

                            <br/>
                            <a href="" className="pmop-edit" style={{position: 'static'}}>
                                <i className="zmdi zmdi-camera"></i> <span
                                className="hidden-xs">Update Profile Picture</span>
                            </a>
                        </div>

                    </div>
                </div>
                <div className="row card-body card-padding">
                    <Datepicker value={date} name="date" label="Date of Birth" size="col-xs-12"
                                onChange={this.onFieldChange.bind(this)}/>
                </div>
            </div>
        )*/



        return (
            <div className="card" id="profile-main">
                <div className="pm-overview-show-mobile">
                    <div className="pmo-pic">
                        <div className="p-relative">
                            <a href="">
                                <img className="img-responsive" src="/img/profile-pics/profile-pic-2.jpg"
                                     alt=""/>
                            </a>

                            <br/>
                            <a href="" className="pmop-edit" style={{position: 'static'}}>
                                <i className="zmdi zmdi-camera"></i> <span
                                className="hidden-xs">Update Profile Picture</span>
                            </a>
                        </div>

                    </div>
                </div>
                <div className="row card-body card-padding">


                    <TextField value={name} name="name" label="Name" size="col-xs-3"
                               onChange={this.onFieldChange.bind(this)}/>

                    <TextField value={user_name} name="user_name" label="Username" size="col-xs-3"
                               onChange={this.onFieldChange.bind(this)}/>

                    <SelectField value={title} options={[]} name="title"
                                 label="Title" size="col-xs-3"
                                 onChange={this.onFieldChange.bind(this)}/>

                    <SelectField value={gender} options={[]} name="gender"
                                 label="Gender" size="col-xs-3"
                                 onChange={this.onFieldChange.bind(this)}/>


                    <TextField value={trading_name} name="trading_name" label="Your trading name" size="col-xs-6"
                               onChange={this.onFieldChange.bind(this)}/>

                    <TextField value={profession} name="profession" label="Your trade/profession" size="col-xs-3"
                               onChange={this.onFieldChange.bind(this)}/>


                    <Datepicker value={date} name="date" label="Date of Birth" size="col-xs-3"
                               onChange={this.onFieldChange.bind(this)}/>


                    <TextAreaField value={description} name="description" label="Sell yourself in a few words"
                                   size="col-xs-12" onChange={this.onFieldChange.bind(this)}/>

                    <SelectField value={category} options={[]} name="category"
                                 label="Pick Category" size="col-xs-3"
                                 onChange={this.onFieldChange.bind(this)}/>

                    <div className="clearfix"/>



                    <TextField value={street_address} name="street_address" label="Street Address" size="col-xs-6"
                               onChange={this.onFieldChange.bind(this)}/>

                    <TextField value={city} name="city" label="City" size="col-xs-6"
                               onChange={this.onFieldChange.bind(this)}/>

                    <SelectField value={province} options={[]} name="province"
                                 label="Province" size="col-xs-6"
                                 onChange={this.onFieldChange.bind(this)}/>

                    <SelectField value={country} options={[]} name="country"
                                 label="Country" size="col-xs-6"
                                 onChange={this.onFieldChange.bind(this)}/>



                    <TextField value={website} name="website" label="Website" size="col-xs-6"
                               onChange={this.onFieldChange.bind(this)}/>

                    <TextField value={email} name="email" label="Email" size="col-xs-6"
                               onChange={this.onFieldChange.bind(this)}/>

                    <TextField value={twitter} name="twitter" label="Twitter" size="col-xs-6"
                               onChange={this.onFieldChange.bind(this)}/>

                    <TextField value={facebook} name="facebook" label="Facebook" size="col-xs-6"
                               onChange={this.onFieldChange.bind(this)}/>

                    <TextField value={linkedin} name="linkedin" label="Linkedin" size="col-xs-6"
                               onChange={this.onFieldChange.bind(this)}/>

                    <TextField value={skype} name="skype" label="Skype" size="col-xs-6"
                               onChange={this.onFieldChange.bind(this)}/>

                </div>
            </div>
        )
    }
}

SettingsIndividual.propTypes = {}

const mapStateToProps = (state, ownProps) => {
    const {profile} = state
    const user_id = profile.data.id

    return {
        user_id,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingsIndividual)