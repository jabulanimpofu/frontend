import React, {Component, PropTypes} from 'react'

class FloatingActionButton extends Component {
    render() {
        return (
            <button onClick={this.onClick.bind(this)}
                    className="btn btn-lg btn-primary btn-icon waves-effect waves-circle waves-float floating-action-button">
                <i className="zmdi zmdi-plus"></i>
            </button>
        )
    }

    onClick(e) {
        e.preventDefault()
        this.props.onPress()
    }
}

FloatingActionButton.propTypes = {
    onPress: PropTypes.func.isRequired,
}

FloatingActionButton.defaultProps = {
    onPress: () => window.scrollTo(0, 0),
}

export default FloatingActionButton