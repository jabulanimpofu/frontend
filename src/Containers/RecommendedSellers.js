import React, {Component, PropTypes} from 'react'
import Rating from '../Components/Rating'
import {connect} from 'react-redux'
import {follow, invite, getActive} from '../Actions/Api'

class RecommendedSellers extends Component {

    componentDidMount(){
        this.props.doGetActive()
    }

    render() {
        const {active_users, doFollow, doConnect} = this.props
        const _sellers = !Array.isArray(active_users.data) ? [] : active_users.data.map((seller, index) => {
            if (seller.hide)
                return null
            return (
                <a href="" className="list-group-item media" key={index}>
                    <div className="pull-left">
                        <img className="avatar-img mCS_img_loaded" src={seller.avatar} alt=""/>
                    </div>

                    <div className="media-body">
                        <div className="lgi-heading">{seller.name}</div>
                        <small className="lgi-text">{seller.category}</small>
                        <Rating rating={seller.rating}/>
                    </div>

                    <span>
                            <div className="btn btn-primary btn-sm" style={{marginRight: 8}} onClick={(e) => {
                                e.preventDefault()
                                doFollow(seller.id)
                            }}>Follow</div>
                            <div className="btn btn-primary btn-sm" style={{marginRight: 8}} onClick={(e) => {
                                e.preventDefault()
                                doConnect(seller.id)
                            }}>Connect</div>
                        </span>
                </a>
            )
        })
        return (
            <div className="card">
                <div className="card-header">
                    <h2>You might find these interesting</h2>
                </div>

                <div className="list-group lg-alt">
                    {_sellers}
                </div>
            </div>
        )
    }
}

RecommendedSellers.propTypes = {
    active_users: PropTypes.object.isRequired,
    doFollow: PropTypes.func.isRequired,
    doConnect: PropTypes.func.isRequired,
    doGetActive: PropTypes.func.isRequired,
}

RecommendedSellers.defaultProps = {}

const mapStateToProps = (state, ownProps) => {
    const {collections: {active_users}} = state
    return {
        active_users
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        doFollow(user_id){
            alert(user_id)
            return dispatch(follow(user_id))
        },
        doConnect(user_id){
            return dispatch(invite(user_id))
        },
        doGetActive(){
            return dispatch(getActive())
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RecommendedSellers)