import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import Config from '../../config'
import {getFeatured} from '../Actions/Api'

const Product = (props) => {
    const {product} = props
    const {object : {name, price, image}} = product
    return (
        <div className="featured-product" style={{marginBottom: 8}}>
            <div className="card-header ch-img" style={{backgroundImage: `url(${Config.imgix.baseUrl}/${image}?auto=enhance&w=200&h=200&fit=crop&fm=png&dpr=2)`, height: 150}}></div>
            <div className="card-header">
                <h2>
                    {name}
                    <small>{price}</small>
                </h2>
            </div>
        </div>
    )
}

const products = [
    {
        image: "url(img/demo/note.png)",
        title: "Colgate Toothpaste",
        price: "$3"
    },
    {
        image: "url(img/demo/note.png)",
        title: "Other Product",
        price: "$5"
    },
]

class FeaturedProducts extends Component {
    componentDidMount(){
        this.props.doGetFeatured()
    }
    render() {
        const {products} = this.props
        const _products = !Array.isArray(products.data) ? [] : products.data.map((product, index) => {
            return (
                <Product product={product} key={index}/>
            )
        })
        return (
            <div className="card">
                <div className="card-header">
                    <h2>Featured Products</h2>
                </div>

                <div className="card-body" style={{padding: 8}}>
                    {_products}
                </div>
            </div>
        )
    }
}

FeaturedProducts.propTypes = {
    products: PropTypes.object.isRequired,
    doGetFeatured: PropTypes.func.isRequired,
}

const mapStateToProps = (state, ownProps) => {
    const {collections: {featured: products}} = state
    return {
        products
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        doGetFeatured(){
            return dispatch(getFeatured())
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FeaturedProducts)