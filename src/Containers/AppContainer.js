import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import Header from '../Components/Header'
import {connectToStream, subscribeToFeed} from '../Actions/Stream'
import {getProfile, getNotifications, getMessages, getTimeline, getRecentActivity} from '../Actions/Api'
import {logout} from '../Actions/App'
import {API} from '../Constants'
const {GET_NOTIFICATIONS, GET_TIMELINE, GET_TIMELINE_AGGREGATED, GET_MESSAGES, GET_RECENT_ACTIVITY} = API
import {Redirect, withRouter} from 'react-router-dom'
const $ = window.$

class AppContainer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            redirect: null
        }
    }

    componentWillMount() {
        const {doConnectToStream} = this.props
        doConnectToStream()
    }

    componentDidMount() {
        // this.notify('Jabulani are you here?', 'inverse')
    }

    componentWillReceiveProps(nextProps) {
        const {profile, location} = nextProps
        const {token, hydrated} = profile
        const {id} = profile.data

        //redirects
        let redirect = null
        if (hydrated && !token && location.pathname !== '/auth') {
            redirect = {
                pathname: '/auth',
                state: {from: location}
            }
        }
        else if (hydrated && id && location.pathname === '/auth') {
            redirect = {
                pathname: '/home',
                state: {from: location}
            }
        }
        if (redirect !== this.state.redirect) {
            this.setState({redirect})
        }
    }

    componentDidUpdate(prevProps) {
        const {profile, doGetProfile, doSubscribeToNotifications, doSubscribeToRecentActivity, doSubscribeToTimelineFlat, doSubscribeToTimelineAggregated,} = this.props
        const {data, token, tokens} = profile

        //clear redirects
        if (this.state.redirect) {
            this.setState({redirect: null})
        }

        //new token
        if (token && prevProps.profile.token !== token) {
            doGetProfile()
        }

        //new id
        if (data.id && prevProps.profile.data.id !== data.id) {

        }

        //new tokens
        if (tokens.notification && prevProps.profile.tokens.notification !== tokens.notification) {
            doSubscribeToNotifications()
        }
        if (tokens.recent_activity && prevProps.profile.tokens.recent_activity !== tokens.recent_activity) {
            doSubscribeToRecentActivity()
        }
        if (tokens.timeline_flat && prevProps.profile.tokens.timeline_flat !== tokens.timeline_flat) {
            doSubscribeToTimelineFlat()
        }
        if (tokens.timeline_aggregated && prevProps.profile.tokens.timeline_aggregated !== tokens.timeline_aggregated) {
            doSubscribeToTimelineAggregated()
        }
    }

    render() {
        console.log(this.props)
        const {location, history, match} = this.props
        const {profile} = this.props
        const {redirect} = this.state
        if (!profile.hydrated)
            return (
                <div className="page-loader palette-Plus-Afrik bg">
                    <div className="preloader pl-xl pls-white">
                        <svg className="pl-circular" viewBox="25 25 50 50">
                            <circle className="plc-path" cx="50" cy="50" r="20"/>
                        </svg>
                    </div>
                </div>
            )

        if (redirect)
            return (
                <div className="page-loader palette-Plus-Afrik bg">
                    <Redirect to={redirect}/>
                </div>
            )


        return (
            <div>
                <Header profile={true} menu={true} applications={false} notifications={true} match={match} location={location} history={history}/>
                {this.props.children}
            </div>
        )
    }

    /*
     * Bootstrap Growl - Notifications popups
     */
    notify(message, type) {
        $.growl({
            message: message
        }, {
            type: type,
            allow_dismiss: false,
            label: 'Cancel',
            className: 'btn-xs btn-inverse',
            placement: {
                from: 'top',
                align: 'right'
            },
            delay: 2500,
            animate: {
                enter: 'animated bounceIn',
                exit: 'animated bounceOut'
            },
            offset: {
                x: 20,
                y: 85
            }
        });
    };
}

AppContainer.propTypes = {
    profile: PropTypes.object.isRequired,
    doConnectToStream: PropTypes.func.isRequired,
    doGetProfile: PropTypes.func.isRequired,
}

const mapStateToProps = (state, ownProps) => {
    const {profile} = state
    return {
        profile
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        doConnectToStream(){
            return dispatch(connectToStream())
        },
        doGetProfile(){
            return dispatch(getProfile())
        },
        doLogout(){
            return dispatch(logout())
        },


        doSubscribeToNotifications(){
            return dispatch(subscribeToFeed(GET_NOTIFICATIONS, 'notification', getNotifications()))
        },
        doSubscribeToRecentActivity(){
            return dispatch(subscribeToFeed(GET_RECENT_ACTIVITY, 'recent_activity', getRecentActivity()))
        },
        doSubscribeToTimelineFlat(){
            return dispatch(subscribeToFeed(GET_TIMELINE, 'timeline_flat', getTimeline()))
        },
        doSubscribeToTimelineAggregated(){
            return dispatch(subscribeToFeed(GET_TIMELINE_AGGREGATED, 'timeline_aggregated'))
        },
        doSubscribeToMessages(){
            return dispatch(subscribeToFeed(GET_MESSAGES, 'message', getMessages()))
        },
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AppContainer))