import {createConstants} from './Util'

export const NAV = createConstants({
    NAV_REPLACE: null,
    NAV_PUSH: null,
    NAV_POP: null,
    NAV_JUMP_TO_KEY: null,
    NAV_JUMP_TO_INDEX: null,
    NAV_RESET: null,
    CLOSE_MODAL: null,

    GO_HOME: null,
    GO_PROFILE: null,
    GO_COMPANIES: null,
    GO_SERVICES: null,
    GO_PRODUCTS: null,
    GO_MESSAGES: null,
})

export const API = createConstants({
    LOGIN: null,
    REGISTER: null,
    RESET: null,
    EMAIL: null,

    GET_USER: null,
    GET_PROFILE: null,
    LOGOUT: null,
    GET_CATEGORIES: null,
    GET_PRODUCT_CATEGORIES: null,
    GET_SUB_CATEGORIES: null,
    GET_POST: null,
    GET_POSTS: null,
    GET_SERVICES: null,
    GET_PRODUCTS: null,
    GET_TIMELINE: null,
    GET_TIMELINE_LOCATION: null,
    GET_NOTIFICATIONS: null,
    GET_MESSAGES: null,
    CREATE_PRODUCT: null,
    CREATE_SERVICE: null,
    CREATE_POST: null,
    UPLOAD: null,
    FOLLOW: null,
    COMMENT: null,
    GET_FEED: null,
    GET_ACTIVE: null,
    GET_FEATURED: null,
    GET_RECENT_ACTIVITY: null,
})

export const API_FLAGS = createConstants({
    SUCCESS: null,
    ERROR: null,
    PROGRESS: null,
    STREAM: null,
})

export const HTTP_METHODS = createConstants({
    GET: null,
    POST: null,
})

export const GENERAL = createConstants({
    COLLECTION: null,
    REQUEST: null,
    LOGOUT: null,
    STREAM_NOTIFICATION: null,
    STREAM_RECENT_ACTIVITY: null,
    STREAM_MESSAGE: null,
    STREAM_TIMELINE_FLAT: null,
    STREAM_TIMELINE_AGGREGATED: null,
    SET_FILTER: null,
    RESET_FILTER: null,
})