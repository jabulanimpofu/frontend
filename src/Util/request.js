import { InternalError, ApiError } from './request_errors';
import {API_FLAGS} from '../Constants'
const queryString = require('query-string')

async function getJSON(res) {
  const contentType = res.headers.get('Content-Type');
  const emptyCodes = [204, 205];

  if (!~emptyCodes.indexOf(res.status) && contentType && ~contentType.indexOf('json')) {
    return await res.json();
  } else {
    return await Promise.resolve();
  }
}

function normalizeTypeDescriptors(types) {
  let [requestType, successType, failureType] = types;

  if (typeof requestType === 'string' || typeof requestType === 'symbol') {
    requestType = { type: requestType };
  }

  if (typeof successType === 'string' || typeof successType === 'symbol') {
    successType = { type: successType };
  }
  successType = {
    payload: (action, state, res) => getJSON(res),
    ...successType
  };

  if (typeof failureType === 'string' || typeof failureType === 'symbol') {
    failureType = { type: failureType };
  }
  failureType = {
    payload: (action, state, res) =>
      getJSON(res).then(
        (json) => new ApiError(res.status, res.statusText, json)
      ),
    ...failureType
  };

  return [requestType, successType, failureType];
}

async function actionWith(descriptor, args) {
  try {
    descriptor.payload = await (
      typeof descriptor.payload === 'function' ?
      descriptor.payload(...args) :
      descriptor.payload
    );
  } catch (e) {
    descriptor.payload = new InternalError(e.message);
    descriptor.error = true;
  }

  try {
    descriptor.meta = await (
      typeof descriptor.meta === 'function' ?
      descriptor.meta(...args) :
      descriptor.meta
    );
  } catch (e) {
    delete descriptor.meta;
    descriptor.payload = new InternalError(e.message);
    descriptor.error = true;
  }

  return descriptor;
}

const trim = (haystack, needle, side = null) => {
    haystack = haystack.trim()

    if (!side || side === 'l') {
        haystack = haystack.slice(0, needle.length) === needle
            ? trim(haystack.slice(needle.length), needle, side)
            : haystack
    }

    if (!side || side === 'r') {
        haystack = haystack.slice(haystack.length - needle.length) === needle
            ? trim(haystack.slice(0, haystack.length - needle.length), needle, side)
            : haystack
    }
    return haystack;
}

const getTypes = (type) => {
  return [type, type + '_' + API_FLAGS.SUCCESS, type + '_' + API_FLAGS.ERROR]
}

const getUrl = (root, endpoint, body) => {
    let url = trim(root, '/') + '/' + trim(endpoint, '/')
    if(typeof body === 'object') url += `?${queryString.stringify(body)}`
    return url
}

const getHeaders = (obj = {}, token) => {
    return new Headers({
        Accept: 'application/json',
        Authorization: `Bearer ${token}`,
        ...obj
    })
}

export { getJSON, normalizeTypeDescriptors, actionWith, trim, getUrl, getTypes, getHeaders };
