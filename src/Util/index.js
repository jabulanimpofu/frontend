export const createConstants = (obj) =>  {
    const ret = {}
    let key
    if (!(obj instanceof Object && !Array.isArray(obj))) {
        throw new Error('createConstants: Argument must be an object.');
    }
    for (key in obj) {
        if (obj.hasOwnProperty(key)) {
            ret[key] = key
        }
    }
    return ret
}