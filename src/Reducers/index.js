import {combineReducers} from 'redux'
import { routerReducer as router } from 'react-router-redux'
import profile from './Profile'
import collections from './Collections'
import uploads from './Uploads'
import filter from './Filter'
import user from './User'
import notifications from './Notifications'

export default combineReducers({router, profile, collections, uploads, filter, user, notifications})
