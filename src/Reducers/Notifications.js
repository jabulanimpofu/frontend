import {GENERAL, API, API_FLAGS} from '../Constants'
const initialState = {
    isFetching: false,
    data: {
        activities: [],
        activity_count: 0,
        actor_count: 0,

        created_at: "",
        group: "",
        id: "",
        is_read: false,
        is_seen: false,
        updated_at: "",
        verb: "follow",
    },
    stream: {
        _deleted: [],
        _feed: '',
        _new: [],
        _published_at: [],
    },
}

const Notifications = (state = initialState, action) => {
    switch (action.type) {
        case API.GET_NOTIFICATIONS: {
            state = Object.assign({}, state, {isFetching: true})
        }
            break
        case API.GET_NOTIFICATIONS + '_' + API_FLAGS.STREAM: {
            const _new = action.payload.new
            const {app_id: _app_id, deleted: _deleted, published_at: _published_at, feed: _feed} = action.payload

            const stream = {_app_id, _deleted, _published_at, _feed, _new}
            const ids_new = getIds(_new)
            const ids_deleted = getIds(_deleted)

            const activities = !Array.isArray(state.data.activities) ? state.data.activities : state.data.activities.map((entry) => {
                if (ids_new[entry.id]) entry = Object.assign({}, entry, {isNew: true})
                if (ids_deleted[entry.id]) entry = Object.assign({}, entry, {isDeleted: true})
                return entry
            })
            const data = Object.assign({}, state.data, {activities})
            state = Object.assign({}, state, {isFetching: false, stream, data})
        }
            break
        case API.GET_NOTIFICATIONS + '_' + API_FLAGS.SUCCESS: {
            const pp = {
                activities: [],
                activity_count: 1,
                actor_count: 1,
                created_at: "2017-04-27T07:30:51.961000",
                group: "44459_2017-04-27",
                id: "71182a90-2b1b-11e7-8080-800043775e66",
                is_read: false,
                is_seen: false,
                updated_at: "2017-04-27T07:30:51.961000",
                verb: "follow",
            }

            const {payload: raw, meta} = action
            if(!Array.isArray(raw) || !raw.length)
                return state
            const payload = raw[0]


            const ids_new = getIds(state.stream._new)
            const ids_deleted = getIds(state.stream._deleted)
            const activities = !Array.isArray(payload.activities) ? [] : payload.activities.map((entry) => {
                if (ids_new[entry.id]) entry = Object.assign({}, entry, {isNew: true})
                if (ids_deleted[entry.id]) entry = Object.assign({}, entry, {isDeleted: true})
                return entry
            })
            const data = Object.assign({}, payload, {activities})
            state = Object.assign({}, state, {isFetching: false, data, stream: initialState.stream})
        }
            break;

        default:
    }
    return state
}

const getIds = (arr) => {
    if (!Array.isArray(arr)) return {}
    return arr.reduce((ids, obj) => Object.assign({}, ids, {[obj.id]: true}), {})
}

export default Notifications