import {API, API_FLAGS} from '../Constants'
const initialState = {
    isFetching: false,
    data: {},
}

const User = (state = initialState, action) => {
    switch (action.type){
        case API.GET_USER:{
            state = Object.assign({}, state, {isFetching: true})
        }
        break
        case API.GET_USER + '_' + API_FLAGS.SUCCESS:{
            const {payload: data, meta} = action
            state = Object.assign({}, state, {isFetching: false, data})
        }
        break
        case API.GET_USER + '_' + API_FLAGS.ERROR:{
            state = Object.assign({}, initialState, {})
        }
        break
        default:
    }
    return state
}

export default User