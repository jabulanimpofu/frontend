import {API, API_FLAGS, GENERAL} from '../Constants'
import {combineReducers} from 'redux'
import {REHYDRATE} from 'redux-persist/constants'

const dt = {
    account_type: "individual",
    email: "007@mi6.com",
    id: 3,
    jwt: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZXF1ZXN0Ijp7ImVtYWlsIjoiMDA3QG1pNi5jb20ifSwiaWF0IjoxNDkzMDUzMTM5fQ.ay-Mu69hgXISPbgcbYHX_qILobpTnQDI0iDf9ZjTFRE",
    name: "James Bond",
    notification: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyZXNvdXJjZSI6IioiLCJhY3Rpb24iOiJyZWFkIiwiZmVlZF9pZCI6Im5vdGlmaWNhdGlvbjMifQ.h1qQV7KAjWTtJoOfoMV5Mch0TcOzK85yFLsoexiFUL0",
    password: "007",
    timeline: {
        aggregated: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyZXNvdXJjZSI6IioiLCJhY3Rpb24iOiJyZWFkIiwiZmVlZF9pZCI6InRpbWVsaW5lX2FnZ3JlZ3JhdGVkMyJ9.xF8bspmKw5uMR-7GjCXYs0HzfJYQXJvZHaYs-BAzuZ8",
        flat: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyZXNvdXJjZSI6IioiLCJhY3Rpb24iOiJyZWFkIiwiZmVlZF9pZCI6InRpbWVsaW5lX2ZsYXQzIn0.VAveZPVJWZd0pzo7I9I3UXh2oL2fwMqdlMPlgr4jqTs",

    }
}

const initialState = {
    data: {},
    error: {},
    isFetching: false,
    message: {},
    token: null,
    hydrated: false,
    tokens: {}
}

const data = (state = initialState.data, action) => {
    switch (action.type) {
        case REHYDRATE: {
            // let incoming = action.payload.profile
            // if (incoming) state = incoming.data
        }
            break
        case GENERAL.LOGOUT: {
            state = initialState.error
        }
            break

        case API.REGISTER + '_' + API_FLAGS.ERROR:
        case API.LOGIN + '_' + API_FLAGS.ERROR:
        case API.EMAIL + '_' + API_FLAGS.ERROR:{
            state = initialState.data
        }
        break

        case API.REGISTER + '_' + API_FLAGS.SUCCESS:
        case API.LOGIN + '_' + API_FLAGS.SUCCESS:
        case API.EMAIL + '_' + API_FLAGS.SUCCESS:
        case API.GET_PROFILE + '_' + API_FLAGS.SUCCESS: {
            const {payload, meta} = action
            const {account_type,email,id,name,password} = payload
            state = {account_type,email,id,name,password}
        }
            break

    }
    return state
}

const error = (state = initialState.error, action) => {
    const {type} = action
    switch (type) {
        case GENERAL.LOGOUT: {
            state = initialState.error
        }
            break
        case API.REGISTER:
        case API.LOGIN:
        case API.EMAIL:
            state = Object.assign({}, state, {[type + '_' + API_FLAGS.ERROR]: null})
            break
        case API.REGISTER + '_' + API_FLAGS.ERROR:
        case API.LOGIN + '_' + API_FLAGS.ERROR:
        case API.EMAIL + '_' + API_FLAGS.ERROR: {
            const {error, meta, payload} = action
            const {message, name, response, status, statusText} = payload
            const type_error = {
                [type]: {
                    name,
                    errors: response
                }
            }
            state = Object.assign({}, state, type_error)
        }
            break
        default:
            break
    }
    return state
}

const isFetching = (state = initialState.isFetching, action) => {
    const {type} = action
    switch (type) {
        case GENERAL.LOGOUT: {
            state = initialState.isFetching
        }
            break
        case API.REGISTER:
        case API.LOGIN:
        case API.EMAIL:
            state = true
            break
        case API.REGISTER + '_' + API_FLAGS.SUCCESS:
        case API.LOGIN + '_' + API_FLAGS.SUCCESS:
        case API.EMAIL + '_' + API_FLAGS.SUCCESS:
        case API.REGISTER + '_' + API_FLAGS.ERROR:
        case API.LOGIN + '_' + API_FLAGS.ERROR:
        case API.EMAIL + '_' + API_FLAGS.ERROR:
            state = false
            break
        default:
            break
    }
    return state
}

const message = (state = initialState.message, action) => {
    const {type} = action
    switch (type) {
        case GENERAL.LOGOUT: {
            state = initialState.message
        }
            break
        case API.REGISTER + '_' + API_FLAGS.ERROR:
        case API.LOGIN + '_' + API_FLAGS.ERROR:
        case API.EMAIL + '_' + API_FLAGS.ERROR: {
            const {error, meta, payload} = action
            const {message, name, response, status, statusText} = payload
            state = {
                title: name,
                description: message
            }
        }
            break

        default:
            break
    }
    return state
}

const token = (state = initialState.token, action) => {
    const API_ERROR = /_ERROR/
    const {type} = action

    if (API_ERROR.test(type)) {
        const {error, payload} = action
        if (error && payload.status === 401 && payload.response.error === 'token_expired') {
            return null
        }
    }


    switch (type) {
        case GENERAL.LOGOUT: {
            state = initialState.token
        }
            break
        case REHYDRATE: {
            let incoming = action.payload.profile
            if (incoming) state = incoming.token
        }
            break
        case API.REGISTER + '_' + API_FLAGS.SUCCESS:
        case API.LOGIN + '_' + API_FLAGS.SUCCESS:
            const {meta, payload} = action
            state = payload.jwt
            break
        case API.REGISTER + '_' + API_FLAGS.ERROR:
        case API.LOGIN + '_' + API_FLAGS.ERROR:
            state = null
            break
        default:
            break

    }
    return state
}

const hydrated = (state = initialState.hydrated, action) => {
    switch (action.type) {
        case REHYDRATE:
            state = true
            break
    }
    return state
}

const tokens = (state = initialState.tokens, action) => {
    switch (action.type) {
        case GENERAL.LOGOUT: {
            state = initialState.tokens
        }
        case API.REGISTER + '_' + API_FLAGS.SUCCESS:
        case API.LOGIN + '_' + API_FLAGS.SUCCESS:
        case API.EMAIL + '_' + API_FLAGS.SUCCESS:
        case API.GET_PROFILE + '_' + API_FLAGS.SUCCESS:{
            const {meta, payload} = action
            const {timeline, notification, recent_activity} = payload.tokens
            const {aggregated: timeline_aggregated, flat: timeline_flat} = timeline
            state = {notification, timeline_aggregated, timeline_flat, recent_activity}
        }
            break
        case API.GET_NOTIFICATIONS + '_' + API_FLAGS.SUCCESS: {
            const {meta, payload} = action
            const {readonlyToken, notifications} = payload
            state = {...state, notifications: readonlyToken}
        }
            break
        case API.GET_TIMELINE + '_' + API_FLAGS.SUCCESS: {
            const {meta, payload} = action
            const {readonlyToken, posts} = payload
            state = {...state, timeline: readonlyToken}
        }
            break
        default:
    }
    return state
}

export default combineReducers({data, error, isFetching, message, token, hydrated, tokens})