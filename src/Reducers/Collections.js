import {GENERAL, API, API_FLAGS} from '../Constants'
import {combineReducers} from 'redux'
import {REHYDRATE} from 'redux-persist/constants'

const initialState = {
    isFetching: false,
    data: null,
    stream: {
        _deleted: [],
        _feed: '',
        _new: [],
        _published_at: [],
    },
}

export const timeline = (state = {...initialState, type: ''}, action) => {
    switch (action.type) {
        case API.GET_PRODUCTS:
        case API.GET_SERVICES:
        case API.GET_POSTS:
        case API.GET_TIMELINE: {
            state = Object.assign({}, state, {isFetching: true, type: action.type})
        }
            break
        case API.GET_TIMELINE + '_' + API_FLAGS.STREAM: {
            const _new = action.payload.new
            const {app_id: _app_id, deleted: _deleted, published_at: _published_at, feed: _feed} = action.payload

            const stream = {_app_id, _deleted, _published_at, _feed, _new}
            const ids_new = getIds(_new)
            const ids_deleted = getIds(_deleted)

            const data = !Array.isArray(state.data) ? state.data : state.data.map((entry) => {
                if (ids_new[entry.id]) entry = Object.assign({}, entry, {isNew: true})
                if (ids_deleted[entry.id]) entry = Object.assign({}, entry, {isDeleted: true})
                return entry
            })

            state = Object.assign({}, state, {isFetching: false, stream, data})
        }
            break
        case API.GET_PRODUCTS + '_' + API_FLAGS.SUCCESS:
        case API.GET_SERVICES + '_' + API_FLAGS.SUCCESS:
        case API.GET_POSTS + '_' + API_FLAGS.SUCCESS:
        case API.GET_TIMELINE + '_' + API_FLAGS.SUCCESS: {
            const {payload, meta} = action
            const ids_new = getIds(state.stream._new)
            const ids_deleted = getIds(state.stream._deleted)
            const data = !Array.isArray(payload) ? [] : payload.map((entry) => {
                if (ids_new[entry.id]) entry = Object.assign({}, entry, {isNew: true})
                if (ids_deleted[entry.id]) entry = Object.assign({}, entry, {isDeleted: true})
                entry.object.image = entry.object.image ? [entry.object.image] : []

                if(action.type === API.GET_PRODUCTS + '_' + API_FLAGS.SUCCESS){
                    entry.object = Object.assign({}, entry.object, {
                        name: "Randomly generated product",
                        price: "$5.00",
                        category: 'Clothes',
                        sub_category: "Jeans",
                    })
                }

                if(action.type === API.GET_SERVICES + '_' + API_FLAGS.SUCCESS){
                    entry.object = Object.assign({}, entry.object, {
                        name: "Randomly generated product",
                        price: "$5.00",
                        rate: "hour",
                        category: 'Clothes',
                        sub_category: "Jeans",
                    })
                }

                if(action.type === API.GET_TIMELINE + '_' + API_FLAGS.SUCCESS){
                    const rand = Math.floor(Math.random()*2)
                    switch(rand){
                        case 1:{
                            entry.object = Object.assign({}, entry.object, {
                                name: "Randomly generated product",
                                price: "$5.00",
                                rate: "hour",
                                category: 'Clothes',
                                sub_category: "Jeans",
                            })
                        }
                            break
                        case 2:{
                            entry.object = Object.assign({}, entry.object, {
                                name: "Randomly generated product",
                                price: "$5.00",
                                category: 'Clothes',
                                sub_category: "Jeans",
                            })
                        }
                            break
                        default:
                    }
                }

                return entry
            })
            state = Object.assign({}, state, {isFetching: false, data, stream: initialState.stream})
        }
            break
        case GENERAL.CLEAR_NEW_TIMELINE: {

        }
            break;

        default:
    }
    return state
}

export const recent_activity = (state = initialState, action) => {
    switch (action.type) {
        case API.GET_RECENT_ACTIVITY: {
            state = Object.assign({}, state, {isFetching: true})
        }
            break
        case API.GET_RECENT_ACTIVITY + '_' + API_FLAGS.STREAM: {
            const _new = action.payload.new
            const {app_id: _app_id, deleted: _deleted, published_at: _published_at, feed: _feed} = action.payload

            const stream = {_app_id, _deleted, _published_at, _feed, _new}
            const ids_new = getIds(_new)
            const ids_deleted = getIds(_deleted)

            const data = !Array.isArray(state.data) ? state.data : state.data.map((entry) => {
                if (ids_new[entry.id]) entry = Object.assign({}, entry, {isNew: true})
                if (ids_deleted[entry.id]) entry = Object.assign({}, entry, {isDeleted: true})
                return entry
            })

            state = Object.assign({}, state, {isFetching: false, stream, data})
        }
            break
        case API.GET_RECENT_ACTIVITY + '_' + API_FLAGS.SUCCESS: {
            const {payload, meta} = action
            const ids_new = getIds(state.stream._new)
            const ids_deleted = getIds(state.stream._deleted)
            const data = !Array.isArray(payload) ? [] : payload.map((entry) => {
                if (ids_new[entry.id]) entry = Object.assign({}, entry, {isNew: true})
                if (ids_deleted[entry.id]) entry = Object.assign({}, entry, {isDeleted: true})
                return entry
            })
            state = Object.assign({}, state, {isFetching: false, data, stream: initialState.stream})
        }
            break;

        default:
    }
    return state
}

export const messages = (state = initialState, action) => {
    switch (action.type) {
        case API.GET_MESSAGES: {
            state = Object.assign({}, state, {isFetching: true})
        }
            break
        case API.GET_MESSAGES + '_' + API_FLAGS.STREAM: {
            const _new = action.payload.new
            const {app_id: _app_id, deleted: _deleted, published_at: _published_at, feed: _feed} = action.payload

            const stream = {_app_id, _deleted, _published_at, _feed, _new}
            const ids_new = getIds(_new)
            const ids_deleted = getIds(_deleted)

            const data = !Array.isArray(state.data) ? state.data : state.data.map((entry) => {
                if (ids_new[entry.id]) entry = Object.assign({}, entry, {isNew: true})
                if (ids_deleted[entry.id]) entry = Object.assign({}, entry, {isDeleted: true})
                return entry
            })

            state = Object.assign({}, state, {isFetching: false, stream, data})
        }
            break
        case API.GET_MESSAGES + '_' + API_FLAGS.SUCCESS: {
            const {payload, meta} = action
            const ids_new = getIds(state.stream._new)
            const ids_deleted = getIds(state.stream._deleted)
            const data = !Array.isArray(payload) ? [] : payload.map((entry) => {
                if (ids_new[entry.id]) entry = Object.assign({}, entry, {isNew: true})
                if (ids_deleted[entry.id]) entry = Object.assign({}, entry, {isDeleted: true})
                return entry
            })
            state = Object.assign({}, state, {isFetching: false, data, stream: initialState.stream})
        }
            break;

        default:
    }
    return state
}

export const active_users = (state = initialState, action) => {
    switch (action.type) {
        case API.GET_ACTIVE: {
            state = Object.assign({}, state, {isFetching: true})
        }
            break

        case API.FOLLOW: {
            const userIndex = state.data.reduce((userIndex, user, index) => {
                if (user.id === action.id)
                    userIndex = index
                return userIndex
            }, undefined)
            if (userIndex === undefined)
                return state
            const data = [
                state.data.slice(0, userIndex),
                {...state.data[userIndex], hide: true},
                state.data.slice(userIndex + 1)
            ]
            state = Object.assign({}, state, {isFetching: true, data})
        }
            break

        case API.GET_ACTIVE + '_' + API_FLAGS.SUCCESS:
            const {payload: data, meta} = action
            state = Object.assign({}, state, {isFetching: false, data})
            break;
        default:
    }
    return state
}

export const posts = (state = initialState, action) => {
    switch (action.type) {

        case API.GET_POSTS: {
            state = Object.assign({}, state, {isFetching: true})
        }
            break
        case API.GET_POSTS + '_' + API_FLAGS.SUCCESS: {
            const {payload: data, meta} = action
            state = Object.assign({}, state, {isFetching: false, data})
        }
            break;

        default:
    }
    return state
}

export const users = (state = initialState, action) => {
    switch (action.type) {
        case API.GET_TIMELINE: {
            state = Object.assign({}, state, {isFetching: true})
        }
            break
        case API.GET_TIMELINE + '_' + API_FLAGS.SUCCESS: {

        }
            break;

        default:
    }
    return state
}

export const categories = (state = initialState, action) => {
    switch (action.type) {
        case REHYDRATE: {
            try {
                let incoming = action.payload.collections
                if (incoming) state = incoming.categories
            }catch (e){

            }
        }
            break
        case API.GET_CATEGORIES: {
            state = Object.assign({}, state, {isFetching: true})
        }
            break
        case API.GET_CATEGORIES + '_' + API_FLAGS.SUCCESS: {
            const {payload: data, meta} = action
            state = Object.assign({}, state, {isFetching: false, data})
        }
            break;

        default:
    }
    return state
}

export const product_categories = (state = initialState, action) => {
    switch (action.type) {
        case REHYDRATE: {
            try {
                let incoming = action.payload.collections
                if (incoming) state = incoming.product_categories
            }catch (e){

            }
        }
            break
        case API.GET_PRODUCT_CATEGORIES: {
            state = Object.assign({}, state, {isFetching: true})
        }
            break
        case API.GET_PRODUCT_CATEGORIES + '_' + API_FLAGS.SUCCESS: {
            const {payload: data, meta} = action
            state = Object.assign({}, state, {isFetching: false, data})
        }
            break;

        default:
    }
    return state
}

export const sub_categories = (state = initialState, action) => {
    switch (action.type) {
        case REHYDRATE: {
            try {
                let incoming = action.payload.collections
                if (incoming) state = incoming.sub_categories
            }catch (e){

            }
        }
            break
        case API.GET_SUB_CATEGORIES: {
            state = Object.assign({}, state, {isFetching: true})
        }
            break
        case API.GET_SUB_CATEGORIES + '_' + API_FLAGS.SUCCESS: {
            const {payload: data, meta} = action
            state = Object.assign({}, state, {isFetching: false, data})
        }
            break;

        default:
    }
    return state
}

export const featured = (state = initialState, action) => {
    switch (action.type) {
        case API.GET_FEATURED: {
            state = Object.assign({}, state, {isFetching: true})
        }
            break
        case API.GET_FEATURED + '_' + API_FLAGS.SUCCESS: {
            const {payload: data, meta} = action
            state = Object.assign({}, state, {isFetching: false, data})
        }
            break;

        default:
    }
    return state
}

const getIds = (arr) => {
    if (!Array.isArray(arr)) return {}
    return arr.reduce((ids, obj) => Object.assign({}, ids, {[obj.id]: true}), {})
}

export default combineReducers({
    posts,
    timeline,
    users,
    categories,
    product_categories,
    sub_categories,
    recent_activity,
    active_users,
    featured,
    messages,
})