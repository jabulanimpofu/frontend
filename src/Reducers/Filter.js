import {GENERAL} from '../Constants'

const initialState = {
    term: '',
    type: null,
    category: null,
    place: {
        suburb: '',
        city: '',
        province: '',
        country: '',
    }
}
const filter = (state = initialState, action) => {
    switch (action.type) {
        case GENERAL.SET_FILTER: {
            state = action.filter
        }
            break
        case GENERAL.RESET_FILTER: {
            state = initialState
        }
            break
        default:
    }
    return state
}

export default filter

