import {GENERAL, API, API_FLAGS} from '../Constants'
const initialState = {}

const Uploads = (state = initialState, action) => {
    switch (action.type) {

        case API.UPLOAD: {
            const {file} = action
            const {type, size, webkitRelativePath, name, preview, lastModified, lastModifiedDate, id} = file
            state = {
                ...state,
                [id]: {
                    type,
                    size,
                    webkitRelativePath,
                    name,
                    preview,
                    lastModified,
                    lastModifiedDate,
                    id,
                    progress: 0,
                }
            }
        }
            break;

        case API.UPLOAD + '_' + API_FLAGS.PROGRESS: {
            const {id, progress} = action
            const image = state[id]

            state = {
                ...state,
                [id]: {...image, progress}
            }
        }
            break

        case API.UPLOAD + '_' + API_FLAGS.SUCCESS: {
            const {id, images} = action
            const url = images[0]
            const image = state[id]

            state = {
                ...state,
                [id]: {...image, url}
            }
        }
            break

        default:
    }
    return state
}

export default Uploads